import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0304Page } from './advice0304';

@NgModule({
  declarations: [
    Advice0304Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0304Page),
  ],
})
export class Advice0304PageModule {}
