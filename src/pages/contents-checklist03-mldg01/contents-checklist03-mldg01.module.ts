import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContentsChecklist03Mldg01Page } from './contents-checklist03-mldg01';

@NgModule({
  declarations: [
    ContentsChecklist03Mldg01Page,
  ],
  imports: [
    IonicPageModule.forChild(ContentsChecklist03Mldg01Page),
  ],
})
export class ContentsChecklist03Mldg01PageModule {}
