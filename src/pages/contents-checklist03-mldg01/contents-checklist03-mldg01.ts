import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { CheckList03E } from '../checklist03E/checklist03E';

/**
 * Generated class for the ContentsChecklist03Mldg01Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contents-checklist03-mldg01',
  templateUrl: 'contents-checklist03-mldg01.html',
})
export class ContentsChecklist03Mldg01Page {

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public viewCtrl : ViewController) {
  }

  goMeldung() {
    this.navCtrl.push(CheckList03E);
  }

  goCancel()
{
  var GoInPage = this.modalCtrl.create('ContentsChecklist03Mldg02Page');
  GoInPage.present();
}
}

