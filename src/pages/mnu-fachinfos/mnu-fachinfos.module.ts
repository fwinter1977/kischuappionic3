import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFachinfosPage } from './mnu-fachinfos';

@NgModule({
  declarations: [
    MnuFachinfosPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuFachinfosPage),
  ],
})
export class MnuFachinfosPageModule {}
