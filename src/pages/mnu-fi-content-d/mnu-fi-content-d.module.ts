import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFiContentDPage } from './mnu-fi-content-d';

@NgModule({
  declarations: [
    MnuFiContentDPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuFiContentDPage),
  ],
})
export class MnuFiContentDPageModule {}
