import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0160Page } from './advice0160';

@NgModule({
  declarations: [
    Advice0160Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0160Page),
  ],
})
export class Advice0160PageModule {}
