import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFicPage } from './mnu-fic';

@NgModule({
  declarations: [
    MnuFicPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuFicPage),
  ],
})
export class MnuFicPageModule {}
