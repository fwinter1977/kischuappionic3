import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0110Page } from './advice0110';

@NgModule({
  declarations: [
    Advice0110Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0110Page),
  ],
})
export class Advice0110PageModule {}
