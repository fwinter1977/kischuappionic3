import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { SpeicherPage } from '../speicher/speicher';
import { KontaktePage } from '../kontakte/kontakte';
import { EinstellungenPage } from '../einstellungen/einstellungen';
import { ErstInfoPage } from '../erstinfo/erstinfo';
import { ImpressumPage } from '../impressum/impressum';
import { SettingsPage } from '../settings/settings';

/**
 * Generated class for the NavigationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  
  selector: 'page-navigation',
  templateUrl: 'navigation.html',
})
export class NavigationPage {

  constructor(public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NavigationPage');
  }
  public Back()
  {
    this.viewCtrl.dismiss();
  }
  Go(target: string)
  {
    var GoInPage = this.modalCtrl.create(target+'Page');
    GoInPage.present();
  }
  GoHome()
  {
    //var GoInPage = this.modalCtrl.create(target+'Page');
    //GoInPage.present();
    this.navCtrl.push(HomePage);
  }
  GoSpeicher()
  {
    //var GoInPage = this.modalCtrl.create(target+'Page');
    //GoInPage.present();
    this.navCtrl.push(SpeicherPage);
  }
  GoKontakte()
  {
    //var GoInPage = this.modalCtrl.create(target+'Page');
    //GoInPage.present();
    this.navCtrl.push(KontaktePage);
  }
  GoEinstellungen()
  {
    //var GoInPage = this.modalCtrl.create(target+'Page');
    //GoInPage.present();
    this.navCtrl.push(EinstellungenPage);
  }
  GoErstInfo()
  {
    //var GoInPage = this.modalCtrl.create(target+'Page');
    //GoInPage.present();
    this.navCtrl.setRoot(ErstInfoPage);
  }
  GoEinstellungen2()
  {
    //var GoInPage = this.modalCtrl.create(target+'Page');
    //GoInPage.present();
    this.navCtrl.push(SettingsPage);
  }
  GoImpressum()
  {
    //var GoInPage = this.modalCtrl.create(target+'Page');
    //GoInPage.present();
    this.navCtrl.push(ImpressumPage);
  }
}
