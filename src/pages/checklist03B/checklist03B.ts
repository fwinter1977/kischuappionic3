import { Component } from '@angular/core';
import { Nav, Platform, ModalController, NavParams, ViewController } from 'ionic-angular';
import { CheckList03A } from '../checklist03A/checklist03A';
import { CheckList03C } from '../checklist03C/checklist03C';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { DataProvider } from '../../providers/data/data';
import { AbspeichernPage } from '../abspeichern/abspeichern';
@Component({
  selector: 'page-checklist03B',
  templateUrl: 'checklist03B.html'
})
export class CheckList03B {
  loadedList: any;
  checked10a : string;
  checked10b : string;
  checked11a : string;
  checked11b : string;
  checked12a : string;
  checked12b : string;
  checked13a : string;
  checked13b : string;
  checked14a : string;
  checked14b : string;
  checked15a : string;
  checked15b : string;
  hideMe10: boolean;
  hideMe11: boolean;
  hideMe12: boolean;
  hideMe13: boolean;
  hideMe14: boolean;
  hideMe15: boolean;
  
  constructor(public modalCtrl: ModalController, public navCtrl: NavController, public alertCtrl: AlertController, public data : DataProvider,public viewCtrl: ViewController) {

    this.loadedList = this.data.getInitialData();
    if (this.loadedList.length == 0) {
      this.checked10a = "false";
      this.checked10b = "false";
      this.checked11a = "false";
      this.checked11b = "false";
      this.checked12a = "false";
      this.checked12b = "false";
      this.checked13a = "false";
      this.checked13b = "false";
      this.checked14a = "false";
      this.checked14b = "false";
      this.checked15a = "false";
      this.checked15b = "false";
      this.hideMe11 = true;
      this.hideMe12 = true;
      this.hideMe13 = true;
      this.hideMe14 = true;
      this.hideMe15 = true;
    }
    else {
      this.checked10a = "false";
      this.checked10b = "false";
      this.checked11a = "false";
      this.checked11b = "false";
      this.checked12a = "false";
      this.checked12b = "false";
      this.checked13a = "false";
      this.checked13b = "false";
      this.checked14a = "false";
      this.checked14b = "false";
      this.checked15a = "false";
      this.checked15b = "false";
      this.hideMe11 = true;
      this.hideMe12 = true;
      this.hideMe13 = true;
      this.hideMe14 = true;
      this.hideMe15 = true;
      
      if (this.loadedList[209] != undefined) {
        this.checked10a = this.loadedList[209].checkeda;
        this.checked10b = this.loadedList[209].checkedb;
        this.hideMe10 = this.loadedList[209].hidden;
      }
      if (this.loadedList[210] != undefined) {
        this.checked11a = this.loadedList[210].checkeda;
        this.checked11b = this.loadedList[210].checkedb;
        this.hideMe11 = this.loadedList[210].hidden;
      }
      if (this.loadedList[211] != undefined) {
        this.checked12a = this.loadedList[211].checkeda;
        this.checked12b = this.loadedList[211].checkedb;
        this.hideMe12 = this.loadedList[211].hidden;
      }
      if (this.loadedList[212] != undefined) {
        this.checked13a = this.loadedList[212].checkeda;
        this.checked13b = this.loadedList[212].checkedb;
        this.hideMe13 = this.loadedList[212].hidden;
      }
      if (this.loadedList[213] != undefined) {
        this.checked14a = this.loadedList[213].checkeda;
        this.checked14b = this.loadedList[213].checkedb;
        this.hideMe14 = this.loadedList[213].hidden;
      }
      if (this.loadedList[214] != undefined) {
        this.checked15a = this.loadedList[214].checkeda;
        this.checked15b = this.loadedList[214].checkedb;
        this.hideMe15 = this.loadedList[214].hidden;
      }
    }
  
  }
  reset() {
    const confirm = this.alertCtrl.create({
      title: 'Zurücksetzen',
      message: 'Möchten Sie diesen Teil der Checkliste zurücksetzen?',
      buttons: [
        {
          text: 'Ja',
          handler: () => {
            this.checked10a = "false";
            this.checked10b = "false";
            this.checked11a = "false";
            this.checked11b = "false";
            this.checked12a = "false";
            this.checked12b = "false";
            this.checked13a = "false";
            this.checked13b = "false";
            this.checked14a = "false";
            this.checked14b = "false";
            this.checked15a = "false";
            this.checked15b = "false";
            this.hideMe11 = true;
            this.hideMe12 = true;
            this.hideMe13 = true;
            this.hideMe14 = true;
            this.hideMe15 = true;
          }
        },
        {
          text: 'nein',
          handler: () => {
            //console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }
  
  saveList() {

    this.checked10a = "false";
    this.checked10b = "false";
    this.checked11a = "false";
    this.checked11b = "false";
    this.checked12a = "false";
    this.checked12b = "false";
    this.checked13a = "false";
    this.checked13b = "false";
    this.checked14a = "false";
    this.checked14b = "false";
    this.checked15a = "false";
    this.checked15b = "false";
    this.hideMe11 = true;
    this.hideMe12 = true;
    this.hideMe13 = true;
    this.hideMe14 = true;
    this.hideMe15 = true;
    this.navCtrl.push(AbspeichernPage);
  }

  itemPrev(event, item) {
    this.navCtrl.push(CheckList03A);
  }
  itemNext(event, item) {
    this.navCtrl.push(CheckList03C);
  }
  itemHome(event, item) {
    this.viewCtrl.dismiss();
  }
  ja10()
  {
    this.checked10a="true";
    this.checked10b="false";
    this.data.setData(209, "true", "false", false);
    this.hideMe11 = false;

  }
  nein10()
  {
    this.checked10a="false";
    this.checked10b="true";
    this.data.setData(209, "false", "true", false);
    this.hideMe11 = false;

  }

  ja11()
  {

    this.checked11a="true";
    this.checked11b="false";
    this.data.setData(210, "true", "false", false);
    this.hideMe12=true;
    this.HinweisJugendamt();
  }
  nein11()
  {

    this.checked11a="false";
    this.checked11b="true";
    this.data.setData(210, "false", "true", false);
    this.hideMe12=false;
  }
  ja12()
  {

    this.checked12a="true";
    this.checked12b="false";
    this.data.setData(211, "true", "false", false);
    this.hideMe13=false;

  }
  nein12()
  {

    this.checked12a="false";
    this.checked12b="true";
    this.data.setData(211, "false", "true", false);
    this.hideMe13=true;
    this.HinweisFeld();
  }
  ja13()
  {

    this.checked13a="true";
    this.checked13b="false";
    this.data.setData(212, "true", "false", false);
    this.hideMe14=false;
  }
  nein13()
  {

    this.checked13a="false";
    this.checked13b="true";
    this.data.setData(212, "false", "true", false);
    this.hideMe14=false;
  }
  ja14()
  {

    this.checked14a="true";
    this.checked14b="false";
    this.data.setData(213, "true", "false", false);
    this.HinweisNext();
  }  
  nein14()
  {

    this.checked14a="false";
    this.checked14b="true";
    this.data.setData(213, "false", "true", false);
    this.hideMe15=false;
  }
  ja15()
  {

    this.checked15a="true";
    this.checked15b="false";
    this.data.setData(214, "true", "false", false);
    this.HinweisNext();
  }
  nein15()
  {

    this.checked15a="false";
    this.checked15b="true";
    this.data.setData(214, "false", "true", false);
    this.HinweisFeld();
  }


  HinweisNext() {
    //neue Funktion!
        var data = { message : 'Hinweis' };
        var finishPage = this.modalCtrl.create('AdviceNextPage',data);
        finishPage.present();
      }
  
  Hinweis(txt_Hinweis: string) {
    let alert = this.alertCtrl.create({
      title: 'Hinweis',
      subTitle: txt_Hinweis,
      //subTitle: 'Hinweistitel werden noch ergänzt.',
      buttons: ['Ok']
    });
    alert.present();
  }
  HinweisJugendamt() {
    //neue Funktion!
    var data = { message: 'Hinweis' };
    var finishPage = this.modalCtrl.create('AdviceJugendamtPage', data);
    finishPage.present();
  }

  HinweisFeld() {
    //neue Funktion!
    var data = { message: 'Hinweis' };
    var finishPage = this.modalCtrl.create('AdviceFieldPage', data);
    finishPage.present();
  }  
  Finish() {
//neue Funktion!
    var data = { message : 'Ende des Verfahrens' };
    var finishPage = this.modalCtrl.create('FinishPage',data);
    finishPage.present();
  }
  Notizen(qnumber) 
  {
    var notizenPage = this.modalCtrl.create('NotePage', { questionnumber:qnumber });
    notizenPage.present();
  }

  Advice(AdviceNr: string)
  {
    var advicePage = this.modalCtrl.create('Advice'+AdviceNr+'Page');
    advicePage.present();
  }
}
