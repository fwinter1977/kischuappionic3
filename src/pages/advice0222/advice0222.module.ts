import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0222Page } from './advice0222';

@NgModule({
  declarations: [
    Advice0222Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0222Page),
  ],
})
export class Advice0222PageModule {}
