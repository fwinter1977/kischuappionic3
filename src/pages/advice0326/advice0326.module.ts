import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0326Page } from './advice0326';

@NgModule({
  declarations: [
    Advice0326Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0326Page),
  ],
})
export class Advice0326PageModule {}
