import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0118Page } from './advice0118';

@NgModule({
  declarations: [
    Advice0118Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0118Page),
  ],
})
export class Advice0118PageModule {}
