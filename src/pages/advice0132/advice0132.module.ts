import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0132Page } from './advice0132';

@NgModule({
  declarations: [
    Advice0132Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0132Page),
  ],
})
export class Advice0132PageModule {}
