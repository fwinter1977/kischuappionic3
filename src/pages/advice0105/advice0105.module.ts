import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0105Page } from './advice0105';

@NgModule({
  declarations: [
    Advice0105Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0105Page),
  ],
})
export class Advice0105PageModule {}
