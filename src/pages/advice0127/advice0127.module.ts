import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0127Page } from './advice0127';

@NgModule({
  declarations: [
    Advice0127Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0127Page),
  ],
})
export class Advice0127PageModule {}
