import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0125Page } from './advice0125';

@NgModule({
  declarations: [
    Advice0125Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0125Page),
  ],
})
export class Advice0125PageModule {}
