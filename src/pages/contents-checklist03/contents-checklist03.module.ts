import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContentsChecklist03Page } from './contents-checklist03';

@NgModule({
  declarations: [
    ContentsChecklist03Page,
  ],
  imports: [
    IonicPageModule.forChild(ContentsChecklist03Page),
  ],
})
export class ContentsChecklist03PageModule {}
