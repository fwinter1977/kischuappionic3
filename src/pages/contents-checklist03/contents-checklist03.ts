import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { CheckList03A } from '../checklist03A/checklist03A';
import { CheckList03B } from '../checklist03B/checklist03B';
import { CheckList03C } from '../checklist03C/checklist03C';
import { CheckList03D } from '../checklist03D/checklist03D';
import { CheckList03E } from '../checklist03E/checklist03E';


/**
 * Generated class for the ContentsChecklist02Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contents-checklist03',
  templateUrl: 'contents-checklist03.html',
})
export class ContentsChecklist03Page {

    constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public viewCtrl : ViewController) {
    }
  
    public closeAdvice()
    {
      this.viewCtrl.dismiss();
    }
    Start() {
      this.navCtrl.push(CheckList03A);
    }
  
    Start2() {
      this.navCtrl.push(CheckList03B);
    }
    Start3() {
      this.navCtrl.push(CheckList03C);
    }
    Start4() {
      this.navCtrl.push(CheckList03D);
    }
    Start5() {
      var GoInPage = this.modalCtrl.create('ContentsChecklist03Mldg01Page');
      GoInPage.present();
    }
    GoContent()
    {
      var GoInPage = this.modalCtrl.create('ContentsChecklist01Page');
      GoInPage.present();
    }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad ContentsChecklist01Page');
    }
  
  }
  