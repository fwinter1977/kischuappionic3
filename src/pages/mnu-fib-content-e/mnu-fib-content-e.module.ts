import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFibContentEPage } from './mnu-fib-content-e';

@NgModule({
  declarations: [
    MnuFibContentEPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuFibContentEPage),
  ],
})
export class MnuFibContentEPageModule {}
