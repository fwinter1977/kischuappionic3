import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController, AlertController } from 'ionic-angular';
import { SpeicherPage } from '../speicher/speicher';
import { DataProvider } from '../../providers/data/data';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';


/**
 * Generated class for the GoInChecklist01Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-go-in-checklist01',
  templateUrl: 'go-in-checklist01.html',
})

export class GoInChecklist01Page {

  loggedin : boolean; 
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public viewCtrl : ViewController, public data : DataProvider, public alertCtrl : AlertController) {

  }
  public closeAdvice()
  {
    this.viewCtrl.dismiss();
    //this.navCtrl.push(HomePage);
  }
  GoSpeicher() {

    this.loggedin = this.data.getLoggedIn();
    console.log(this.loggedin);
    if (this.loggedin == true)
    {
      this.navCtrl.push(SpeicherPage);
    }
    else
    {
      let alert = this.alertCtrl.create({
        title: 'Achtung',
        subTitle: 'Aus datenschutztechnischen Gründen müssen Sie sich zunächst einloggen, um diese Funktion zu nutzen!',
        buttons: ['Ok']
      });
      alert.present();
    }
    
    }
    /*
    GoLogin()
    {
      this.navCtrl.setRoot(LoginPage);
    }
*/
    GoContent()
    {
      //this.viewCtrl.dismiss();
      this.data.eraseData();
      var GoInPage = this.modalCtrl.create('ContentsChecklist01Page');
      GoInPage.present();
    }
    GoExisting()
    {
      //this.viewCtrl.dismiss();
      //this.data.eraseData();
      var GoInPage = this.modalCtrl.create('ContentsChecklist01Page');
      GoInPage.present();
    }
  ionViewDidLoad() {
    console.log('ionViewDidLoad GoInChecklist01Page');
  }

}
