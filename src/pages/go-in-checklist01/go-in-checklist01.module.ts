import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GoInChecklist01Page } from './go-in-checklist01';

@NgModule({
  declarations: [
    GoInChecklist01Page,
  ],
  imports: [
    IonicPageModule.forChild(GoInChecklist01Page),
  ],
})
export class GoInChecklist01PageModule {}
