import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0325cPage } from './advice0325c';

@NgModule({
  declarations: [
    Advice0325cPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0325cPage),
  ],
})
export class Advice0325cPageModule {}
