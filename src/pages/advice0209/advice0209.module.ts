import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0209Page } from './advice0209';

@NgModule({
  declarations: [
    Advice0209Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0209Page),
  ],
})
export class Advice0209PageModule {}
