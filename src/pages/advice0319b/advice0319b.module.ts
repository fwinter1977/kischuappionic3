import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0319bPage } from './advice0319b';

@NgModule({
  declarations: [
    Advice0319bPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0319bPage),
  ],
})
export class Advice0319bPageModule {}
