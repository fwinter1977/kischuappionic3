import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0157Page } from './advice0157';

@NgModule({
  declarations: [
    Advice0157Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0157Page),
  ],
})
export class Advice0157PageModule {}
