import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0108Page } from './advice0108';

@NgModule({
  declarations: [
    Advice0108Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0108Page),
  ],
})
export class Advice0108PageModule {}
