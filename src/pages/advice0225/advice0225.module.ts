import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0225Page } from './advice0225';

@NgModule({
  declarations: [
    Advice0225Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0225Page),
  ],
})
export class Advice0225PageModule {}
