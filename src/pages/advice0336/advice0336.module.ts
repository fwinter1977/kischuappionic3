import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0336Page } from './advice0336';

@NgModule({
  declarations: [
    Advice0336Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0336Page),
  ],
})
export class Advice0336PageModule {}
