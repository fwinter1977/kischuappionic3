import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdviceFieldPage } from './advice-field';

@NgModule({
  declarations: [
    AdviceFieldPage,
  ],
  imports: [
    IonicPageModule.forChild(AdviceFieldPage),
  ],
})
export class AdviceFieldPageModule {}
