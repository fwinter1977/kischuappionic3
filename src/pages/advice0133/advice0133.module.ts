import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0133Page } from './advice0133';

@NgModule({
  declarations: [
    Advice0133Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0133Page),
  ],
})
export class Advice0133PageModule {}
