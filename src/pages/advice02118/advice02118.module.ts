import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice02118Page } from './advice02118';

@NgModule({
  declarations: [
    Advice02118Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice02118Page),
  ],
})
export class Advice02118PageModule {}
