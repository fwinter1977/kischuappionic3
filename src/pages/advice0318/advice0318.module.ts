import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0318Page } from './advice0318';

@NgModule({
  declarations: [
    Advice0318Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0318Page),
  ],
})
export class Advice0318PageModule {}
