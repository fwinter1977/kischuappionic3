import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0230Page } from './advice0230';

@NgModule({
  declarations: [
    Advice0230Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0230Page),
  ],
})
export class Advice0230PageModule {}
