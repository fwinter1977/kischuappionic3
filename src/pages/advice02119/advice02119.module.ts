import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice02119Page } from './advice02119';

@NgModule({
  declarations: [
    Advice02119Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice02119Page),
  ],
})
export class Advice02119PageModule {}
