import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0324Page } from './advice0324';

@NgModule({
  declarations: [
    Advice0324Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0324Page),
  ],
})
export class Advice0324PageModule {}
