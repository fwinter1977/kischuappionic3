import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0150Page } from './advice0150';

@NgModule({
  declarations: [
    Advice0150Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0150Page),
  ],
})
export class Advice0150PageModule {}
