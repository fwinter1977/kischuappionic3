import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0229Page } from './advice0229';

@NgModule({
  declarations: [
    Advice0229Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0229Page),
  ],
})
export class Advice0229PageModule {}
