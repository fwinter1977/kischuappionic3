import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFicContentBPage } from './mnu-fic-content-b';

@NgModule({
  declarations: [
    MnuFicContentBPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuFicContentBPage),
  ],
})
export class MnuFicContentBPageModule {}
