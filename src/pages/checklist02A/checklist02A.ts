import { Component } from '@angular/core';
import { Nav, Platform, ModalController, NavParams, ViewController } from 'ionic-angular';
import { CheckList02B } from '../checklist02B/checklist02B';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { DataProvider } from '../../providers/data/data';
import { AbspeichernPage } from '../abspeichern/abspeichern';

@Component({
  selector: 'page-checklist02A',
  templateUrl: 'checklist02A.html'

})

export class CheckList02A {

  loadedList: any;
  checked: string;
  checked2: string;
  checked02a: string;
  checked02b: string;
  checked03a: string;
  checked03b: string;
  checked04a: string;
  checked04b: string;
  checked04ba: string;
  checked04bb: string;
  checked04ca: string;
  checked04cb: string;
  checked04da: string;
  checked04db: string;
  checked04ea: string;
  checked04eb: string;
  checked04fa: string;
  checked04fb: string;

  hideMe: boolean;
  hideMe02: boolean;
  hideMe03: boolean;
  hideMe04: boolean;
  hideMe04b: boolean;
  hideMe04c: boolean;
  hideMe04d: boolean;
  hideMe04e: boolean;
  hideMe04f: boolean;
  constructor(public modalCtrl: ModalController, public navCtrl: NavController, public alertCtrl: AlertController, public data: DataProvider,public viewCtrl: ViewController) {
    this.loadedList = this.data.getInitialData();
    if (this.loadedList.length == 0) {
      this.checked = "false";
      this.checked2 = "false";
      this.checked02a = "false";
      this.checked02b = "false";
      this.checked03a = "false";
      this.checked03b = "false";
      this.checked04a = "false";
      this.checked04b = "false";
      this.checked04ba = "false";
      this.checked04bb = "false";
      this.checked04ca = "false";
      this.checked04cb = "false";
      this.checked04da = "false";
      this.checked04db = "false";
      this.checked04ea = "false";
      this.checked04eb = "false";
      this.checked04fa = "false";
      this.checked04fb = "false";

      this.hideMe02 = true;
      this.hideMe03 = true;
      this.hideMe04 = true;
      this.hideMe04b = true;
      this.hideMe04c = true;
      this.hideMe04d = true;
      this.hideMe04e = true;
      this.hideMe04f = true;
    }
    else {
      this.checked = "false";
      this.checked2 = "false";
      this.checked02a = "false";
      this.checked02b = "false";
      this.checked03a = "false";
      this.checked03b = "false";
      this.checked04a = "false";
      this.checked04b = "false";
      this.checked04ba = "false";
      this.checked04bb = "false";
      this.checked04ca = "false";
      this.checked04cb = "false";
      this.checked04da = "false";
      this.checked04db = "false";
      this.checked04ea = "false";
      this.checked04eb = "false";
      this.checked04fa = "false";
      this.checked04fb = "false";

      this.hideMe02 = true;
      this.hideMe03 = true;
      this.hideMe04 = true;
      this.hideMe04b = true;
      this.hideMe04c = true;
      this.hideMe04d = true;
      this.hideMe04e = true;
      this.hideMe04f = true;
      if (this.loadedList[100] != undefined) {
        this.checked = this.loadedList[100].checkeda;
        this.checked2 = this.loadedList[100].checkedb;
        this.hideMe = this.loadedList[100].hidden;
      }
      if (this.loadedList[101] != undefined) {
        this.checked02a = this.loadedList[101].checkeda;
        this.checked02b = this.loadedList[101].checkedb;
        this.hideMe02 = this.loadedList[101].hidden;
      }
      if (this.loadedList[102] != undefined) {
        this.checked03a = this.loadedList[102].checkeda;
        this.checked03b = this.loadedList[102].checkedb;
        this.hideMe03 = this.loadedList[102].hidden;
      }
      if (this.loadedList[103] != undefined) {
        this.checked04a = this.loadedList[103].checkeda;
        this.checked04b = this.loadedList[103].checkedb;
        this.hideMe04 = this.loadedList[103].hidden;
      }
      if (this.loadedList[104] != undefined) {
        this.checked04ba = this.loadedList[104].checkeda;
        this.checked04bb = this.loadedList[104].checkedb;
        this.hideMe04b = this.loadedList[104].hidden;
      }
      if (this.loadedList[105] != undefined) {
        this.checked04ca = this.loadedList[105].checkeda;
        this.checked04cb = this.loadedList[105].checkedb;
        this.hideMe04c = this.loadedList[105].hidden;
      }
      if (this.loadedList[106] != undefined) {
        this.checked04da = this.loadedList[106].checkeda;
        this.checked04db = this.loadedList[106].checkedb;
        this.hideMe04d = this.loadedList[106].hidden;
      }
      if (this.loadedList[107] != undefined) {
        this.checked04ea = this.loadedList[107].checkeda;
        this.checked04eb = this.loadedList[107].checkedb;
        this.hideMe04e = this.loadedList[107].hidden;
      }
      if (this.loadedList[1071] != undefined) {
        this.checked04fa = this.loadedList[1071].checkeda;
        this.checked04fb = this.loadedList[1071].checkedb;
        this.hideMe04f = this.loadedList[1071].hidden;
      }
    }
  }
  reset() {
    const confirm = this.alertCtrl.create({
      title: 'Zurücksetzen',
      message: 'Möchten Sie diesen Teil der Checkliste zurücksetzen?',
      buttons: [
        {
          text: 'Ja',
          handler: () => {
            this.checked = "false";
            this.checked2 = "false";
            this.checked02a = "false";
            this.checked02b = "false";
            this.checked03a = "false";
            this.checked03b = "false";
            this.checked04a = "false";
            this.checked04b = "false";
            this.checked04ba = "false";
            this.checked04bb = "false";
            this.checked04ca = "false";
            this.checked04cb = "false";
            this.checked04da = "false";
            this.checked04db = "false";
            this.checked04ea = "false";
            this.checked04eb = "false";
            this.checked04fa = "false";
            this.checked04fb = "false";
        
            this.hideMe02 = true;
            this.hideMe03 = true;
            this.hideMe04 = true;
            this.hideMe04b = true;
            this.hideMe04c = true;
            this.hideMe04d = true;
            this.hideMe04e = true;
            this.hideMe04f = true;
          }
        },
        {
          text: 'nein',
          handler: () => {
            //console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }
  

  saveList() {

    this.checked = "false";
    this.checked2 = "false";
    this.checked02a = "false";
    this.checked02b = "false";
    this.checked03a = "false";
    this.checked03b = "false";
    this.checked04a = "false";
    this.checked04b = "false";
    this.checked04ba = "false";
    this.checked04bb = "false";
    this.checked04ca = "false";
    this.checked04cb = "false";
    this.checked04da = "false";
    this.checked04db = "false";
    this.checked04ea = "false";
    this.checked04eb = "false";
    this.checked04fa = "false";
    this.checked04fb = "false";

    this.hideMe02 = true;
    this.hideMe03 = true;
    this.hideMe04 = true;
    this.hideMe04b = true;
    this.hideMe04c = true;
    this.hideMe04d = true;
    this.hideMe04e = true;
    this.hideMe04f = true;
    this.navCtrl.setRoot(AbspeichernPage);
  }
  itemHome(event, item) {
    this.viewCtrl.dismiss();
  }

  itemPrev(event, item) {
    this.navCtrl.setRoot(HomePage);
  }
  itemNext(event, item) {
    this.navCtrl.setRoot(CheckList02B);
  }
  ja01() {
    this.checked = "true";
    this.checked2 = "false";
    this.data.setData(100, "true", "false", false);
    this.hideMe02 = false;


  }
  nein01() {
    this.checked = "false";
    this.checked2 = "true";
    this.data.setData(100, "false", "true", false);
    this.hideMe02 = true;

  }

  ja02() {

    this.checked02a = "true";
    this.checked02b = "false";
    this.data.setData(101, "true", "false", false);
    this.hideMe03 = false;
  }
  nein02() {

    this.checked02a = "false";
    this.checked02b = "true";
    this.data.setData(101, "false", "true", false);
    this.hideMe03 = false;
  }
  ja03() {
    this.checked03a = "true";
    this.checked03b = "false";
    this.data.setData(102, "true", "false", false);
    this.hideMe04 = false;
    this.hideMe04d = true;
  }
  nein03() {
    this.checked03a = "false";
    this.checked03b = "true";
    this.data.setData(102, "false", "true", false);
    this.hideMe04 = true;
    this.hideMe04d = false;
  }
  ja04() {
    this.checked04a = "true";
    this.checked04b = "false";
    this.data.setData(103, "true", "false", false);
    this.hideMe04b = false;
  }
  nein04() {
    this.checked04a = "false";
    this.checked04b = "true";
    this.data.setData(103, "false", "true", false);
    this.hideMe04b = false;
  }
  ja04b() {
    this.checked04ba = "true";
    this.checked04bb = "false";
    this.data.setData(104, "true", "false", false);
    this.hideMe04c = false;
  }
  nein04b() {
    this.checked04ba = "false";
    this.checked04bb = "true";
    this.data.setData(104, "false", "true", false);
    this.hideMe04c = false;
  }
  ja04c() {
    this.checked04ca = "true";
    this.checked04cb = "false";
    this.data.setData(105, "true", "false", false);
    this.hideMe04d = false;
  }
  nein04c() {
    this.checked04ca = "false";
    this.checked04cb = "true";
    this.data.setData(105, "false", "true", false);
    this.hideMe04d = true;
    this.Finish();
  }
  ja04d() {
    this.checked04da = "true";
    this.checked04db = "false";
    this.data.setData(106, "true", "false", false);
    this.hideMe04e = false;
  }
  nein04d() {
    this.checked04da = "false";
    this.checked04db = "true";
    this.data.setData(106, "false", "true", false);
    this.hideMe04e = false;
  }
  ja04e() {
    this.checked04ea = "true";
    this.checked04eb = "false";
    this.data.setData(107, "true", "false", false);
    this.hideMe04f = false;
  }
  nein04e() {
    this.checked04ea = "false";
    this.checked04eb = "true";
    this.data.setData(107, "false", "true", false);
    this.hideMe04f = true;
    this.HinweisNext();
  }
  ja04f() {
    this.checked04fa = "true";
    this.checked04fb = "false";
    this.data.setData(1071, "true", "false", false);
    this.HinweisNext();
  }
  nein04f() {
    this.checked04fa = "false";
    this.checked04fb = "true";
    this.data.setData(1071, "false", "true", false);
    this.Finish();
  }
  Hinweis(txt_Hinweis: string) {
    let alert = this.alertCtrl.create({
      title: 'Hinweis',
      subTitle: txt_Hinweis,
      //subTitle: 'Hinweistitel werden noch ergänzt.',
      buttons: ['Ok']
    });

    alert.present();
  }
  HinweisNext() {
    //neue Funktion!
    var data = { message: 'Hinweis' };
    var finishPage = this.modalCtrl.create('AdviceNextPage', data);
    finishPage.present();
  }
  Notizen(qnumber) {
    var notizenPage = this.modalCtrl.create('NotePage', { questionnumber: qnumber });
    notizenPage.present();
  }

  Advice(AdviceNr: string) {
    var advicePage = this.modalCtrl.create('Advice' + AdviceNr + 'Page');
    advicePage.present();
  }

  Finish() {
    //neue Funktion!
    var data = { message: 'Ende des Verfahrens' };
    var finishPage = this.modalCtrl.create('FinishPage', data);
    finishPage.present();
  }
}

