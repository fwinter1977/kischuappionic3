import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';
import { CheckList02C } from '../checklist02C/checklist02C';
import { HomePage } from '../home/home';
import { AlertController } from 'ionic-angular';
import { Nav, Platform, ModalController, NavParams } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { AbspeichernPage } from '../abspeichern/abspeichern';
@Component({
  selector: 'page-checklist02D',
  templateUrl: 'checklist02D.html'
})
export class CheckList02D {
  loadedList: any;
  checked25a : string;
  checked25b : string;
  checked26a : string;
  checked26b : string;
  checked27a : string;
  checked27b : string;
  checked28a : string;
  checked28b : string;
  checked29a : string;
  checked29b : string;
  checked30a : string;
  checked30b : string; 
  hideMe25: boolean;
  hideMe26: boolean;
  hideMe27: boolean;
  hideMe28: boolean;
  hideMe29: boolean;
  hideMe30: boolean;

  constructor(public modalCtrl: ModalController, public navCtrl: NavController, public alertCtrl: AlertController, public data : DataProvider, public viewCtrl: ViewController) {
    this.loadedList = this.data.getInitialData();
    if (this.loadedList.length == 0) {
      this.checked25a = "false";
      this.checked25b = "false";
      this.checked26a = "false";
      this.checked26b = "false";
      this.checked27a = "false";
      this.checked27b = "false";
      this.checked28a = "false";
      this.checked28b = "false";
      this.checked29a = "false";
      this.checked29b = "false";
      this.checked30a = "false";
      this.checked30b = "false";
      this.hideMe26 = true;
      this.hideMe27 = true;
      this.hideMe28 = true;
      this.hideMe29 = true;
      this.hideMe30 = true;
    }
    else {
      this.checked25a = "false";
      this.checked25b = "false";
      this.checked26a = "false";
      this.checked26b = "false";
      this.checked27a = "false";
      this.checked27b = "false";
      this.checked28a = "false";
      this.checked28b = "false";
      this.checked29a = "false";
      this.checked29b = "false";
      this.checked30a = "false";
      this.checked30b = "false";
      this.hideMe26 = true;
      this.hideMe27 = true;
      this.hideMe28 = true;
      this.hideMe29 = true;
      this.hideMe30 = true;

      if (this.loadedList[130] != undefined) {
        this.checked25a = this.loadedList[130].checkeda;
        this.checked25b = this.loadedList[130].checkedb;
        this.hideMe25 = this.loadedList[130].hidden;
      }
      if (this.loadedList[131] != undefined) {
        this.checked26a = this.loadedList[131].checkeda;
        this.checked26b = this.loadedList[131].checkedb;
        this.hideMe26 = this.loadedList[131].hidden;
      }
      if (this.loadedList[132] != undefined) {
        this.checked27a = this.loadedList[132].checkeda;
        this.checked27b = this.loadedList[132].checkedb;
        this.hideMe27 = this.loadedList[132].hidden;
      }
      if (this.loadedList[133] != undefined) {
        this.checked28a = this.loadedList[133].checkeda;
        this.checked28b = this.loadedList[133].checkedb;
        this.hideMe28 = this.loadedList[133].hidden;
      }
      if (this.loadedList[134] != undefined) {
        this.checked29a = this.loadedList[134].checkeda;
        this.checked29b = this.loadedList[134].checkedb;
        this.hideMe29 = this.loadedList[134].hidden;
      }
      if (this.loadedList[135] != undefined) {
        this.checked30a = this.loadedList[135].checkeda;
        this.checked30b = this.loadedList[135].checkedb;
        this.hideMe30 = this.loadedList[135].hidden;
      }
    }
  }
  reset() {
    const confirm = this.alertCtrl.create({
      title: 'Zurücksetzen',
      message: 'Möchten Sie diesen Teil der Checkliste zurücksetzen?',
      buttons: [
        {
          text: 'Ja',
          handler: () => {
            this.checked25a = "false";
            this.checked25b = "false";
            this.checked26a = "false";
            this.checked26b = "false";
            this.checked27a = "false";
            this.checked27b = "false";
            this.checked28a = "false";
            this.checked28b = "false";
            this.checked29a = "false";
            this.checked29b = "false";
            this.checked30a = "false";
            this.checked30b = "false";
            this.hideMe26 = true;
            this.hideMe27 = true;
            this.hideMe28 = true;
            this.hideMe29 = true;
            this.hideMe30 = true;
          }
        },
        {
          text: 'nein',
          handler: () => {
            //console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }
  

  saveList() {

    this.checked25a = "false";
    this.checked25b = "false";
    this.checked26a = "false";
    this.checked26b = "false";
    this.checked27a = "false";
    this.checked27b = "false";
    this.checked28a = "false";
    this.checked28b = "false";
    this.checked29a = "false";
    this.checked29b = "false";
    this.checked30a = "false";
    this.checked30b = "false";
    this.hideMe26 = true;
    this.hideMe27 = true;
    this.hideMe28 = true;
    this.hideMe29 = true;
    this.hideMe30 = true;

    this.navCtrl.push(AbspeichernPage);
  }
  
  itemPrev(event, item) {
    this.navCtrl.push(CheckList02C);
  }
  itemNext(event, item) {
    this.navCtrl.push(CheckList02D);
  }
  itemHome(event, item) {
    this.viewCtrl.dismiss();
  }
  ja25()
  {
    this.checked25a="true";
    this.checked25b="false";
    this.data.setData(130, "true", "false", false);
    this.hideMe28 = false;
    this.hideMe26 = true;
  }
  nein25()
  {
    this.checked25a="false";
    this.checked25b="true";
    this.data.setData(130, "false", "true", false);
    this.hideMe28 = true;
    this.hideMe26 = false;
  }
  ja26()
  {
    this.checked26a="true";
    this.checked26b="false";
    this.data.setData(131, "true", "false", false);
    this.hideMe28 = false;
    this.hideMe27 = true;
  }
  nein26()
  {
    this.checked26a="false";
    this.checked26b="true";
    this.data.setData(131, "false", "true", false);
    this.hideMe28 = true;
    this.hideMe27 = false;
  }
  ja27()
  {
    this.checked27a="true";
    this.checked27b="false";
    this.data.setData(132, "true", "false", false);
    this.hideMe28 = false;
  }
  nein27()
  {
    this.checked27a="false";
    this.checked27b="true";
    this.data.setData(132, "false", "true", false);
    this.hideMe28 = true;
    this.HinweisFeld();
  }
  ja28()
  {
    this.checked28a="true";
    this.checked28b="false";
    this.data.setData(133, "true", "false", false);
    this.hideMe29 = false;
  }
  nein28()
  {
    this.checked28a="false";
    this.checked28b="true";
    this.data.setData(133, "false", "true", false);
    this.hideMe29 = false;
  }
  ja29()
  {
    this.checked29a="true";
    this.checked29b="false";
    this.data.setData(134, "true", "false", false);
    this.hideMe30 = false;
  }
  nein29()
  {
    this.checked29a="false";
    this.checked29b="true";
    this.data.setData(134, "false", "true", false);
    this.hideMe30 = false;
  }
  ja30()
  {
    this.checked30a="true";
    this.checked30b="false";
    this.data.setData(135, "true", "false", false);
    this.FinishJA();
  }
  nein30()
  {
    this.checked30a="false";
    this.checked30b="true";
    this.data.setData(135, "true", "false", false);
  }
  FinishJA() {
    //neue Funktion!
        var data = { message : 'Ende des Verfahrens' };
        var finishPage = this.modalCtrl.create('FinishjamtPage',data);
        finishPage.present();
      }
  Finish() {
    //neue Funktion!
        var data = { message : 'Ende des Verfahrens' };
        var finishPage = this.modalCtrl.create('FinishPage',data);
        finishPage.present();
      }
      HinweisNext() {
        //neue Funktion!
            var data = { message : 'Hinweis' };
            var finishPage = this.modalCtrl.create('AdviceNextPage',data);
            finishPage.present();
          }

  Hinweis(txt_Hinweis: string) {
    let alert = this.alertCtrl.create({
      title: 'Hinweis',
      subTitle: txt_Hinweis,
      //subTitle: 'Hinweistitel werden noch ergänzt.',
      buttons: ['Ok']
    });
    alert.present();
  }
  HinweisFeld() {
    //neue Funktion!
    var data = { message: 'Hinweis' };
    var finishPage = this.modalCtrl.create('AdviceFieldPage', data);
    finishPage.present();
  }
  Notizen(qnumber) 
  {
    var notizenPage = this.modalCtrl.create('NotePage', { questionnumber:qnumber });
    notizenPage.present();
  }
  Advice(AdviceNr: string)
  {
    var advicePage = this.modalCtrl.create('Advice'+AdviceNr+'Page');
    advicePage.present();
  }
}
