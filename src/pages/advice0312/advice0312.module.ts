import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0312Page } from './advice0312';

@NgModule({
  declarations: [
    Advice0312Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0312Page),
  ],
})
export class Advice0312PageModule {}
