import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0107bPage } from './advice0107b';

@NgModule({
  declarations: [
    Advice0107bPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0107bPage),
  ],
})
export class Advice0107bPageModule {}
