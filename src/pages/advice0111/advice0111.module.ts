import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0111Page } from './advice0111';

@NgModule({
  declarations: [
    Advice0111Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0111Page),
  ],
})
export class Advice0111PageModule {}
