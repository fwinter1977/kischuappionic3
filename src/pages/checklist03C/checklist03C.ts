import { Component } from '@angular/core';
import { CheckList03B } from '../checklist03B/checklist03B';
import { CheckList03D } from '../checklist03D/checklist03D';
import { NavController, ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { Nav, Platform, ModalController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { DataProvider } from '../../providers/data/data';
import { AbspeichernPage } from '../abspeichern/abspeichern';

@Component({
  selector: 'page-checklist03C',
  templateUrl: 'checklist03C.html'
})
export class CheckList03C {
  loadedList: any;
  checked16a: string;
  checked16b: string;
  checked17a: string;
  checked17b: string;
  checked18a: string;
  checked18b: string;
  checked19a: string;
  checked19b: string;
  checked19ba: string;
  checked19bb: string;
  checked20a: string;
  checked20b: string;
  checked21a: string;
  checked21b: string;
  checked22a: string;
  checked22b: string;
  checked23a: string;
  checked23b: string;
  checked24a: string;
  checked24b: string;
  checked25a: string;
  checked25b: string;
  checked25ba: string;
  checked25bb: string;
  checked25ca: string;
  checked25cb: string;
  checked25da: string;
  checked25db: string;
  checked25ea: string;
  checked25eb: string;
  hideMe16: boolean;
  hideMe17: boolean;
  hideMe18: boolean;
  hideMe19: boolean;
  hideMe19b: boolean;
  hideMe20: boolean;
  hideMe21: boolean;
  hideMe22: boolean;
  hideMe23: boolean;
  hideMe24: boolean;
  hideMe25: boolean;
  hideMe25b: boolean;
  hideMe25c: boolean;
  hideMe25d: boolean;
  hideMe25e: boolean;
  constructor(public modalCtrl: ModalController, public navCtrl: NavController, public alertCtrl: AlertController, public data: DataProvider, public viewCtrl: ViewController) {
    this.loadedList = this.data.getInitialData();
    if (this.loadedList.length == 0) {
      this.checked16a = "false";
      this.checked16b = "false";
      this.checked17a = "false";
      this.checked17b = "false";
      this.checked18a = "false";
      this.checked18b = "false";
      this.checked19a = "false";
      this.checked19b = "false";
      this.checked19ba = "false";
      this.checked19bb = "false";
      this.checked20a = "false";
      this.checked20b = "false";
      this.checked21a = "false";
      this.checked21b = "false";
      this.checked22a = "false";
      this.checked22b = "false";
      this.checked23a = "false";
      this.checked23b = "false";
      this.checked24a = "false";
      this.checked24b = "false";
      this.checked25a = "false";
      this.checked25b = "false";
      this.checked25ba = "false";
      this.checked25bb = "false";
      this.checked25ca = "false";
      this.checked25cb = "false";
      this.checked25da = "false";
      this.checked25db = "false";
      this.checked25ea = "false";
      this.checked25eb = "false";
      this.hideMe17 = true;
      this.hideMe18 = true;
      this.hideMe19 = true;
      this.hideMe19b = true;
      this.hideMe20 = true;
      this.hideMe21 = true;
      this.hideMe22 = true;
      this.hideMe23 = true;
      this.hideMe24 = true;
      this.hideMe25 = true;
      this.hideMe25b = true;
      this.hideMe25c = true;
      this.hideMe25d = true;
      this.hideMe25e = true;
    }
    else {
      this.checked16a = "false";
      this.checked16b = "false";
      this.checked17a = "false";
      this.checked17b = "false";
      this.checked18a = "false";
      this.checked18b = "false";
      this.checked19a = "false";
      this.checked19b = "false";
      this.checked19ba = "false";
      this.checked19bb = "false";
      this.checked20a = "false";
      this.checked20b = "false";
      this.checked21a = "false";
      this.checked21b = "false";
      this.checked22a = "false";
      this.checked22b = "false";
      this.checked23a = "false";
      this.checked23b = "false";
      this.checked24a = "false";
      this.checked24b = "false";
      this.checked25a = "false";
      this.checked25b = "false";
      this.checked25ba = "false";
      this.checked25bb = "false";
      this.checked25ca = "false";
      this.checked25cb = "false";
      this.checked25da = "false";
      this.checked25db = "false";
      this.checked25ea = "false";
      this.checked25eb = "false";
      this.hideMe17 = true;
      this.hideMe18 = true;
      this.hideMe19 = true;
      this.hideMe19b = true;
      this.hideMe20 = true;
      this.hideMe21 = true;
      this.hideMe22 = true;
      this.hideMe23 = true;
      this.hideMe24 = true;
      this.hideMe25 = true;
      this.hideMe25b = true;
      this.hideMe25c = true;
      this.hideMe25d = true;
      this.hideMe25e = true;

      if (this.loadedList[215] != undefined) {
        this.checked16a = this.loadedList[215].checkeda;
        this.checked16b = this.loadedList[215].checkedb;
        this.hideMe16 = this.loadedList[215].hidden;
      }
      if (this.loadedList[216] != undefined) {
        this.checked17a = this.loadedList[216].checkeda;
        this.checked17b = this.loadedList[216].checkedb;
        this.hideMe17 = this.loadedList[216].hidden;
      }
      if (this.loadedList[217] != undefined) {
        this.checked18a = this.loadedList[217].checkeda;
        this.checked18b = this.loadedList[217].checkedb;
        this.hideMe18 = this.loadedList[217].hidden;
      }
      if (this.loadedList[218] != undefined) {
        this.checked19a = this.loadedList[218].checkeda;
        this.checked19b = this.loadedList[218].checkedb;
        this.hideMe19 = this.loadedList[218].hidden;
      }
      if (this.loadedList[219] != undefined) {
        this.checked19ba = this.loadedList[219].checkeda;
        this.checked19bb = this.loadedList[219].checkedb;
        this.hideMe19b = this.loadedList[219].hidden;
      }
      if (this.loadedList[220] != undefined) {
        this.checked20a = this.loadedList[220].checkeda;
        this.checked20b = this.loadedList[220].checkedb;
        this.hideMe20 = this.loadedList[220].hidden;
      }
      if (this.loadedList[221] != undefined) {
        this.checked21a = this.loadedList[221].checkeda;
        this.checked21b = this.loadedList[221].checkedb;
        this.hideMe21 = this.loadedList[221].hidden;
      }
      if (this.loadedList[222] != undefined) {
        this.checked22a = this.loadedList[222].checkeda;
        this.checked22b = this.loadedList[222].checkedb;
        this.hideMe22 = this.loadedList[222].hidden;
      }
      if (this.loadedList[223] != undefined) {
        this.checked23a = this.loadedList[223].checkeda;
        this.checked23b = this.loadedList[223].checkedb;
        this.hideMe23 = this.loadedList[223].hidden;
      }
      if (this.loadedList[224] != undefined) {
        this.checked24a = this.loadedList[224].checkeda;
        this.checked24b = this.loadedList[224].checkedb;
        this.hideMe24 = this.loadedList[224].hidden;
      }
      if (this.loadedList[225] != undefined) {
        this.checked25a = this.loadedList[225].checkeda;
        this.checked25b = this.loadedList[225].checkedb;
        this.hideMe25 = this.loadedList[225].hidden;
      }
      if (this.loadedList[226] != undefined) {
        this.checked25ba = this.loadedList[226].checkeda;
        this.checked25bb = this.loadedList[226].checkedb;
        this.hideMe25b = this.loadedList[226].hidden;
      }
      if (this.loadedList[227] != undefined) {
        this.checked25ca = this.loadedList[227].checkeda;
        this.checked25cb = this.loadedList[227].checkedb;
        this.hideMe25c = this.loadedList[227].hidden;
      }
      if (this.loadedList[2271] != undefined) {
        this.checked25da = this.loadedList[2271].checkeda;
        this.checked25db = this.loadedList[2271].checkedb;
        this.hideMe25d = this.loadedList[2271].hidden;
      }
      if (this.loadedList[2272] != undefined) {
        this.checked25ea = this.loadedList[2272].checkeda;
        this.checked25eb = this.loadedList[2272].checkedb;
        this.hideMe25e = this.loadedList[2272].hidden;
      }
    }

  }
  reset() {
    const confirm = this.alertCtrl.create({
      title: 'Zurücksetzen',
      message: 'Möchten Sie diesen Teil der Checkliste zurücksetzen?',
      buttons: [
        {
          text: 'Ja',
          handler: () => {
            this.checked16a = "false";
            this.checked16b = "false";
            this.checked17a = "false";
            this.checked17b = "false";
            this.checked18a = "false";
            this.checked18b = "false";
            this.checked19a = "false";
            this.checked19b = "false";
            this.checked19ba = "false";
            this.checked19bb = "false";
            this.checked20a = "false";
            this.checked20b = "false";
            this.checked21a = "false";
            this.checked21b = "false";
            this.checked22a = "false";
            this.checked22b = "false";
            this.checked23a = "false";
            this.checked23b = "false";
            this.checked24a = "false";
            this.checked24b = "false";
            this.checked25a = "false";
            this.checked25b = "false";
            this.checked25ba = "false";
            this.checked25bb = "false";
            this.checked25ca = "false";
            this.checked25cb = "false";
            this.checked25da = "false";
            this.checked25db = "false";
            this.checked25ea = "false";
            this.checked25eb = "false";
            this.hideMe17 = true;
            this.hideMe18 = true;
            this.hideMe19 = true;
            this.hideMe19b = true;
            this.hideMe20 = true;
            this.hideMe21 = true;
            this.hideMe22 = true;
            this.hideMe23 = true;
            this.hideMe24 = true;
            this.hideMe25 = true;
            this.hideMe25b = true;
            this.hideMe25c = true;
            this.hideMe25d = true;
            this.hideMe25e = true;
          }
        },
        {
          text: 'nein',
          handler: () => {
            //console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

  saveList() {

    this.checked16a = "false";
    this.checked16b = "false";
    this.checked17a = "false";
    this.checked17b = "false";
    this.checked18a = "false";
    this.checked18b = "false";
    this.checked19a = "false";
    this.checked19b = "false";
    this.checked19ba = "false";
    this.checked19bb = "false";
    this.checked20a = "false";
    this.checked20b = "false";
    this.checked21a = "false";
    this.checked21b = "false";
    this.checked22a = "false";
    this.checked22b = "false";
    this.checked23a = "false";
    this.checked23b = "false";
    this.checked24a = "false";
    this.checked24b = "false";
    this.checked25a = "false";
    this.checked25b = "false";
    this.checked25ba = "false";
    this.checked25bb = "false";
    this.checked25ca = "false";
    this.checked25cb = "false";
    this.checked25da = "false";
    this.checked25db = "false";
    this.checked25ea = "false";
    this.checked25eb = "false";
    this.hideMe17 = true;
    this.hideMe18 = true;
    this.hideMe19 = true;
    this.hideMe19b = true;
    this.hideMe20 = true;
    this.hideMe21 = true;
    this.hideMe22 = true;
    this.hideMe23 = true;
    this.hideMe24 = true;
    this.hideMe25 = true;
    this.hideMe25b = true;
    this.hideMe25c = true;
    this.hideMe25d = true;
    this.hideMe25e = true;
    this.navCtrl.setRoot(AbspeichernPage);
  }





  itemPrev(event, item) {
    this.navCtrl.push(CheckList03B);
  }
  itemNext(event, item) {
    this.navCtrl.push(CheckList03D);
  }
  itemHome(event, item) {
    this.viewCtrl.dismiss();
  }
  ja16() {
    this.checked16a = "true";
    this.checked16b = "false";
    this.data.setData(215, "true", "false", false);
    this.hideMe17 = false;

  }
  nein16() {
    this.checked16a = "false";
    this.checked16b = "true";
    this.data.setData(215, "false", "true", false);
    this.hideMe17 = true;
    this.Finish();

  }

  ja17() {

    this.checked17a = "true";
    this.checked17b = "false";
    this.data.setData(216, "true", "false", false);
    this.Finish();
    this.hideMe18 = true;

  }
  nein17() {

    this.checked17a = "false";
    this.checked17b = "true";
    this.data.setData(216, "false", "true", false);
    this.hideMe18 = false;
  }
  ja18() {

    this.checked18a = "true";
    this.checked18b = "false";
    this.data.setData(217, "true", "false", false);
    this.hideMe19 = false;
  }
  nein18() {

    this.checked18a = "false";
    this.checked18b = "true";
    this.data.setData(217, "false", "true", false);
    this.hideMe19 = false;
  }
  ja19() {

    this.checked19a = "true";
    this.checked19b = "false";
    this.data.setData(218, "true", "false", false);
    this.hideMe19b = true;
    this.HinweisJugendamt();
  }
  nein19() {

    this.checked19a = "false";
    this.checked19b = "true";
    this.data.setData(218, "false", "true", false);
    this.hideMe19b = false;

  }
  ja19b() {

    this.checked19ba = "true";
    this.checked19bb = "false";
    this.data.setData(219, "true", "false", false);
    this.hideMe20 = false;
  }
  nein19b() {

    this.checked19ba = "false";
    this.checked19bb = "true";
    this.data.setData(219, "false", "true", false);
    this.hideMe20 = false;

  }
  ja20() {

    this.checked20a = "true";
    this.checked20b = "false";
    this.data.setData(220, "true", "false", false);
    this.hideMe21 = false;
    
  }
  nein20() {

    this.checked20a = "false";
    this.checked20b = "true";
    this.data.setData(220, "false", "true", false);
    this.hideMe21 = true;
    this.HinweisNext();

  }
  ja21() {

    this.checked21a = "true";
    this.checked21b = "false";
    this.data.setData(221, "true", "false", false);
    this.hideMe25c = false;
    this.hideMe22 = true;
  }
  nein21() {

    this.checked21a = "false";
    this.checked21b = "true";
    this.data.setData(221, "false", "true", false);
    this.hideMe22 = false;
    this.hideMe25c = true;

  }
  ja22() {

    this.checked22a = "true";
    this.checked22b = "false";
    this.data.setData(222, "true", "false", false);
    this.hideMe25c = false;
    this.hideMe23 = true;
  }
  nein22() {

    this.checked22a = "false";
    this.checked22b = "true";
    this.data.setData(222, "false", "true", false);
    this.hideMe23 = false;
    this.hideMe25c = true;
  }
  ja23() {

    this.checked23a = "true";
    this.checked23b = "false";
    this.data.setData(223, "true", "false", false);
    this.hideMe25c = false;
    this.hideMe24 = true;
  }
  nein23() {

    this.checked23a = "false";
    this.checked23b = "true";
    this.data.setData(223, "false", "true", false);
    this.hideMe24 = false;
    this.hideMe25c = true;

  }
  ja24() {

    this.checked24a = "true";
    this.checked24b = "false";
    this.data.setData(224, "true", "false", false);
    this.hideMe25c = false;
    this.hideMe25 = true;
  }
  nein24() {

    this.checked24a = "false";
    this.checked24b = "true";
    this.data.setData(224, "false", "true", false);
    this.hideMe25 = false;
    this.hideMe25c = true;

  }
  ja25() {

    this.checked25a = "true";
    this.checked25b = "false";
    this.data.setData(225, "true", "false", false);
    this.HinweisNext();
  }
  nein25() {

    this.checked25a = "false";
    this.checked25b = "true";
    this.data.setData(225, "false", "true", false);
    this.HinweisFeld();
  }
  ja25b() {

    this.checked25ba = "true";
    this.checked25bb = "false";
    this.data.setData(226, "true", "false", false);
    this.Finish();
  }
  nein25b() {

    this.checked25ba = "false";
    this.checked25b = "true";
    this.data.setData(226, "false", "true", false);
    this.HinweisNext();
  }
  ja25c() {

    this.checked25ca = "true";
    this.checked25cb = "false";
    this.data.setData(227, "true", "false", false);
    this.hideMe25d = false;

  }

  nein25c() {

    this.checked25ca = "false";
    this.checked25cb = "true";
    this.data.setData(227, "false", "true", false);
    this.hideMe25d = true;
  }
  ja25d() {

    this.checked25da = "true";
    this.checked25db = "false";
    this.data.setData(2271, "true", "false", false);
    this.hideMe25e = false;

  }

  nein25d() {

    this.checked25da = "false";
    this.checked25db = "true";
    this.data.setData(2271, "false", "true", false);
    this.hideMe25e = true;
    this.HinweisNext();
  }
  ja25e() {

    this.checked25ea = "true";
    this.checked25eb = "false";
    this.data.setData(2272, "true", "false", false);
    this.Finish();

  }

  nein25e() {

    this.checked25ea = "false";
    this.checked25eb = "true";
    this.data.setData(2272, "false", "true", false);
    this.HinweisNext();
  }
  Finish() {
    //neue Funktion!
    var data = { message: 'Ende des Verfahrens' };
    var finishPage = this.modalCtrl.create('FinishPage', data);
    finishPage.present();
  }

  HinweisNext() {
    //neue Funktion!
    var data = { message: 'Hinweis' };
    var finishPage = this.modalCtrl.create('AdviceNextPage', data);
    finishPage.present();
  }
  HinweisFeld() {
    //neue Funktion!
    var data = { message: 'Hinweis' };
    var finishPage = this.modalCtrl.create('AdviceFieldPage', data);
    finishPage.present();
  } 
  HinweisJugendamt() {
    //neue Funktion!
    var data = { message: 'Hinweis' };
    var finishPage = this.modalCtrl.create('AdviceJugendamtPage', data);
    finishPage.present();
  }
  Hinweis(txt_Hinweis: string) {
    let alert = this.alertCtrl.create({
      title: 'Hinweis',
      subTitle: txt_Hinweis,
      //subTitle: 'Hinweistitel werden noch ergänzt.',
      buttons: ['Ok']
    });
    alert.present();
  }
  Notizen(qnumber) {
    console.log(qnumber);
    var notizenPage = this.modalCtrl.create('NotePage', { questionnumber: qnumber });
    notizenPage.present();
  }
  Advice(AdviceNr: string) {
    var advicePage = this.modalCtrl.create('Advice' + AdviceNr + 'Page');
    advicePage.present();
  }
}
