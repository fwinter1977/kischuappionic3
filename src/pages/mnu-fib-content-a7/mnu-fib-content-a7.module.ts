import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFibContentA7Page } from './mnu-fib-content-a7';

@NgModule({
  declarations: [
    MnuFibContentA7Page,
  ],
  imports: [
    IonicPageModule.forChild(MnuFibContentA7Page),
  ],
})
export class MnuFibContentA7PageModule {}
