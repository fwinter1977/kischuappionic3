import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0136Page } from './advice0136';

@NgModule({
  declarations: [
    Advice0136Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0136Page),
  ],
})
export class Advice0136PageModule {}
