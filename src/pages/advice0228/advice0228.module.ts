import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0228Page } from './advice0228';

@NgModule({
  declarations: [
    Advice0228Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0228Page),
  ],
})
export class Advice0228PageModule {}
