import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdviceJugendamtPage } from './advice-jugendamt';

@NgModule({
  declarations: [
    AdviceJugendamtPage,
  ],
  imports: [
    IonicPageModule.forChild(AdviceJugendamtPage),
  ],
})
export class AdviceJugendamtPageModule {}
