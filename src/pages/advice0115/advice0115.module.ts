import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0115Page } from './advice0115';

@NgModule({
  declarations: [
    Advice0115Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0115Page),
  ],
})
export class Advice0115PageModule {}
