import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFibContentA3Page } from './mnu-fib-content-a3';

@NgModule({
  declarations: [
    MnuFibContentA3Page,
  ],
  imports: [
    IonicPageModule.forChild(MnuFibContentA3Page),
  ],
})
export class MnuFibContentA3PageModule {}
