import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContentsChecklist01Page } from './contents-checklist01';

@NgModule({
  declarations: [
    ContentsChecklist01Page,
  ],
  imports: [
    IonicPageModule.forChild(ContentsChecklist01Page),
  ],
})
export class ContentsChecklist01PageModule {}
