import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { CheckList01 } from '../checklist01/checklist01';
import { CheckList01B } from '../checklist01B/checklist01B';
import { CheckList01E } from '../checklist01E/checklist01E';
import { CheckList01C } from '../checklist01C/checklist01C';
import { CheckList01D } from '../checklist01D/checklist01D';
import { HomePage } from '../home/home';

/**
 * Generated class for the ContentsChecklist01Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contents-checklist01',
  templateUrl: 'contents-checklist01.html',
})
export class ContentsChecklist01Page {

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public viewCtrl : ViewController) {
  
  }

  public closeAdvice()
  {
    //this.navCtrl.push(HomePage);
    this.viewCtrl.dismiss().then(() => this.navCtrl.setRoot(HomePage)); 
  }
  Start() {
    //this.viewCtrl.dismiss();
    this.navCtrl.push(CheckList01);
  }

  Start2() {
    //this.viewCtrl.dismiss();
    this.navCtrl.push(CheckList01B);
  }
  Start3() {
    //this.viewCtrl.dismiss();
    this.navCtrl.push(CheckList01C);
  }
  Start4() {
    //this.viewCtrl.dismiss();
    this.navCtrl.push(CheckList01D);
  }
  Start5() {
    //this.viewCtrl.dismiss();
    var GoInPage = this.modalCtrl.create('ContentsChecklist01Mldg01Page');
    GoInPage.present();
  }
  GoContent()
  {
    //this.viewCtrl.dismiss();
    var GoInPage = this.modalCtrl.create('ContentsChecklist01Page');
    GoInPage.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContentsChecklist01Page');
  }

}
