import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Registration02Page } from '../registration02/registration02';
import { LoginPage } from '../login/login';
import { Storage} from '@ionic/storage';

/**
 * Generated class for the RegistrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})



export class RegistrationPage {
  checked : string;
  notRegistered:boolean;
    
    

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, public storage: Storage) {

    this.checked = "false";
  }
    ja01()
    {
      this.checked="true";
    }
  
    goRegistration02() {
      if (this.checked=="true")
      {
        this.storage.get('notRegistered').then((_1st) => {
          this.notRegistered = _1st;
          if(this.notRegistered==true)
          {
            this.Warning();
          }
          else
          {
            this.storage.set('notRegistered', true);
          }
        });
        this.navCtrl.push(Registration02Page);
      }
  }
Warning() {
  let alert = this.alertCtrl.create({
    title: 'Warnung',
    subTitle: 'Wenn Sie fortfahren wird der bisherige Nutzer samt Daten gelöscht. Wenn Sie dies verhindern wollen klicken Sie bitte im folgenden Screen auf Abbrechen.',
    buttons: ['Verstanden']
  });
  alert.present();
}
  exitApp()
  {
    this.navCtrl.push(LoginPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrationPage');
  }

}

