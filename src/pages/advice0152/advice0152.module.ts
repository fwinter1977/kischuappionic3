import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0152Page } from './advice0152';

@NgModule({
  declarations: [
    Advice0152Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0152Page),
  ],
})
export class Advice0152PageModule {}
