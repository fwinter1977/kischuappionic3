import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuNbPage } from './mnu-nb';

@NgModule({
  declarations: [
    MnuNbPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuNbPage),
  ],
})
export class MnuNbPageModule {}
