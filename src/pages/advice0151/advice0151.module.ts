import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0151Page } from './advice0151';

@NgModule({
  declarations: [
    Advice0151Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0151Page),
  ],
})
export class Advice0151PageModule {}
