import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFibContentA6Page } from './mnu-fib-content-a6';

@NgModule({
  declarations: [
    MnuFibContentA6Page,
  ],
  imports: [
    IonicPageModule.forChild(MnuFibContentA6Page),
  ],
})
export class MnuFibContentA6PageModule {}
