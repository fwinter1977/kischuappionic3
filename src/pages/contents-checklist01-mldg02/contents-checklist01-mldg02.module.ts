import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContentsChecklist01Mldg02Page } from './contents-checklist01-mldg02';

@NgModule({
  declarations: [
    ContentsChecklist01Mldg02Page,
  ],
  imports: [
    IonicPageModule.forChild(ContentsChecklist01Mldg02Page),
  ],
})
export class ContentsChecklist01Mldg02PageModule {}
