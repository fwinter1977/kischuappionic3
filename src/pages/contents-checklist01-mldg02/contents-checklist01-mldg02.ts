import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { CheckList01 } from '../checklist01/checklist01';
import { CheckList01B } from '../checklist01B/checklist01B';
import { CheckList01C } from '../checklist01C/checklist01C';
import { CheckList01D } from '../checklist01D/checklist01D';
import { HomePage } from '../home/home';

/**
 * Generated class for the ContentsChecklist01Mldg02Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contents-checklist01-mldg02',
  templateUrl: 'contents-checklist01-mldg02.html',
})
export class ContentsChecklist01Mldg02Page {

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public viewCtrl : ViewController) {
  }

  public closeAdvice()
  {
    //this.viewCtrl.dismiss();
    this.navCtrl.setRoot(HomePage);
  }
  Start() {
    this.navCtrl.push(CheckList01);
  }

  Start2() {
    this.navCtrl.push(CheckList01B);
  }
  Start3() {
    this.navCtrl.push(CheckList01C);
  }
  Start4() {
    this.navCtrl.push(CheckList01D);
  }
  Start5() {
    var GoInPage = this.modalCtrl.create('ContentsChecklist01Mldg01Page');
    GoInPage.present();
  }
  GoContent()
  {
    var GoInPage = this.modalCtrl.create('ContentsChecklist01Page');
    GoInPage.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContentsChecklist01Page');
  }

}
