import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0154Page } from './advice0154';

@NgModule({
  declarations: [
    Advice0154Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0154Page),
  ],
})
export class Advice0154PageModule {}
