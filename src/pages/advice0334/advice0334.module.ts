import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0334Page } from './advice0334';

@NgModule({
  declarations: [
    Advice0334Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0334Page),
  ],
})
export class Advice0334PageModule {}
