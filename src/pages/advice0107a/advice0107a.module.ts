import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0107aPage } from './advice0107a';

@NgModule({
  declarations: [
    Advice0107aPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0107aPage),
  ],
})
export class Advice0107aPageModule {}
