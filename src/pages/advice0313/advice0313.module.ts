import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0313Page } from './advice0313';

@NgModule({
  declarations: [
    Advice0313Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0313Page),
  ],
})
export class Advice0313PageModule {}
