import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0123bPage } from './advice0123b';

@NgModule({
  declarations: [
    Advice0123bPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0123bPage),
  ],
})
export class Advice0123bPageModule {}
