import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0142bPage } from './advice0142b';

@NgModule({
  declarations: [
    Advice0142bPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0142bPage),
  ],
})
export class Advice0142bPageModule {}
