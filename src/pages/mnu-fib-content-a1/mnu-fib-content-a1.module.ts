import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFibContentA1Page } from './mnu-fib-content-a1';

@NgModule({
  declarations: [
    MnuFibContentA1Page,
  ],
  imports: [
    IonicPageModule.forChild(MnuFibContentA1Page),
  ],
})
export class MnuFibContentA1PageModule {}
