import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0335Page } from './advice0335';

@NgModule({
  declarations: [
    Advice0335Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0335Page),
  ],
})
export class Advice0335PageModule {}
