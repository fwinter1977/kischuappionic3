import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';


@Component({
  selector: 'page-impressum',
  templateUrl: 'impressum.html'
})
export class ImpressumPage {



  constructor(public navCtrl: NavController) {

  }
  /*
  ngOnInit(){
    setTimeout(() => {
      // bestimmt den "TimOut" für die Dauer des Intros und springt nach xxxxx ms in die HomePage
      this.navCtrl.setRoot(HomePage);
    }, 15000);
  }
  */
 public Back()
 {
  this.navCtrl.setRoot(LoginPage);
 }
}
