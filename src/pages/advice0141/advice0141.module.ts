import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0141Page } from './advice0141';

@NgModule({
  declarations: [
    Advice0141Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0141Page),
  ],
})
export class Advice0141PageModule {}
