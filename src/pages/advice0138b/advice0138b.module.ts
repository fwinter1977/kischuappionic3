import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0138bPage } from './advice0138b';

@NgModule({
  declarations: [
    Advice0138bPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0138bPage),
  ],
})
export class Advice0138bPageModule {}
