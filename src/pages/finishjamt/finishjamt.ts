import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';

/**
 * Generated class for the FinishjamtPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-finishjamt',
  templateUrl: 'finishjamt.html',
})
export class FinishjamtPage {
  public closeFinish()
  {
    this.viewCtrl.dismiss();
  }
  
  
    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl : ViewController, public modalCtrl : ModalController) {
    }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad FinishPage');
    }
    goNext() {
      var GoInPage = this.modalCtrl.create('Finish02Page');
      GoInPage.present();
    }
  }
  