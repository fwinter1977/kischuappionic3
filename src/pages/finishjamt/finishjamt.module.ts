import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinishjamtPage } from './finishjamt';

@NgModule({
  declarations: [
    FinishjamtPage,
  ],
  imports: [
    IonicPageModule.forChild(FinishjamtPage),
  ],
})
export class FinishjamtPageModule {}
