import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0224Page } from './advice0224';

@NgModule({
  declarations: [
    Advice0224Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0224Page),
  ],
})
export class Advice0224PageModule {}
