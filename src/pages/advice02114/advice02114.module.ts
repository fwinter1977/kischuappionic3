import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice02114Page } from './advice02114';

@NgModule({
  declarations: [
    Advice02114Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice02114Page),
  ],
})
export class Advice02114PageModule {}
