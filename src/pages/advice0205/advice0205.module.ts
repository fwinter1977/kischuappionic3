import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0205Page } from './advice0205';

@NgModule({
  declarations: [
    Advice0205Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0205Page),
  ],
})
export class Advice0205PageModule {}
