import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0302Page } from './advice0302';

@NgModule({
  declarations: [
    Advice0302Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0302Page),
  ],
})
export class Advice0302PageModule {}
