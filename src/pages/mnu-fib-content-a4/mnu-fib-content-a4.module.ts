import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFibContentA4Page } from './mnu-fib-content-a4';

@NgModule({
  declarations: [
    MnuFibContentA4Page,
  ],
  imports: [
    IonicPageModule.forChild(MnuFibContentA4Page),
  ],
})
export class MnuFibContentA4PageModule {}
