import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';
import { CheckList01C } from '../checklist01C/checklist01C';
import { CheckList01E } from '../checklist01E/checklist01E';
//import { HomePage } from '../home/home';
import { AlertController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { AbspeichernPage } from '../abspeichern/abspeichern';
@Component({
  selector: 'page-checklist01D',
  templateUrl: 'checklist01D.html'
})
export class CheckList01D {
  loadedList: any;
  checked36a : string;
  checked36b : string;
  checked37a : string;
  checked37b : string;
  checked38a : string;
  checked38b : string;
  checked38ba : string;
  checked38bb : string
  checked39a : string;
  checked39b : string;
  checked40a : string;
  checked40b : string;
  checked41a : string;
  checked41b : string;
  checked42a : string;
  checked42b : string;
  checked43a : string;
  checked43b : string;
  checked44a : string;
  checked44b : string;
  checked45a : string;
  checked45b : string;
  checked46a : string;
  checked46b : string;
  checked47a : string;
  checked47b : string;
  checked48a : string;
  checked48b : string;
  checked49aa : string;
  checked49ab : string;
  checked49a : string;
  checked49b : string;
  checked50a : string;
  checked50b : string;
  checked51a : string;
  checked51b : string;
  checked52a : string;
  checked52b : string;
  checked53a : string;
  checked53b : string;
  checked54a : string;
  checked54b : string;

  //neue Fragen
  checked39aa : string;
  checked39ab : string;
  checked40ba : string;
  checked40bb : string;
  checked42ba : string;
  checked42bb : string;
  checked44ba : string;
  checked44bb : string;
  checked46ba : string;
  checked46bb : string;

  hideMe36: boolean;
  hideMe37: boolean;
  hideMe38: boolean;
  hideMe38b: boolean;
  hideMe39: boolean;
  hideMe40: boolean;
  hideMe41: boolean;
  hideMe42: boolean;
  hideMe43: boolean;
  hideMe44: boolean;
  hideMe45: boolean;
  hideMe46: boolean;
  hideMe47: boolean;
  hideMe48: boolean;
  hideMe49a: boolean;
  hideMe49: boolean;
  hideMe50: boolean;
  hideMe51: boolean;
  hideMe52: boolean;
  hideMe53: boolean;
  hideMe54: boolean;

  //neue Fragen
  hideMe39a: boolean;
  hideMe40b: boolean;
  hideMe42b: boolean;
  hideMe44b: boolean;
  hideMe46b: boolean;

  constructor(public modalCtrl: ModalController, public navCtrl: NavController, public alertCtrl: AlertController, public data : DataProvider,public viewCtrl: ViewController) {
    this.loadedList = this.data.getInitialData();
    if (this.loadedList.length == 0) {
      this.checked36a = "false";
      this.checked36b = "false";
      this.checked37a = "false";
      this.checked37b = "false";
      this.checked38a = "false";
      this.checked38b = "false";
      this.checked38ba = "false";
      this.checked38bb = "false";
      this.checked39a = "false";
      this.checked39b = "false";
      this.checked40a = "false";
      this.checked40b = "false";
      this.checked41a = "false";
      this.checked41b = "false";
      this.checked42a = "false";
      this.checked42b = "false";
      this.checked43a = "false";
      this.checked43b = "false";
      this.checked44a = "false";
      this.checked44b = "false";
      this.checked45a = "false";
      this.checked45b = "false";
      this.checked46a = "false";
      this.checked46b = "false";
      this.checked47a = "false";
      this.checked47b = "false";
      this.checked48a = "false";
      this.checked48b = "false";
      this.checked49aa = "false";
      this.checked49ab = "false";
      this.checked49a = "false";
      this.checked49b = "false";
      this.checked50a = "false";
      this.checked50b = "false";
      this.checked51a = "false";
      this.checked51b = "false";
      this.checked52a = "false";
      this.checked52b = "false";
      this.checked53a = "false";
      this.checked53b = "false";
      this.checked54a = "false";
      this.checked54b = "false";

//neue Fragen
      this.checked39aa = "false";
      this.checked39ab = "false";
      this.checked40ba = "false";
      this.checked40bb = "false";
      this.checked42ba = "false";
      this.checked42bb = "false";
      this.checked44ba = "false";
      this.checked44bb = "false";
      this.checked46ba = "false";
      this.checked46bb = "false";

      this.hideMe39a = true;
      this.hideMe40b = true;
      this.hideMe42b = true;
      this.hideMe44b = true;
      this.hideMe46b = true;

      this.hideMe37 = true;
      this.hideMe38 = true;
      this.hideMe38b = true;
      this.hideMe39 = true;
      this.hideMe40 = true;
      this.hideMe41 = true;
      this.hideMe42 = true;
      this.hideMe43 = true;
      this.hideMe44 = true;
      this.hideMe45 = true;
      this.hideMe46 = true;
      this.hideMe47 = true;
      this.hideMe48 = true;
      this.hideMe49a = true;
      this.hideMe49 = true;
      this.hideMe50 = true;
      this.hideMe51 = true;
      this.hideMe52 = true;
      this.hideMe53 = true;
      this.hideMe54 = true;

      
    }
    else {
      this.checked36a = "false";
      this.checked36b = "false";
      this.checked37a = "false";
      this.checked37b = "false";
      this.checked38a = "false";
      this.checked38b = "false";
      this.checked38ba = "false";
      this.checked38bb = "false";
      this.checked39a = "false";
      this.checked39b = "false";
      this.checked40a = "false";
      this.checked40b = "false";
      this.checked41a = "false";
      this.checked41b = "false";
      this.checked42a = "false";
      this.checked42b = "false";
      this.checked43a = "false";
      this.checked43b = "false";
      this.checked44a = "false";
      this.checked44b = "false";
      this.checked45a = "false";
      this.checked45b = "false";
      this.checked46a = "false";
      this.checked46b = "false";
      this.checked47a = "false";
      this.checked47b = "false";
      this.checked48a = "false";
      this.checked48b = "false";
      this.checked49aa = "false";
      this.checked49ab = "false";
      this.checked49a = "false";
      this.checked49b = "false";
      this.checked50a = "false";
      this.checked50b = "false";
      this.checked51a = "false";
      this.checked51b = "false";
      this.checked52a = "false";
      this.checked52b = "false";
      this.checked53a = "false";
      this.checked53b = "false";
      this.checked54a = "false";
      this.checked54b = "false";


      this.checked39aa = "false";
      this.checked39ab = "false";
      this.checked40ba = "false";
      this.checked40bb = "false";
      this.checked42ba = "false";
      this.checked42bb = "false";
      this.checked44ba = "false";
      this.checked44bb = "false";
      this.checked46ba = "false";
      this.checked46bb = "false";

      this.hideMe39a = true;
      this.hideMe40b = true;
      this.hideMe42b = true;
      this.hideMe44b = true;
      this.hideMe46b = true;


      this.hideMe37 = true;
      this.hideMe38 = true;
      this.hideMe38b = true;
      this.hideMe39 = true;
      this.hideMe40 = true;
      this.hideMe41 = true;
      this.hideMe42 = true;
      this.hideMe43 = true;
      this.hideMe44 = true;
      this.hideMe45 = true;
      this.hideMe46 = true;
      this.hideMe47 = true;
      this.hideMe48 = true;
      this.hideMe49a = true;
      this.hideMe49 = true;
      this.hideMe50 = true;
      this.hideMe51 = true;
      this.hideMe52 = true;
      this.hideMe53 = true;
      this.hideMe54 = true;

      if (this.loadedList[37] != undefined) {
        this.checked36a = this.loadedList[37].checkeda;
        this.checked36b = this.loadedList[37].checkedb;
        this.hideMe36 = this.loadedList[37].hidden;
      }
      if (this.loadedList[38] != undefined) {
        this.checked37a = this.loadedList[38].checkeda;
        this.checked37b = this.loadedList[38].checkedb;
        this.hideMe37 = this.loadedList[38].hidden;
      }
      if (this.loadedList[39] != undefined) {
        this.checked38a = this.loadedList[39].checkeda;
        this.checked38b = this.loadedList[39].checkedb;
        this.hideMe38 = this.loadedList[39].hidden;
      }
      if (this.loadedList[40] != undefined) {
        this.checked38ba = this.loadedList[40].checkeda;
        this.checked38bb = this.loadedList[40].checkedb;
        this.hideMe38b = this.loadedList[40].hidden;
      }
      if (this.loadedList[41] != undefined) {
        this.checked39a = this.loadedList[41].checkeda;
        this.checked39b = this.loadedList[41].checkedb;
        this.hideMe39 = this.loadedList[41].hidden;
      }
      if (this.loadedList[42] != undefined) {
        this.checked40a = this.loadedList[42].checkeda;
        this.checked40b = this.loadedList[42].checkedb;
        this.hideMe40 = this.loadedList[42].hidden;
      }
      if (this.loadedList[43] != undefined) {
        this.checked41a = this.loadedList[43].checkeda;
        this.checked41b = this.loadedList[43].checkedb;
        this.hideMe41 = this.loadedList[43].hidden;
      }
      if (this.loadedList[44] != undefined) {
        this.checked42a = this.loadedList[44].checkeda;
        this.checked42b = this.loadedList[44].checkedb;
        this.hideMe42 = this.loadedList[44].hidden;
      }
      if (this.loadedList[45] != undefined) {
        this.checked43a = this.loadedList[45].checkeda;
        this.checked43b = this.loadedList[45].checkedb;
        this.hideMe43 = this.loadedList[45].hidden;
      }
      if (this.loadedList[46] != undefined) {
        this.checked44a = this.loadedList[46].checkeda;
        this.checked44b = this.loadedList[46].checkedb;
        this.hideMe44 = this.loadedList[46].hidden;
      }
      if (this.loadedList[47] != undefined) {
        this.checked45a = this.loadedList[47].checkeda;
        this.checked45b = this.loadedList[47].checkedb;
        this.hideMe45 = this.loadedList[47].hidden;
      }
      if (this.loadedList[48] != undefined) {
        this.checked46a = this.loadedList[48].checkeda;
        this.checked46b = this.loadedList[48].checkedb;
        this.hideMe46 = this.loadedList[48].hidden;
      }
      if (this.loadedList[49] != undefined) {
        this.checked47a = this.loadedList[49].checkeda;
        this.checked47b = this.loadedList[49].checkedb;
        this.hideMe47 = this.loadedList[49].hidden;
      }
      if (this.loadedList[50] != undefined) {
        this.checked48a = this.loadedList[50].checkeda;
        this.checked48b = this.loadedList[50].checkedb;
        this.hideMe48 = this.loadedList[50].hidden;
      }
      if (this.loadedList[51] != undefined) {
        this.checked49aa = this.loadedList[51].checkeda;
        this.checked49ab = this.loadedList[51].checkedb;
        this.hideMe49a = this.loadedList[51].hidden;
      }
      //Frage ist raus?
      if (this.loadedList[52] != undefined) {
        this.checked49a = this.loadedList[52].checkeda;
        this.checked49b = this.loadedList[52].checkedb;
        this.hideMe49 = this.loadedList[52].hidden;
      }
      //Frage ist raus?
      if (this.loadedList[53] != undefined) {
        this.checked50a = this.loadedList[53].checkeda;
        this.checked50b = this.loadedList[53].checkedb;
        this.hideMe50 = this.loadedList[53].hidden;
      }
      if (this.loadedList[54] != undefined) {
        this.checked51a = this.loadedList[54].checkeda;
        this.checked51b = this.loadedList[54].checkedb;
        this.hideMe51 = this.loadedList[54].hidden;
      }
      if (this.loadedList[55] != undefined) {
        this.checked52a = this.loadedList[55].checkeda;
        this.checked52b = this.loadedList[55].checkedb;
        this.hideMe52 = this.loadedList[55].hidden;
      }
      if (this.loadedList[56] != undefined) {
        this.checked53a = this.loadedList[56].checkeda;
        this.checked53b = this.loadedList[56].checkedb;
        this.hideMe53 = this.loadedList[56].hidden;
      }
      if (this.loadedList[57] != undefined) {
        this.checked54a = this.loadedList[57].checkeda;
        this.checked54b = this.loadedList[57].checkedb;
        this.hideMe54 = this.loadedList[57].hidden;
      }
      if (this.loadedList[65] != undefined) {
        this.checked39aa = this.loadedList[65].checkeda;
        this.checked39ab = this.loadedList[65].checkedb;
        this.hideMe39a = this.loadedList[65].hidden;
      }

      if (this.loadedList[66] != undefined) {
        this.checked40ba = this.loadedList[66].checkeda;
        this.checked40bb = this.loadedList[66].checkedb;
        this.hideMe40b = this.loadedList[66].hidden;
      }

      if (this.loadedList[67] != undefined) {
        this.checked42ba = this.loadedList[67].checkeda;
        this.checked42bb = this.loadedList[67].checkedb;
        this.hideMe42b = this.loadedList[67].hidden;
      }

      if (this.loadedList[68] != undefined) {
        this.checked44ba = this.loadedList[68].checkeda;
        this.checked44bb = this.loadedList[68].checkedb;
        this.hideMe44b = this.loadedList[68].hidden;
      }

      if (this.loadedList[69] != undefined) {
        this.checked46ba = this.loadedList[69].checkeda;
        this.checked46bb = this.loadedList[69].checkedb;
        this.hideMe46b = this.loadedList[69].hidden;
      }
    }
  }
    /*
    this.checked36a = "false";
    this.checked36b = "false";
    this.checked37a = "false";
    this.checked37b = "false";
    this.checked38a = "false";
    this.checked38b = "false";
    this.checked38ba = "false";
    this.checked38bb = "false";
    this.checked39a = "false";
    this.checked39b = "false";
    this.checked40a = "false";
    this.checked40b = "false";
    this.checked41a = "false";
    this.checked41b = "false";
    this.checked42a = "false";
    this.checked42b = "false";
    this.checked43a = "false";
    this.checked43b = "false";
    this.checked44a = "false";
    this.checked44b = "false";
    this.checked45a = "false";
    this.checked45b = "false";
    this.checked46a = "false";
    this.checked46b = "false";
    this.checked47a = "false";
    this.checked47b = "false";
    this.checked48a = "false";
    this.checked48b = "false";
    this.checked49aa = "false";
    this.checked49ab = "false";
    this.checked49a = "false";
    this.checked49b = "false";
    this.checked50a = "false";
    this.checked50b = "false";
    this.checked51a = "false";
    this.checked51b = "false";
    this.checked52a = "false";
    this.checked52b = "false";
    this.checked53a = "false";
    this.checked53b = "false";
    this.checked54a = "false";
    this.checked54b = "false";
    this.hideMe37 = true;
    this.hideMe38 = true;
    this.hideMe38b = true;
    this.hideMe39 = true;
    this.hideMe40 = true;
    this.hideMe41 = true;
    this.hideMe42 = true;
    this.hideMe43 = true;
    this.hideMe44 = true;
    this.hideMe45 = true;
    this.hideMe46 = true;
    this.hideMe47 = true;
    this.hideMe48 = true;
    this.hideMe49a = true;
    this.hideMe49 = true;
    this.hideMe50 = true;
    this.hideMe51 = true;
    this.hideMe52 = true;
    this.hideMe53 = true;
    this.hideMe54 = true;
  }
  */

 reset() {
  const confirm = this.alertCtrl.create({
    title: 'Zurücksetzen',
    message: 'Möchten Sie diesen Teil der Checkliste zurücksetzen?',
    buttons: [
      {
        text: 'Ja',
        handler: () => {
          this.checked36a = "false";
          this.checked36b = "false";
          this.checked37a = "false";
          this.checked37b = "false";
          this.checked38a = "false";
          this.checked38b = "false";
          this.checked38ba = "false";
          this.checked38bb = "false";
          this.checked39a = "false";
          this.checked39b = "false";
          this.checked40a = "false";
          this.checked40b = "false";
          this.checked41a = "false";
          this.checked41b = "false";
          this.checked42a = "false";
          this.checked42b = "false";
          this.checked43a = "false";
          this.checked43b = "false";
          this.checked44a = "false";
          this.checked44b = "false";
          this.checked45a = "false";
          this.checked45b = "false";
          this.checked46a = "false";
          this.checked46b = "false";
          this.checked47a = "false";
          this.checked47b = "false";
          this.checked48a = "false";
          this.checked48b = "false";
          this.checked49aa = "false";
          this.checked49ab = "false";
          this.checked49a = "false";
          this.checked49b = "false";
          this.checked50a = "false";
          this.checked50b = "false";
          this.checked51a = "false";
          this.checked51b = "false";
          this.checked52a = "false";
          this.checked52b = "false";
          this.checked53a = "false";
          this.checked53b = "false";
          this.checked54a = "false";
          this.checked54b = "false";
          this.hideMe37 = true;
          this.hideMe38 = true;
          this.hideMe38b = true;
          this.hideMe39 = true;
          this.hideMe40 = true;
          this.hideMe41 = true;
          this.hideMe42 = true;
          this.hideMe43 = true;
          this.hideMe44 = true;
          this.hideMe45 = true;
          this.hideMe46 = true;
          this.hideMe47 = true;
          this.hideMe48 = true;
          this.hideMe49a = true;
          this.hideMe49 = true;
          this.hideMe50 = true;
          this.hideMe51 = true;
          this.hideMe52 = true;
          this.hideMe53 = true;
          this.hideMe54 = true;

          this.checked39aa = "false";
          this.checked39ab = "false";
          this.checked40ba = "false";
          this.checked40bb = "false";
          this.checked42ba = "false";
          this.checked42bb = "false";
          this.checked44ba = "false";
          this.checked44bb = "false";
          this.checked46ba = "false";
          this.checked46bb = "false";
    
          this.hideMe39a = true;
          this.hideMe40b = true;
          this.hideMe42b = true;
          this.hideMe44b = true;
          this.hideMe46b = true;
    
        }
      },
      {
        text: 'nein',
        handler: () => {
          //console.log('Agree clicked');
        }
      }
    ]
  });
  confirm.present();
}

 saveList() {

  this.checked36a = "false";
  this.checked36b = "false";
  this.checked37a = "false";
  this.checked37b = "false";
  this.checked38a = "false";
  this.checked38b = "false";
  this.checked38ba = "false";
  this.checked38bb = "false";
  this.checked39a = "false";
  this.checked39b = "false";
  this.checked40a = "false";
  this.checked40b = "false";
  this.checked41a = "false";
  this.checked41b = "false";
  this.checked42a = "false";
  this.checked42b = "false";
  this.checked43a = "false";
  this.checked43b = "false";
  this.checked44a = "false";
  this.checked44b = "false";
  this.checked45a = "false";
  this.checked45b = "false";
  this.checked46a = "false";
  this.checked46b = "false";
  this.checked47a = "false";
  this.checked47b = "false";
  this.checked48a = "false";
  this.checked48b = "false";
  this.checked49aa = "false";
  this.checked49ab = "false";
  this.checked49a = "false";
  this.checked49b = "false";
  this.checked50a = "false";
  this.checked50b = "false";
  this.checked51a = "false";
  this.checked51b = "false";
  this.checked52a = "false";
  this.checked52b = "false";
  this.checked53a = "false";
  this.checked53b = "false";
  this.checked54a = "false";
  this.checked54b = "false";
  this.hideMe37 = true;
  this.hideMe38 = true;
  this.hideMe38b = true;
  this.hideMe39 = true;
  this.hideMe40 = true;
  this.hideMe41 = true;
  this.hideMe42 = true;
  this.hideMe43 = true;
  this.hideMe44 = true;
  this.hideMe45 = true;
  this.hideMe46 = true;
  this.hideMe47 = true;
  this.hideMe48 = true;
  this.hideMe49a = true;
  this.hideMe49 = true;
  this.hideMe50 = true;
  this.hideMe51 = true;
  this.hideMe52 = true;
  this.hideMe53 = true;
  this.hideMe54 = true;

  this.checked39aa = "false";
  this.checked39ab = "false";
  this.checked40ba = "false";
  this.checked40bb = "false";
  this.checked42ba = "false";
  this.checked42bb = "false";
  this.checked44ba = "false";
  this.checked44bb = "false";
  this.checked46ba = "false";
  this.checked46bb = "false";

  this.hideMe39a = true;
  this.hideMe40b = true;
  this.hideMe42b = true;
  this.hideMe44b = true;
  this.hideMe46b = true;


  this.navCtrl.push(AbspeichernPage);
}


  itemPrev(event, item) {
    this.navCtrl.push(CheckList01C);
  }
  itemNext(event, item) {
    this.navCtrl.push(CheckList01E);
  }
  itemHome(event, item) {
    this.viewCtrl.dismiss();
  }
  ja36()
  {
    this.checked36a="true";
    this.checked36b="false";
    this.data.setData(37, "true", "false", false);
    this.Finish();
    this.hideMe37=true;
  }
  nein36()
  {
    this.checked36a="false";
    this.checked36b="true";
    this.data.setData(37, "false", "true", false);
    this.hideMe37=false;

  }
  ja37()
  {
    this.checked37a="true";
    this.checked37b="false";
    this.data.setData(38, "true", "false", false);
    this.hideMe38=false;

  }
  nein37()
  {
    this.checked37a="false";
    this.checked37b="true";
    this.data.setData(38, "true", "´false", false);
    this.hideMe38=false;

  }
  ja38()
  {
    this.checked38a="true";
    this.checked38b="false";
    this.data.setData(39, "true", "false", false);
    this.hideMe39=true;
    this.HinweisJugendamt();
  }
  nein38()
  {
    this.checked38a="false";
    this.checked38b="true";
    this.data.setData(39, "false", "true", false);
    this.hideMe39=false;
  }
  ja38b()
  {
    this.checked38ba="true";
    this.checked38bb="false";
    this.data.setData(40, "true", "false", false);
    this.hideMe39a=true;
    this.HinweisNext();
  }
  nein38b()
  {
    this.checked38ba="false";
    this.checked38bb="true";
    this.data.setData(40, "false", "true", false);
    this.hideMe39a=false;

  }

  ja39()
  {
    this.checked39a="true";
    this.checked39b="false";
    this.data.setData(41, "true", "false", false);
    this.hideMe39a=false;
  }
  nein39()
  {
    this.checked39a="false";
    this.checked39b="true";
    this.data.setData(41, "false", "true", false);
    this.hideMe39a=false;
  }

  ja39a()
  {
    this.checked39aa="true";
    this.checked39ab="false";
    this.data.setData(65, "true", "false", false);
    this.hideMe40=false;
  }
  nein39a()
  {
    this.checked39aa="false";
    this.checked39ab="true";
    this.data.setData(65, "false", "true", false);
    this.hideMe40=true;
    this.HinweisNext();
  }

  ja40()
  {
    this.checked40a="true";
    this.checked40b="false";
    this.data.setData(42, "true", "false", false);
    this.hideMe40b=false;
    this.hideMe42=true;
  }
  nein40()
  {
    this.checked40a="false";
    this.checked40b="true";
    this.data.setData(42, "false", "true", false);
    this.hideMe40b=true;
    this.hideMe42=false;
  }

  nein40b()
  {
    this.checked40ba="false";
    this.checked40bb="true";
    this.data.setData(66, "false", "true", false);
    this.hideMe41=true;
    this.hideMe42=false;
  }

  ja40b()
  {
    this.checked40ba="true";
    this.checked40bb="false";
    this.data.setData(66, "true", "false", false);
    this.hideMe41=false;
    this.hideMe42=true;
  }

  ja41()
  {
    this.checked41a="true";
    this.checked41b="false";
    this.data.setData(43, "true", "false", false);
    this.hideMe50 = false;
  }
  nein41()
  {
    this.checked41a="false";
    this.checked41b="true";
    this.data.setData(43, "false", "true", false);
    this.hideMe50 = false;
  }
  ja42()
  {
    this.checked42a="true";
    this.checked42b="false";
    this.data.setData(44, "true", "false", false);
    this.hideMe42b=false;
    this.hideMe44=true;
  }
  nein42()
  {
    this.checked42a="false";
    this.checked42b="true";
    this.data.setData(44, "false", "true", false);
    this.hideMe42b=true;
    this.hideMe44=false;
  }
  ja42b()
  {
    this.checked42ba="true";
    this.checked42bb="false";
    this.data.setData(67, "true", "false", false);
    this.hideMe43=false;
    this.hideMe44=true;
  }
  nein42b()
  {
    this.checked42ba="false";
    this.checked42bb="true";
    this.data.setData(67, "false", "true", false);
    this.hideMe43=true;
    this.hideMe44=false;
  }
  ja43()
  {
    this.checked43a="true";
    this.checked43b="false";
    this.data.setData(45, "true", "false", false);
    this.hideMe50=false;
  }
  nein43()
  {
    this.checked43a="false";
    this.checked43b="true";
    this.data.setData(45, "false", "true", false);
    this.hideMe50=false;
  }
  ja44()
  {
    this.checked44a="true";
    this.checked44b="false";
    this.data.setData(46, "true", "false", false);
    this.hideMe44b=false;
    this.hideMe46=true;
  }
  nein44()
  {
    this.checked44a="false";
    this.checked44b="true";
    this.data.setData(46, "false", "true", false);
    this.hideMe44b=true;
    this.hideMe46=false;
  }
  ja44b()
  {
    this.checked44ba="true";
    this.checked44bb="false";
    this.data.setData(68, "true", "false", false);
    this.hideMe45=false;
    this.hideMe46=true;
  }
  nein44b()
  {
    this.checked44ba="false";
    this.checked44bb="true";
    this.data.setData(68, "false", "true", false);
    this.hideMe45=true;
    this.hideMe46=false;
  }
  ja45()
  {
    this.checked45a="true";
    this.checked45b="false";
    this.data.setData(47, "true", "false", false);
    this.hideMe50=false;
  }
  nein45()
  {
    this.checked45a="false";
    this.checked45b="true";
    this.data.setData(47, "false", "true", false);
    this.hideMe50=false;
  }
  ja46()
  {
    this.checked46a="true";
    this.checked46b="false";
    this.data.setData(48, "true", "false", false);
    this.hideMe46b=false;
    this.hideMe49a=true;
  }
  nein46()
  {
    this.checked46a="false";
    this.checked46b="true";
    this.data.setData(48, "false", "true", false);
    this.hideMe46b=true;
    this.hideMe49a=false;
  }
  ja46b()
  {
    this.checked46ba="true";
    this.checked46bb="false";
    this.data.setData(69, "true", "false", false);
    this.hideMe47=false;
    this.hideMe49a=true;
  }
  nein46b()
  {
    this.checked46ba="false";
    this.checked46bb="true";
    this.data.setData(69, "false", "true", false);
    this.hideMe47=true;
    this.hideMe49a=false;
  }
  ja47()
  {
    this.checked47a="true";
    this.checked47b="false";
    this.data.setData(49, "true", "false", false);
    this.hideMe48=false;
    this.hideMe50=true;
  }
  nein47()
  {
    this.checked47a="false";
    this.checked47b="true";
    this.data.setData(49, "false", "true", false);
    this.hideMe48=true;
    this.hideMe50=false;

  }
  ja48()
  {
    this.checked48a="true";
    this.checked48b="false";
    this.data.setData(50, "true", "false", false);
    this.hideMe50=false;

  }
  nein48()
  {
    this.checked48a="false";
    this.checked48b="true";
    this.data.setData(50, "false", "true", false);
    this.hideMe50=false;

  }
  ja49a()
  {
    this.checked49aa="true";
    this.checked49ab="false";
    this.data.setData(51, "true", "false", false);
    this.HinweisNext();
  }
  nein49a()
  {
    this.checked49aa="false";
    this.checked49ab="true";
    this.data.setData(51, "false", "true", false);
    this.HinweisFeld();
  }
  ja49()
  {
    this.checked49a="true";
    this.checked49b="false";
    this.data.setData(52, "true", "false", false);
  }
  nein49()
  {
    this.checked49a="false";
    this.checked49b="true";
    this.data.setData(52, "false", "true", false);
    this.hideMe50=false;
    this.hideMe51=false;
    this.hideMe52=false;
    this.hideMe53=false;
    this.hideMe54=false;
  }
  ja50()
  {
    this.checked50a="true";
    this.checked50b="false";
    this.data.setData(53, "true", "false", false);
    this.hideMe51 = false;
  }
  nein50()
  {
    this.checked50a="false";
    this.checked50b="true";
    this.data.setData(53, "false", "true", false);
    this.hideMe51 = false;
  }
  nein51()
  {
    this.checked51a="false";
    this.checked51b="true";
    this.data.setData(54, "false", "true", false);
    this.hideMe54 = false;
    this.hideMe52 = true;
  }
  ja51()
  {
    this.checked51a="true";
    this.checked51b="false";
    this.data.setData(54, "true", "false", false);
    this.hideMe52 = false;
    this.hideMe54 = true;
  }

  ja52()
  {
    this.checked52a="true";
    this.checked52b="false";
    this.data.setData(55, "true", "false", false);
    this.hideMe53 = false;
  }
  nein52()
  {
    this.checked52a="false";
    this.checked52b="true";
    this.data.setData(55, "false", "true", false);
    this.hideMe53 = false;
  }
  ja53()
  {
    this.checked53a="true";
    this.checked53b="false";
    this.data.setData(56, "true", "false", false);
    this.hideMe54 = false;
  }
  nein53()
  {
    this.checked53a="false";
    this.checked53b="true";
    this.data.setData(56, "false", "true", false);
    this.hideMe54 = false;
  }
  ja54()
  {
    this.checked54a="true";
    this.checked54b="false";
    this.data.setData(57, "true", "false", false);
    this.Finish();
  }
  nein54()
  {
    this.checked54a="false";
    this.checked54b="true";
    this.data.setData(57, "false", "true", false);
    this.HinweisNext();
  }
  Finish() {
    //neue Funktion!
        var data = { message : 'Ende des Verfahrens' };
        var finishPage = this.modalCtrl.create('FinishPage',data);
        finishPage.present();
      }
      HinweisNext() {
        //neue Funktion!
        var data = { message: 'Hinweis' };
        var finishPage = this.modalCtrl.create('AdviceNextPage', data);
        finishPage.present();
      }
      HinweisJugendamt() {
        //neue Funktion!
        var data = { message: 'Hinweis' };
        var finishPage = this.modalCtrl.create('AdviceJugendamtPage', data);
        finishPage.present();
      }
    
      HinweisFeld() {
        //neue Funktion!
        var data = { message: 'Hinweis' };
        var finishPage = this.modalCtrl.create('AdviceFieldPage', data);
        finishPage.present();
      }
  Hinweis(txt_Hinweis: string) {
    let alert = this.alertCtrl.create({
      title: 'Hinweis',
      subTitle: txt_Hinweis,
      //subTitle: 'Hinweistitel werden noch ergänzt.',
      buttons: ['Ok']
    });
    alert.present();
  }
  Notizen(qnumber) 
  {
    var notizenPage = this.modalCtrl.create('NotePage', { questionnumber:qnumber });
    notizenPage.present();
  }
  Advice(AdviceNr: string)
  {
    var advicePage = this.modalCtrl.create('Advice'+AdviceNr+'Page');
    advicePage.present();
  }
}
