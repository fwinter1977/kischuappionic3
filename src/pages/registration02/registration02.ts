import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { Storage} from '@ionic/storage';
import { HomePage } from '../home/home';
/**
 * Generated class for the Registration02Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registration02',
  templateUrl: 'registration02.html',
})
export class Registration02Page {
  username:string;
  pw1:any;
  pw2:any;
  errormessage:string;
  pwl:string;
  unl:string;
      

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {
  }
  register()
  {
    
    
    if (this.pw1==this.pw2)
    {
      this.pwl = this.pw1;
      this.unl = this.username;
      if ((this.pwl.length < 5) || (this.unl.length < 5))
      {
        this.errormessage="Passwort und Nutzername müssen mindestens aus 5 Zeichen bestehen!"
      }
      else
      {
        this.storage.set('username', this.username);
        this.errormessage="";
        this.storage.set('password', this.pw1)
        this.navCtrl.push(HomePage);
      }
    }
    else
    {
      this.errormessage="die Passwörter stimmen nicht überein!"
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad Registration02Page');
  }
  goHome() {
    this.navCtrl.push(LoginPage);
    }
}
