import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0321Page } from './advice0321';

@NgModule({
  declarations: [
    Advice0321Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0321Page),
  ],
})
export class Advice0321PageModule {}
