import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice02112Page } from './advice02112';

@NgModule({
  declarations: [
    Advice02112Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice02112Page),
  ],
})
export class Advice02112PageModule {}
