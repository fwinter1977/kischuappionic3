import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFibContentBPage } from './mnu-fib-content-b';

@NgModule({
  declarations: [
    MnuFibContentBPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuFibContentBPage),
  ],
})
export class MnuFibContentBPageModule {}
