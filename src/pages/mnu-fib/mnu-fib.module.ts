import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFibPage } from './mnu-fib';

@NgModule({
  declarations: [
    MnuFibPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuFibPage),
  ],
})
export class MnuFibPageModule {}
