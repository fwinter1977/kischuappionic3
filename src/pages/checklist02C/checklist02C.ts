import { Component } from '@angular/core';
import { CheckList02B } from '../checklist02B/checklist02B';
import { CheckList02D } from '../checklist02D/checklist02D';
import { NavController, ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { Nav, Platform, ModalController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { DataProvider } from '../../providers/data/data';
import { AbspeichernPage } from '../abspeichern/abspeichern';

@Component({
  selector: 'page-checklist02C',
  templateUrl: 'checklist02C.html'
})
export class CheckList02C {
  loadedList: any;
  checked09a: string;
  checked09b: string;
  checked09ba: string;
  checked09bb: string;
  checked10a: string;
  checked10b: string;
  checked12ba: string;
  checked12bb: string;
  checked12a: string;
  checked12b: string;
  checked13a: string;
  checked13b: string;
  checked14a: string;
  checked14b: string;
  checked15a: string;
  checked15b: string;
  checked16a: string;
  checked16b: string;
  checked17a: string;
  checked17b: string;
  checked18a: string;
  checked18b: string;
  checked19a: string;
  checked19b: string;
  checked20a: string;
  checked20b: string;
  checked21a: string;
  checked21b: string;
  checked22a: string;
  checked22b: string;
  checked23a: string;
  checked23b: string;
  checked24a: string;
  checked24b: string;
  hideMe09: boolean;
  hideMe09b: boolean;
  hideMe10: boolean;

//neue Fragen
checked15ba: string;
checked15bb: string;
checked19ba: string;
checked19bb: string;
checked22aa: string;
checked22ab: string;
hideMe15b: boolean;
hideMe19b: boolean;
hideMe22a: boolean;

  hideMe12b: boolean;
  hideMe12: boolean;
  hideMe13: boolean;
  hideMe14: boolean;
  hideMe15: boolean;
  hideMe16: boolean;
  hideMe17: boolean;
  hideMe18: boolean;
  hideMe19: boolean;
  hideMe20: boolean;
  hideMe21: boolean;
  hideMe22: boolean;
  hideMe23: boolean;
  hideMe24: boolean;

  constructor(public modalCtrl: ModalController, public navCtrl: NavController, public alertCtrl: AlertController, public data: DataProvider, public viewCtrl: ViewController) {

    this.loadedList = this.data.getInitialData();
    if (this.loadedList.length == 0) {
      this.checked09a = "false";
      this.checked09b = "false";
      this.checked09ba = "false";
      this.checked09bb = "false";
      this.checked10a = "false";
      this.checked10b = "false";
      this.checked12ba = "false";
      this.checked12bb = "false";
      this.checked12a = "false";
      this.checked12b = "false";
      this.checked13a = "false";
      this.checked13b = "false";
      this.checked14a = "false";
      this.checked14b = "false";
      this.checked15a = "false";
      this.checked15b = "false";
      this.checked16a = "false";
      this.checked16b = "false";
      this.checked17a = "false";
      this.checked17b = "false";
      this.checked18a = "false";
      this.checked18b = "false";
      this.checked19a = "false";
      this.checked19b = "false";
      this.checked20a = "false";
      this.checked20b = "false";
      this.checked21a = "false";
      this.checked21b = "false";
      this.checked22a = "false";
      this.checked22b = "false";
      this.checked23a = "false";
      this.checked23b = "false";
      this.checked24a = "false";
      this.checked24b = "false";

//neue Fragen
this.checked15ba = "false";
this.checked15bb = "false";
this.checked19a = "false";
this.checked19b = "false";
this.checked19ba = "false";
this.checked19bb = "false";
this.checked22aa = "false";
this.checked22ab = "false";
this.hideMe15b = true;
this.hideMe19b = true;
this.hideMe22a = true;


      this.hideMe09b = true;
      this.hideMe10 = true;
      this.hideMe12b = true;
      this.hideMe12 = true;
      this.hideMe13 = true;
      this.hideMe14 = true;
      this.hideMe15 = true;
      this.hideMe16 = true;
      this.hideMe17 = true;
      this.hideMe18 = true;
      this.hideMe19 = true;
      this.hideMe20 = true;
      this.hideMe21 = true;
      this.hideMe22 = true;
      this.hideMe23 = true;
      this.hideMe24 = true;
    }
    else {
      this.checked09a = "false";
      this.checked09b = "false";
      this.checked09ba = "false";
      this.checked09bb = "false";
      this.checked10a = "false";
      this.checked10b = "false";
      this.checked12ba = "false";
      this.checked12bb = "false";
      this.checked12a = "false";
      this.checked12b = "false";
      this.checked13a = "false";
      this.checked13b = "false";
      this.checked14a = "false";
      this.checked14b = "false";
      this.checked15a = "false";
      this.checked15b = "false";
      this.checked16a = "false";
      this.checked16b = "false";
      this.checked17a = "false";
      this.checked17b = "false";
      this.checked18a = "false";
      this.checked18b = "false";
      this.checked19a = "false";
      this.checked19b = "false";
      this.checked20a = "false";
      this.checked20b = "false";
      this.checked21a = "false";
      this.checked21b = "false";
      this.checked22a = "false";
      this.checked22b = "false";
      this.checked23a = "false";
      this.checked23b = "false";
      this.checked24a = "false";
      this.checked24b = "false";

      //neue Fragen
this.checked19ba = "false";
this.checked19bb = "false";
this.checked15ba = "false";
this.checked15bb = "false";
this.checked19a = "false";
this.checked19b = "false";
this.checked22aa = "false";
this.checked22ab = "false";
this.hideMe15b = true;
this.hideMe19b = true;
this.hideMe22a = true;

      this.hideMe09b = true;
      this.hideMe10 = true;
      this.hideMe12b = true;
      this.hideMe12 = true;
      this.hideMe13 = true;
      this.hideMe14 = true;
      this.hideMe15 = true;
      this.hideMe16 = true;
      this.hideMe17 = true;
      this.hideMe18 = true;
      this.hideMe19 = true;
      this.hideMe20 = true;
      this.hideMe21 = true;
      this.hideMe22 = true;
      this.hideMe23 = true;
      this.hideMe24 = true;

      if (this.loadedList[113] != undefined) {
        this.checked09a = this.loadedList[113].checkeda;
        this.checked09b = this.loadedList[113].checkedb;
        this.hideMe09 = this.loadedList[113].hidden;
      }
      if (this.loadedList[114] != undefined) {
        this.checked09ba = this.loadedList[114].checkeda;
        this.checked09bb = this.loadedList[114].checkedb;
        this.hideMe09b = this.loadedList[114].hidden;
      }
      if (this.loadedList[115] != undefined) {
        this.checked10a = this.loadedList[115].checkeda;
        this.checked10b = this.loadedList[115].checkedb;
        this.hideMe10 = this.loadedList[115].hidden;
      }
      if (this.loadedList[116] != undefined) {
        this.checked12ba = this.loadedList[116].checkeda;
        this.checked12bb = this.loadedList[116].checkedb;
        this.hideMe12b = this.loadedList[116].hidden;
      }
      if (this.loadedList[117] != undefined) {
        this.checked12a = this.loadedList[117].checkeda;
        this.checked12b = this.loadedList[117].checkedb;
        this.hideMe12 = this.loadedList[117].hidden;
      }
      if (this.loadedList[118] != undefined) {
        this.checked13a = this.loadedList[118].checkeda;
        this.checked13b = this.loadedList[118].checkedb;
        this.hideMe13 = this.loadedList[118].hidden;
      }
      if (this.loadedList[119] != undefined) {
        this.checked14a = this.loadedList[119].checkeda;
        this.checked14b = this.loadedList[119].checkedb;
        this.hideMe14 = this.loadedList[119].hidden;
      }
      if (this.loadedList[120] != undefined) {
        this.checked15a = this.loadedList[120].checkeda;
        this.checked15b = this.loadedList[120].checkedb;
        this.hideMe15 = this.loadedList[120].hidden;
      }
      if (this.loadedList[121] != undefined) {
        this.checked16a = this.loadedList[121].checkeda;
        this.checked16b = this.loadedList[121].checkedb;
        this.hideMe16 = this.loadedList[121].hidden;
      }
      if (this.loadedList[122] != undefined) {
        this.checked17a = this.loadedList[122].checkeda;
        this.checked17b = this.loadedList[122].checkedb;
        this.hideMe17 = this.loadedList[122].hidden;
      }
      if (this.loadedList[123] != undefined) {
        this.checked18a = this.loadedList[123].checkeda;
        this.checked18b = this.loadedList[123].checkedb;
        this.hideMe18 = this.loadedList[123].hidden;
      }
      if (this.loadedList[124] != undefined) {
        this.checked19a = this.loadedList[124].checkeda;
        this.checked19b = this.loadedList[124].checkedb;
        this.hideMe19 = this.loadedList[124].hidden;
      }
      if (this.loadedList[125] != undefined) {
        this.checked20a = this.loadedList[125].checkeda;
        this.checked20b = this.loadedList[125].checkedb;
        this.hideMe20 = this.loadedList[125].hidden;
      }
      if (this.loadedList[126] != undefined) {
        this.checked21a = this.loadedList[126].checkeda;
        this.checked21b = this.loadedList[126].checkedb;
        this.hideMe21 = this.loadedList[126].hidden;
      }
      if (this.loadedList[127] != undefined) {
        this.checked22a = this.loadedList[127].checkeda;
        this.checked22b = this.loadedList[127].checkedb;
        this.hideMe22 = this.loadedList[127].hidden;
      }
      if (this.loadedList[128] != undefined) {
        this.checked23a = this.loadedList[128].checkeda;
        this.checked23b = this.loadedList[128].checkedb;
        this.hideMe23 = this.loadedList[128].hidden;
      }
      if (this.loadedList[129] != undefined) {
        this.checked24a = this.loadedList[129].checkeda;
        this.checked24b = this.loadedList[129].checkedb;
        this.hideMe24 = this.loadedList[129].hidden;
      }

      if (this.loadedList[36] != undefined) {
        this.checked15ba = this.loadedList[136].checkeda;
        this.checked15bb = this.loadedList[136].checkedb;
        this.hideMe15b = this.loadedList[136].hidden;
      }

      if (this.loadedList[137] != undefined) {
        this.checked19ba = this.loadedList[137].checkeda;
        this.checked19bb = this.loadedList[137].checkedb;
        this.hideMe19b = this.loadedList[137].hidden;
      }

      if (this.loadedList[138] != undefined) {
        this.checked22aa = this.loadedList[138].checkeda;
        this.checked22ab = this.loadedList[138].checkedb;
        this.hideMe22a = this.loadedList[138].hidden;
      }
    }
  }
  reset() {
    const confirm = this.alertCtrl.create({
      title: 'Zurücksetzen',
      message: 'Möchten Sie diesen Teil der Checkliste zurücksetzen?',
      buttons: [
        {
          text: 'Ja',
          handler: () => {
            this.checked09a = "false";
            this.checked09b = "false";
            this.checked09ba = "false";
            this.checked09bb = "false";
            this.checked10a = "false";
            this.checked10b = "false";
            this.checked12ba = "false";
            this.checked12bb = "false";
            this.checked12a = "false";
            this.checked12b = "false";
            this.checked13a = "false";
            this.checked13b = "false";
            this.checked14a = "false";
            this.checked14b = "false";
            this.checked15a = "false";
            this.checked15b = "false";
            this.checked16a = "false";
            this.checked16b = "false";
            this.checked17a = "false";
            this.checked17b = "false";
            this.checked18a = "false";
            this.checked18b = "false";
            this.checked19a = "false";
            this.checked19b = "false";
            this.checked20a = "false";
            this.checked20b = "false";
            this.checked21a = "false";
            this.checked21b = "false";
            this.checked22a = "false";
            this.checked22b = "false";
            this.checked23a = "false";
            this.checked23b = "false";
            this.checked24a = "false";
            this.checked24b = "false";

            //neue Fragen
this.checked15ba = "false";
this.checked15bb = "false";
this.checked19a = "false";
this.checked19b = "false";
this.checked22aa = "false";
this.checked22ab = "false";
this.hideMe15b = true;
this.hideMe19b = true;
this.hideMe22a = true;

            this.hideMe09b = true;
            this.hideMe10 = true;
            this.hideMe12b = true;
            this.hideMe12 = true;
            this.hideMe13 = true;
            this.hideMe14 = true;
            this.hideMe15 = true;
            this.hideMe16 = true;
            this.hideMe17 = true;
            this.hideMe18 = true;
            this.hideMe19 = true;
            this.hideMe20 = true;
            this.hideMe21 = true;
            this.hideMe22 = true;
            this.hideMe23 = true;
            this.hideMe24 = true;
          }
        },
        {
          text: 'nein',
          handler: () => {
            //console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }
  
    saveList() {

      this.checked09a = "false";
      this.checked09b = "false";
      this.checked09ba = "false";
      this.checked09bb = "false";
      this.checked10a = "false";
      this.checked10b = "false";
      this.checked12ba = "false";
      this.checked12bb = "false";
      this.checked12a = "false";
      this.checked12b = "false";
      this.checked13a = "false";
      this.checked13b = "false";
      this.checked14a = "false";
      this.checked14b = "false";
      this.checked15a = "false";
      this.checked15b = "false";
      this.checked16a = "false";
      this.checked16b = "false";
      this.checked17a = "false";
      this.checked17b = "false";
      this.checked18a = "false";
      this.checked18b = "false";
      this.checked19a = "false";
      this.checked19b = "false";
      this.checked20a = "false";
      this.checked20b = "false";
      this.checked21a = "false";
      this.checked21b = "false";
      this.checked22a = "false";
      this.checked22b = "false";
      this.checked23a = "false";
      this.checked23b = "false";
      this.checked24a = "false";
      this.checked24b = "false";

//neue Fragen
this.checked15ba = "false";
this.checked15bb = "false";
this.checked19a = "false";
this.checked19b = "false";
this.checked22aa = "false";
this.checked22ab = "false";
this.hideMe15b = true;
this.hideMe19b = true;
this.hideMe22a = true;

      this.hideMe09b = true;
      this.hideMe10 = true;
      this.hideMe12b = true;
      this.hideMe12 = true;
      this.hideMe13 = true;
      this.hideMe14 = true;
      this.hideMe15 = true;
      this.hideMe16 = true;
      this.hideMe17 = true;
      this.hideMe18 = true;
      this.hideMe19 = true;
      this.hideMe20 = true;
      this.hideMe21 = true;
      this.hideMe22 = true;
      this.hideMe23 = true;
      this.hideMe24 = true;

      this.navCtrl.push(AbspeichernPage);
    }
  

  itemPrev(event, item) {
    this.navCtrl.push(CheckList02B);
  }
  itemNext(event, item) {
    this.navCtrl.push(CheckList02D);
  }
  itemHome(event, item) {
    this.viewCtrl.dismiss();
  }
  ja09() {
    this.checked09a = "true";
    this.checked09b = "false";
    this.data.setData(113, "true", "false", false);
    this.hideMe09b = true;
    this.Finish();

  }
  nein09() {
    this.checked09a = "false";
    this.checked09b = "true";
    this.data.setData(113, "false", "true", false);
    this.hideMe09b = false;

  }
  ja09b() {
    this.checked09ba = "true";
    this.checked09bb = "false";
    this.data.setData(114, "true", "false", false);
    this.HinweisNext();
    this.hideMe10 = true;
  }
  nein09b() {
    this.checked09ba = "false";
    this.checked09bb = "true";
    this.data.setData(114, "false", "true", false);
    this.hideMe10 = false;

  }
  ja10() {
    this.checked10a = "true";
    this.checked10b = "false";
    this.data.setData(115, "true", "false", false);
    this.hideMe12 = false;

  }
  nein10() {
    this.checked10a = "false";
    this.checked10b = "true";
    this.data.setData(115, "false", "true", false);
    this.hideMe12 = false;

  }

  ja12() {

    this.checked12a = "true";
    this.checked12b = "false";
    this.data.setData(117, "true", "false", false);
    this.hideMe12b = false;
    this.hideMe15 = true;

  }
  nein12() {

    this.checked12a = "false";
    this.checked12b = "true";
    this.data.setData(117, "false", "true", false);
    this.hideMe12b = true;
    this.hideMe15 = false;
  }

  ja12b() {

    this.checked12ba = "true";
    this.checked12bb = "false";
    this.data.setData(116, "true", "false", false);
    this.hideMe13 = false;
    this.hideMe15 = true;
  }
  nein12b() {

    this.checked12ba = "false";
    this.checked12bb = "true";
    this.data.setData(116, "false", "true", false);
    this.hideMe13 = true;
    this.hideMe15 = false;

  }

  ja13() {

    this.checked13a = "true";
    this.checked13b = "false";
    this.data.setData(118, "true", "false", false);
    this.hideMe14 = false;
    this.hideMe15 = true;
  }
  nein13() {

    this.checked13a = "false";
    this.checked13b = "true";
    this.data.setData(118, "false", "true", false);
    this.hideMe14 = true;
    this.hideMe15 = false;
  }
  ja14() {

    this.checked14a = "true";
    this.checked14b = "false";
    this.data.setData(119, "true", "false", false);
    this.hideMe22a = false;
    
  }
  nein14() {

    this.checked14a = "false";
    this.checked14b = "true";
    this.data.setData(119, "false", "true", false);
    this.hideMe22a = false;
  }
  ja15() {

    this.checked15a = "true";
    this.checked15b = "false";
    this.data.setData(120, "true", "false", false);
    this.hideMe15b = false;
    this.hideMe19 = true;
  }
  nein15() {

    this.checked15a = "false";
    this.checked15b = "true";
    this.data.setData(120, "false", "true", false);
    this.hideMe15b = true;
    this.hideMe19 = false;
  }

  ja15b() {

    this.checked15ba = "true";
    this.checked15bb = "false";
    this.data.setData(120, "true", "false", false);
    this.hideMe16 = false;
    this.hideMe19 = true;
  }
  nein15b() {

    this.checked15ba = "false";
    this.checked15bb = "true";
    this.data.setData(120, "false", "true", false);
    this.hideMe16 = true;
    this.hideMe19 = false;
  }

  ja16() {

    this.checked16a = "true";
    this.checked16b = "false";
    this.data.setData(121, "true", "false", false);
    this.hideMe17 = false;
    this.hideMe19 = true;
  }
  nein16() {

    this.checked16a = "false";
    this.checked16b = "true";
    this.data.setData(121, "false", "true", false);
    this.hideMe17 = true;
    this.hideMe19 = false;
  }
  ja17() {

    this.checked17a = "true";
    this.checked17b = "false";
    this.data.setData(122, "true", "false", false);
    this.hideMe18 = false;
    this.hideMe22a = true;
  }
  nein17() {

    this.checked17a = "false";
    this.checked17b = "true";
    this.data.setData(122, "false", "true", false);
    this.hideMe18 = true;
    this.hideMe22a = false;
  }
  ja18() {

    this.checked18a = "true";
    this.checked18b = "false";
    this.data.setData(123, "true", "false", false);
    this.hideMe22a = false;
  }
  nein18() {

    this.checked18a = "false";
    this.checked18b = "true";
    this.data.setData(123, "false", "true", false);
    this.hideMe22a = false;
  }
  ja19() {

    this.checked19a = "true";
    this.checked19b = "false";
    this.data.setData(124, "true", "false", false);   
    this.hideMe19b = false;
    this.hideMe23 = true;
  }
  nein19() {

    this.checked19a = "false";
    this.checked19b = "true";
    this.data.setData(124, "false", "true", false);
    this.hideMe23 = false;
    this.hideMe19b = true;
  }
  ja19b() {

    this.checked19ba = "true";
    this.checked19bb = "false";
    this.data.setData(124, "true", "false", false);   
    this.hideMe20 = false;
    this.hideMe23 = true;
  }
  nein19b() {

    this.checked19ba = "false";
    this.checked19bb = "true";
    this.data.setData(124, "false", "true", false);
    this.hideMe23 = false;
    this.hideMe20 = true;
  }
  ja20() {

    this.checked20a = "true";
    this.checked20b = "false";
    this.data.setData(125, "true", "false", false);
    this.hideMe21 = false;
    this.hideMe23 = true;
  }
  nein20() {

    this.checked20a = "false";
    this.checked20b = "true";
    this.data.setData(125, "false", "true", false);
    this.hideMe21 = true;
    this.hideMe23 = false;
  }
  ja21() {

    this.checked21a = "true";
    this.checked21b = "false";
    this.data.setData(126, "true", "false", false);
    this.hideMe22 = false;
    this.hideMe22a = true;
  }

  nein21() {
    this.checked21a = "false";
    this.checked21b = "true";
    this.data.setData(126, "false", "true", false);
    this.hideMe22 = true;
    this.hideMe22a = false;
  }

  ja22() {
    this.checked22a = "true";
    this.checked22b = "false";
    this.data.setData(127, "true", "false", false);
    this.hideMe22a = false;
  }

  nein22() {

    this.checked22a = "false";
    this.checked22b = "true";
    this.data.setData(127, "false", "true", false);
    this.hideMe22a = false;
  }

  ja22a() {
    this.checked22aa = "true";
    this.checked22ab = "false";
    this.data.setData(127, "true", "false", false);
    this.hideMe23 = true;
    this.Finish();
  }

  nein22a() {

    this.checked22aa = "false";
    this.checked22ab = "true";
    this.data.setData(127, "false", "true", false);
    this.hideMe23 = false;
  }
  
  ja23() {

    this.checked23a = "true";
    this.checked23b = "false";
    this.data.setData(128, "true", "false", false);
    this.HinweisNext();
    this.hideMe24 = true;

  }
  nein23() {

    this.checked23a = "false";
    this.checked23b = "true";
    this.data.setData(128, "false", "true", false);
    this.hideMe24 = false;
  }
  ja24() {

    this.checked24a = "true";
    this.checked24b = "false";
    this.data.setData(128, "true", "false", false);
    this.hideMe19 = true;
    this.HinweisNext();
  }
  nein24() {

    this.checked24a = "false";
    this.checked24b = "true";
    this.data.setData(129, "false", "true", false);
    this.HinweisFeld();
  }

  HinweisNext() {
    //neue Funktion!
    var data = { message: 'Hinweis' };
    var finishPage = this.modalCtrl.create('AdviceNextPage', data);
    finishPage.present();
  }

  Hinweis(txt_Hinweis: string) {
    let alert = this.alertCtrl.create({
      title: 'Hinweis',
      subTitle: txt_Hinweis,
      //subTitle: 'Hinweistitel werden noch ergänzt.',
      buttons: ['Ok']
    });
    alert.present();
  }
  HinweisFeld() {
    //neue Funktion!
    var data = { message: 'Hinweis' };
    var finishPage = this.modalCtrl.create('AdviceFieldPage', data);
    finishPage.present();
  }
  Finish() {
    //neue Funktion!
    var data = { message: 'Ende des Verfahrens' };
    var finishPage = this.modalCtrl.create('FinishPage', data);
    finishPage.present();
  }
  Notizen(qnumber) 
  {
    var notizenPage = this.modalCtrl.create('NotePage', { questionnumber:qnumber });
    notizenPage.present();
  }

  Advice(AdviceNr: string) {
    var advicePage = this.modalCtrl.create('Advice' + AdviceNr + 'Page');
    advicePage.present();
  }
}
