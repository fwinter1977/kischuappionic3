import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0331Page } from './advice0331';

@NgModule({
  declarations: [
    Advice0331Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0331Page),
  ],
})
export class Advice0331PageModule {}
