import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { HomePage } from '../home/home';

/**
 * Generated class for the Finish04Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-finish04',
  templateUrl: 'finish04.html',
})
export class Finish04Page {

  public closeFinishold()
{
  var GoInPage = this.modalCtrl.create('Finish03Page');
  GoInPage.present();
}


  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl : ViewController, public modalCtrl : ModalController)
  {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FinishPage');
  }

  public goBack()
  {
    this.viewCtrl.dismiss();
    var GoInPage = this.modalCtrl.create('Finish03Page');
    GoInPage.present();
  }

  public closeFinish()
  {
    this.viewCtrl.dismiss();
    this.navCtrl.push(HomePage);
  }

}
