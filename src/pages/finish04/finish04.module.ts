import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Finish04Page } from './finish04';

@NgModule({
  declarations: [
    Finish04Page,
  ],
  imports: [
    IonicPageModule.forChild(Finish04Page),
  ],
})
export class Finish04PageModule {}
