import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';
import { CheckList03D } from '../checklist03D/checklist03D';
import { HomePage } from '../home/home';
import { AlertController } from 'ionic-angular';
import { Nav, Platform, ModalController, NavParams } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { AbspeichernPage } from '../abspeichern/abspeichern';
import { ContentsChecklist03Page } from '../contents-checklist03/contents-checklist03';
@Component({
  selector: 'page-checklist01E',
  templateUrl: 'checklist03E.html'
})
export class CheckList03E {
  loadedList: any;
  checked34a : string;
  checked34b : string;
  checked35a : string;
  checked35b : string;
  checked36a : string;
  checked36b : string;
  checked37a : string;
  checked37b : string;
  checked38a : string;
  checked38b : string;
  checked39a : string;
  checked39b : string;
  hideMe34: boolean;  
  hideMe35: boolean;
  hideMe36: boolean;
  hideMe37: boolean;
  hideMe38: boolean;
  hideMe39: boolean;

  constructor(public modalCtrl: ModalController, public navCtrl: NavController, public alertCtrl: AlertController, public data : DataProvider, public viewCtrl: ViewController) {

    this.loadedList = this.data.getInitialData();
    if (this.loadedList.length == 0) {
      this.checked34a = "false";
      this.checked34b = "false";
      this.checked35a = "false";
      this.checked35b = "false";
      this.checked36a = "false";
      this.checked36b = "false";
      this.checked37a = "false";
      this.checked37b = "false";
      this.checked38a = "false";
      this.checked38b = "false";
      this.checked39a = "false";
      this.checked39b = "false";
      this.hideMe35 = true;
      this.hideMe36 = true;
      this.hideMe37 = true;
      this.hideMe38 = true;
      this.hideMe39 = true;
    }
    else {
      this.checked34a = "false";
      this.checked34b = "false";
      this.checked35a = "false";
      this.checked35b = "false";
      this.checked36a = "false";
      this.checked36b = "false";
      this.checked37a = "false";
      this.checked37b = "false";
      this.checked38a = "false";
      this.checked38b = "false";
      this.checked39a = "false";
      this.checked39b = "false";
      this.hideMe35 = true;
      this.hideMe36 = true;
      this.hideMe37 = true;
      this.hideMe38 = true;
      this.hideMe39 = true;
      
      if (this.loadedList[236] != undefined) {
        this.checked34a = this.loadedList[236].checkeda;
        this.checked34b = this.loadedList[236].checkedb;
        this.hideMe34 = this.loadedList[236].hidden;
      }
      if (this.loadedList[237] != undefined) {
        this.checked35a = this.loadedList[237].checkeda;
        this.checked35b = this.loadedList[237].checkedb;
        this.hideMe35 = this.loadedList[237].hidden;
      }
      if (this.loadedList[238] != undefined) {
        this.checked36a = this.loadedList[238].checkeda;
        this.checked36b = this.loadedList[238].checkedb;
        this.hideMe36 = this.loadedList[238].hidden;
      }
      if (this.loadedList[239] != undefined) {
        this.checked37a = this.loadedList[239].checkeda;
        this.checked37b = this.loadedList[239].checkedb;
        this.hideMe37 = this.loadedList[239].hidden;
      }
      if (this.loadedList[240] != undefined) {
        this.checked38a = this.loadedList[240].checkeda;
        this.checked38b = this.loadedList[240].checkedb;
        this.hideMe38 = this.loadedList[240].hidden;
      }
      if (this.loadedList[241] != undefined) {
        this.checked39a = this.loadedList[241].checkeda;
        this.checked39b = this.loadedList[241].checkedb;
        this.hideMe39 = this.loadedList[241].hidden;
      }
    }
  
  }
  reset() {
    const confirm = this.alertCtrl.create({
      title: 'Zurücksetzen',
      message: 'Möchten Sie diesen Teil der Checkliste zurücksetzen?',
      buttons: [
        {
          text: 'Ja',
          handler: () => {
            this.checked34a = "false";
            this.checked34b = "false";
            this.checked35a = "false";
            this.checked35b = "false";
            this.checked36a = "false";
            this.checked36b = "false";
            this.checked37a = "false";
            this.checked37b = "false";
            this.checked38a = "false";
            this.checked38b = "false";
            this.checked39a = "false";
            this.checked39b = "false";
            this.hideMe35 = true;
            this.hideMe36 = true;
            this.hideMe37 = true;
            this.hideMe38 = true;
            this.hideMe39 = true;
          }
        },
        {
          text: 'nein',
          handler: () => {
            //console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }
  
  saveList() {

    this.checked34a = "false";
    this.checked34b = "false";
    this.checked35a = "false";
    this.checked35b = "false";
    this.checked36a = "false";
    this.checked36b = "false";
    this.checked37a = "false";
    this.checked37b = "false";
    this.checked38a = "false";
    this.checked38b = "false";
    this.checked39a = "false";
    this.checked39b = "false";
    this.hideMe35 = true;
    this.hideMe36 = true;
    this.hideMe37 = true;
    this.hideMe38 = true;
    this.hideMe39 = true;
    this.navCtrl.push(AbspeichernPage);
  }




  
  itemPrev(event, item) {
    this.navCtrl.push(CheckList03D);
  }
  itemNext(event, item) {
    this.navCtrl.push(CheckList03E);
  }
  itemHome(event, item) {
    /*
    this.navCtrl.setRoot(HomePage).then(() => {
    });;
    */
   this.navCtrl.setRoot(ContentsChecklist03Page).then(() => {

  });;
  }
  ja34()
  {
    this.checked34a="true";
    this.checked34b="false";
    this.data.setData(236, "true", "false", false);
    this.hideMe35 = false;
  }
  nein34()
  {
    this.checked34a="false";
    this.checked34b="true";
    this.data.setData(236, "false", "true", false);
    this.hideMe35 = true;
  }
  ja35()
  {
    this.checked35a="true";
    this.checked35b="false";
    this.data.setData(237, "true", "false", false);
    this.hideMe36 = false;
  }
  nein35()
  {
    this.checked35a="false";
    this.checked35b="true";
    this.data.setData(237, "false", "true", false);
    this.hideMe36 = false;
  }
  ja36()
  {
    this.checked36a="true";
    this.checked36b="false";
    this.data.setData(238, "true", "false", false);
    this.hideMe37 = false;
  }
  nein36()
  {
    this.checked36a="false";
    this.checked36b="true";
    this.data.setData(238, "false", "true", false);
    this.hideMe37 = false;
  }
  ja37()
  {
    this.checked37a="true";
    this.checked37b="false";
    this.data.setData(239, "true", "false", false);
    this.hideMe39 = false;
    this.hideMe38 = true;
  }
  nein37()
  {
    this.checked37a="false";
    this.checked37b="true";
    this.data.setData(239, "false", "true", false);
    this.hideMe38 = false;
    this.hideMe39 = true;
  }
  ja38()
  {
    this.checked38a="true";
    this.checked38b="false";
    this.data.setData(240, "true", "false", false);
    this.hideMe39 = false;
  }
  nein38()
  {
    this.checked38a="false";
    this.checked38b="true";
    this.data.setData(240, "false", "true", false);
    this.hideMe39 = true;
    this.HinweisFeld();
  }
  ja39()
  {
    this.checked39a="true";
    this.checked39b="false";
    this.data.setData(241, "true", "false", false);
    this.FinishJA();
  }
  nein39()
  {
    this.checked39a="false";
    this.checked39b="true";
    this.data.setData(241, "false", "true", false);
  }
  FinishJA() {
    //neue Funktion!
        var data = { message : 'Ende des Verfahrens' };
        var finishPage = this.modalCtrl.create('FinishjamtPage',data);
        finishPage.present();
      }
  Finish() {
    //neue Funktion!
        var data = { message : 'Ende des Verfahrens' };
        var finishPage = this.modalCtrl.create('FinishPage',data);
        finishPage.present();
      }
      HinweisNext() {
        //neue Funktion!
            var data = { message : 'Hinweis' };
            var finishPage = this.modalCtrl.create('AdviceNextPage',data);
            finishPage.present();
          }

  Hinweis(txt_Hinweis: string) {
    let alert = this.alertCtrl.create({
      title: 'Hinweis',
      subTitle: txt_Hinweis,
      //subTitle: 'Hinweistitel werden noch ergänzt.',
      buttons: ['Ok']
    });
    alert.present();
  }
  HinweisFeld() {
    //neue Funktion!
    var data = { message: 'Hinweis' };
    var finishPage = this.modalCtrl.create('AdviceFieldPage', data);
    finishPage.present();
  } 
  Notizen(qnumber) 
  {
    console.log(qnumber);
    var notizenPage = this.modalCtrl.create('NotePage', { questionnumber:qnumber });
    notizenPage.present();
  }
  Advice(AdviceNr: string)
  {
    var advicePage = this.modalCtrl.create('Advice'+AdviceNr+'Page');
    advicePage.present();
  }
}
