import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { KiWoGe01 } from '../kiwoge01/kiwoge01';
import { Nutzung02B } from '../nutzung02B/nutzung02B';
import { Nutzung02C } from '../nutzung02C/nutzung02C';
import { Nutzung02D } from '../nutzung02D/nutzung02D';
import { Nutzung02E } from '../nutzung02E/nutzung02E';
import { Nutzung02F } from '../nutzung02F/nutzung02F';
import { Nutzung02G } from '../nutzung02G/nutzung02G';
import { Nutzung02H } from '../nutzung02H/nutzung02H';

@Component({
  selector: 'page-kiwoge',
  templateUrl: 'kiwoge.html'
})
export class KiWoGe {


  constructor(public navCtrl: NavController) {

  }

  itemNutzg01(event, item) {
    this.navCtrl.push(KiWoGe01);
  }
  itemNutzg02(event, item) {
    this.navCtrl.push(Nutzung02B);
  }
  itemNutzg03(event, item) {
    this.navCtrl.push(Nutzung02C);
  }
  itemNutzg04(event, item) {
    this.navCtrl.push(Nutzung02D);
  }
  itemNutzg05(event, item) {
    this.navCtrl.push(Nutzung02E);
  }
  itemNutzg06(event, item) {
    this.navCtrl.push(Nutzung02F);
  }
  itemNutzg07(event, item) {
    this.navCtrl.push(Nutzung02G);
  }
  itemNutzg08(event, item) {
    this.navCtrl.push(Nutzung02H);
  }


}
