import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0326cPage } from './advice0326c';

@NgModule({
  declarations: [
    Advice0326cPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0326cPage),
  ],
})
export class Advice0326cPageModule {}
