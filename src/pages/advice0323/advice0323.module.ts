import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0323Page } from './advice0323';

@NgModule({
  declarations: [
    Advice0323Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0323Page),
  ],
})
export class Advice0323PageModule {}
