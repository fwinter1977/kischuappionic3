import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFibContentCPage } from './mnu-fib-content-c';

@NgModule({
  declarations: [
    MnuFibContentCPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuFibContentCPage),
  ],
})
export class MnuFibContentCPageModule {}
