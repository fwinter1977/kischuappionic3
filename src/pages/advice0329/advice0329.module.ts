import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0329Page } from './advice0329';

@NgModule({
  declarations: [
    Advice0329Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0329Page),
  ],
})
export class Advice0329PageModule {}
