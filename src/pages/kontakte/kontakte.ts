import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
//import { CallNumber } from '@ionic-native/call-number';

/**
 * Generated class for the KontaktePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-kontakte',
  templateUrl: 'kontakte.html',
})
export class KontaktePage {
  loggedin : boolean;
  //contacts: Array<{name: string, function: string,  phone: string, mail: string, fax: string, traeger: string, profile: string, kompetenzen: string, availability: string}>;
  contacts: Array<{name: string, function: string,  phone: number, web: string; mail: string, fax: string, traeger: string, profile: string, kompetenzen: string, availability: string}>;

  //contacts: any;
  name : string;
  function : string; 
  phone : number; 
  mail : string;
  web : string;
  fax : string;
  traeger : string;
  profile : string;
  kompetenzen : string;
  availability : string;
  erfolgsmeldung: string;
  zaehler: number;
  //seeAll: boolean;
  contactSee; listSee : boolean;



  constructor(public navCtrl: NavController, public navParams: NavParams, public data: DataProvider, public alertCtrl : AlertController) {
    this.contacts = this.data.getContacts();
//Sortieren
    this.contacts = this.contacts.sort((a, b) => a < b ? -1 : 1);
    this.contacts.sort((a, b) => a.name > b.name ? 1 : -1);
  }


    ionViewDidLoad() {
      //this.seeAll = true;
      this.listSee = false;
      this.contactSee = true;
      this.zaehler = 0;
      /*
      this.name = this.contacts[0].name = "Medizinische  Kinderschutzhotline";
      this.function = this.contacts[0].function = "Beratung & Hilfe";
      this.phone = this.contacts[0].phone = 8001921000;
      this.mail = this.contacts[0].mail;
      this.fax = this.contacts[0].mail;
      this.traeger = this.contacts[0].traeger = "Medizinische Kinderschutzhotline, c/o Kinder- und Jugendpsychiatrie und Psychotherapie Universitätsklinik Ulm";
      this.profile = this.contacts[0].profile = "Die Hotline richtet sich an medizinisches Fachpersonal. Die Hotline bietet  kollegiale Beratung und Fallbesprechung bei Verdacht auf Kindeswohlgefährdungen.";
      this.kompetenzen = this.contacts[0].kompetenzen = "An der Medizinischen Kinderschutzhotline beraten Sie Ärztinnen und Ärzte, sowie eine Kinder- und Jugendlichenpsychotherapeutin im fortgeschrittenen Medizinstudium aus den Fachbereichen Rechtsmedizin, Kinder- und Jugendpsychiatrie und Kinder- und Jugendheilkunde, mit entsprechender Zusatzqualifikation.";
      this.availability = this.contacts[0].availability;
*/
      //this.contacts = this.data.getContacts();
      //this.contacts = this.data.getTheContacts();
        //});
        if (this.contacts != undefined)
        {
          /*
          this.name = this.contacts[0].name = "Medizinische  Kinderschutzhotline";
          this.function = this.contacts[0].function = "Beratung & Hilfe";
          this.phone = this.contacts[0].phone = 8001921000;
          this.mail = this.contacts[0].mail;
          this.fax = this.contacts[0].mail;
          this.traeger = this.contacts[0].traeger = "Medizinische Kinderschutzhotline, c/o Kinder- und Jugendpsychiatrie und Psychotherapie Universitätsklinik Ulm";
          this.profile = this.contacts[0].profile = "Die Hotline richtet sich an medizinisches Fachpersonal. Die Hotline bietet  kollegiale Beratung und Fallbesprechung bei Verdacht auf Kindeswohlgefährdungen.";
          this.kompetenzen = this.contacts[0].kompetenzen = "An der Medizinischen Kinderschutzhotline beraten Sie Ärztinnen und Ärzte, sowie eine Kinder- und Jugendlichenpsychotherapeutin im fortgeschrittenen Medizinstudium aus den Fachbereichen Rechtsmedizin, Kinder- und Jugendpsychiatrie und Kinder- und Jugendheilkunde, mit entsprechender Zusatzqualifikation.";
          this.availability = this.contacts[0].availability;
*/
        }

        this.loggedin = this.data.getLoggedIn();
        console.log(this.loggedin);
        if (this.loggedin == false)
        {
          let alert = this.alertCtrl.create({
            title: 'Achtung',
            subTitle: 'Aus datenschutztechnischen Gründen müssen Sie sich zunächst einloggen, um die Kontaktefunktion nutzen zu können!',
            buttons: ['Ok']
          });
          this.listSee = true;
          this.contactSee = true;
          alert.present();
        }

        //console.log('Ktkte', this.contacts[0]);
      
      
  }    

  isReadonly() {
    return this.isReadonly;   //return true/false 
  }

  
  next()
  {this.zaehler++;
    if(this.zaehler <= this.contacts.length && this.contacts != undefined)
    {
      
      this.name = this.contacts[this.zaehler].name;
      this.function = this.contacts[this.zaehler].function;
      this.phone = this.contacts[this.zaehler].phone;
      this.mail = this.contacts[this.zaehler].mail;
      this.web = this.contacts[this.zaehler].web;
      this.fax = this.contacts[this.zaehler].mail;
      this.traeger = this.contacts[this.zaehler].mail;
      this.profile = this.contacts[this.zaehler].mail;
      this.kompetenzen = this.contacts[this.zaehler].mail;
      this.availability = this.contacts[this.zaehler].availability;

      
      console.log('next', this.name, this.zaehler);
    }
    else{
      this.zaehler = 0;
    }

  }
  seeall()
  {
    /*
    if(this.seeAll == true)
    {
      this.seeAll = false;
    }
    else
    {
      this.seeAll = true;
    };
    console.log('dadada', this.seeall);
*/
  }

  prev()
  {
    console.log('before');
    this.zaehler--;
    if(this.zaehler >= 0 && this.contacts != undefined)
    {
      
      this.name = this.contacts[this.zaehler].name;
      this.function = this.contacts[this.zaehler].function;
      this.phone = this.contacts[this.zaehler].phone;
      this.mail = this.contacts[this.zaehler].mail;
      this.fax = this.contacts[this.zaehler].mail;
      this.traeger = this.contacts[this.zaehler].mail;
      this.profile = this.contacts[this.zaehler].mail;
      this.kompetenzen = this.contacts[this.zaehler].mail;
      this.availability = this.contacts[this.zaehler].availability;

      
      console.log('next', this.name, this.zaehler);
    }

  }

  call() {
    //this.navCtrl.push(LoginPage);
    //this.data.getTheContacts();
    
    if (this.contacts != undefined)
    {
      //this.callNumber('1233');
    }

    }

    openContact(p)
    {
      //console.log(p);
      this.contacts = this.data.getContacts();
      console.log(p.name);
      this.zaehler = p;
      this.contactSee = false;
      this.listSee = true;
      this.name = p.name;
      this.function = p.function;
      this.phone = p.phone;
      this.mail = p.mail;
      this.web = p.web;
      this.fax = p.fax;
      this.traeger = p.traeger;
      this.profile = p.profile;
      this.kompetenzen = p.kompetenzen;
      this.availability = p.availability;
    }

    close()
    {
      this.contactSee = true;
      this.listSee = false;
    }
}
