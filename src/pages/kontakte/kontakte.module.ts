import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KontaktePage } from './kontakte';

@NgModule({
  declarations: [
    //KontaktePage,
  ],
  imports: [
    IonicPageModule.forChild(KontaktePage),
  ],
})
export class KontaktePageModule {}
