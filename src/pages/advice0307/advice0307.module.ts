import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0307Page } from './advice0307';

@NgModule({
  declarations: [
    Advice0307Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0307Page),
  ],
})
export class Advice0307PageModule {}
