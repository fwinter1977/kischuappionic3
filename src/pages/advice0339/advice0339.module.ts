import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0339Page } from './advice0339';

@NgModule({
  declarations: [
    Advice0339Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0339Page),
  ],
})
export class Advice0339PageModule {}
