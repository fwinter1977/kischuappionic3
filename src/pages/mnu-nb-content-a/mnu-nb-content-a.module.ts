import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuNbContentAPage } from './mnu-nb-content-a';

@NgModule({
  declarations: [
    MnuNbContentAPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuNbContentAPage),
  ],
})
export class MnuNbContentAPageModule {}
