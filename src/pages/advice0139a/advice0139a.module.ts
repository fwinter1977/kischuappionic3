import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0139aPage } from './advice0139a';

@NgModule({
  declarations: [
    Advice0139aPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0139aPage),
  ],
})
export class Advice0139aPageModule {}
