import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0142Page } from './advice0142';

@NgModule({
  declarations: [
    Advice0142Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0142Page),
  ],
})
export class Advice0142PageModule {}
