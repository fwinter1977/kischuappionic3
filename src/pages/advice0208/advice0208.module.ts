import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0208Page } from './advice0208';

@NgModule({
  declarations: [
    Advice0208Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0208Page),
  ],
})
export class Advice0208PageModule {}
