import { Component } from '@angular/core';
import { Nav, Platform, ModalController, NavParams, ViewController } from 'ionic-angular';
import { CheckList03B } from '../checklist03B/checklist03B';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { DataProvider } from '../../providers/data/data';
import { AbspeichernPage } from '../abspeichern/abspeichern';

@Component({
  selector: 'page-checklist03A',
  templateUrl: 'checklist03A.html'
  
})

export class CheckList03A {

  loadedList: any;
  checked : string;
  checked2 : string;
  checked02a : string;
  checked02b : string;
  checked03a : string;
  checked03b : string;
  checked04a : string;
  checked04b : string;
  checked05a : string;
  checked05b : string;
  checked06a : string;
  checked06b : string;
  checked07a : string;
  checked07b : string;
  checked08a : string;
  checked08b : string;
  checked09a : string;
  checked09b : string;
  hideMe: boolean;
  hideMe02: boolean;
  hideMe03: boolean;
  hideMe04: boolean;
  hideMe05: boolean;
  hideMe06: boolean;
  hideMe07: boolean;
  hideMe08: boolean;
  hideMe09: boolean;
    constructor(public modalCtrl: ModalController, public navCtrl: NavController, public alertCtrl: AlertController, public data : DataProvider, public viewCtrl: ViewController) {

      this.loadedList = this.data.getInitialData();
      if (this.loadedList.length == 0) {
        this.checked = "false";
        this.checked2 = "false";
        this.checked02a = "false";
        this.checked02b = "false";
        this.checked03a = "false";
        this.checked03b = "false";
        this.checked04a = "false";
        this.checked04b = "false";
        this.checked05a = "false";
        this.checked05b = "false";
        this.checked06a = "false";
        this.checked06b = "false";
        this.checked07a = "false";
        this.checked07b = "false";
        this.checked08a = "false";
        this.checked08b = "false";
        this.checked09a = "false";
        this.checked09b = "false";
        
        this.hideMe02 = true;
        this.hideMe03 = true;
        this.hideMe04 = true;
        this.hideMe05 = true;
        this.hideMe06 = true;
        this.hideMe07 = true;
        this.hideMe08 = true;
        this.hideMe09 = true;
      }
      else {
        this.checked = "false";
        this.checked2 = "false";
        this.checked02a = "false";
        this.checked02b = "false";
        this.checked03a = "false";
        this.checked03b = "false";
        this.checked04a = "false";
        this.checked04b = "false";
        this.checked05a = "false";
        this.checked05b = "false";
        this.checked06a = "false";
        this.checked06b = "false";
        this.checked07a = "false";
        this.checked07b = "false";
        this.checked08a = "false";
        this.checked08b = "false";
        this.checked09a = "false";
        this.checked09b = "false";
        
        this.hideMe02 = true;
        this.hideMe03 = true;
        this.hideMe04 = true;
        this.hideMe05 = true;
        this.hideMe06 = true;
        this.hideMe07 = true;
        this.hideMe08 = true;
        this.hideMe09 = true;

        if (this.loadedList[200] != undefined) {
          this.checked = this.loadedList[200].checkeda;
          this.checked2 = this.loadedList[200].checkedb;
          this.hideMe = this.loadedList[200].hidden;
        }
        if (this.loadedList[201] != undefined) {
          this.checked02a = this.loadedList[201].checkeda;
          this.checked02b = this.loadedList[201].checkedb;
          this.hideMe02 = this.loadedList[201].hidden;
        }
        if (this.loadedList[202] != undefined) {
          this.checked03a = this.loadedList[202].checkeda;
          this.checked03b = this.loadedList[202].checkedb;
          this.hideMe03 = this.loadedList[202].hidden;
        }
        if (this.loadedList[203] != undefined) {
          this.checked04a = this.loadedList[203].checkeda;
          this.checked04b = this.loadedList[203].checkedb;
          this.hideMe04 = this.loadedList[203].hidden;
        }
        if (this.loadedList[204] != undefined) {
          this.checked05a = this.loadedList[204].checkeda;
          this.checked05b = this.loadedList[204].checkedb;
          this.hideMe05 = this.loadedList[204].hidden;
        }
        if (this.loadedList[205] != undefined) {
          this.checked06a = this.loadedList[205].checkeda;
          this.checked06b = this.loadedList[205].checkedb;
          this.hideMe06 = this.loadedList[205].hidden;
        }
        if (this.loadedList[206] != undefined) {
          this.checked07a = this.loadedList[206].checkeda;
          this.checked07b = this.loadedList[206].checkedb;
          this.hideMe07 = this.loadedList[206].hidden;
        }
        if (this.loadedList[207] != undefined) {
          this.checked08a = this.loadedList[207].checkeda;
          this.checked08b = this.loadedList[207].checkedb;
          this.hideMe08 = this.loadedList[207].hidden;
        }
        if (this.loadedList[208] != undefined) {
          this.checked09a = this.loadedList[208].checkeda;
          this.checked09b = this.loadedList[208].checkedb;
          this.hideMe09 = this.loadedList[208].hidden;
        }
      }

    }

    reset() {
      const confirm = this.alertCtrl.create({
        title: 'Zurücksetzen',
        message: 'Möchten Sie diesen Teil der Checkliste zurücksetzen?',
        buttons: [
          {
            text: 'Ja',
            handler: () => {
              this.checked = "false";
              this.checked2 = "false";
              this.checked02a = "false";
              this.checked02b = "false";
              this.checked03a = "false";
              this.checked03b = "false";
              this.checked04a = "false";
              this.checked04b = "false";
              this.checked05a = "false";
              this.checked05b = "false";
              this.checked06a = "false";
              this.checked06b = "false";
              this.checked07a = "false";
              this.checked07b = "false";
              this.checked08a = "false";
              this.checked08b = "false";
              this.checked09a = "false";
              this.checked09b = "false";
              
              this.hideMe02 = true;
              this.hideMe03 = true;
              this.hideMe04 = true;
              this.hideMe05 = true;
              this.hideMe06 = true;
              this.hideMe07 = true;
              this.hideMe08 = true;
              this.hideMe09 = true;
            }
          },
          {
            text: 'nein',
            handler: () => {
              //console.log('Agree clicked');
            }
          }
        ]
      });
      confirm.present();
    }
    

    saveList() {

      this.checked = "false";
      this.checked2 = "false";
      this.checked02a = "false";
      this.checked02b = "false";
      this.checked03a = "false";
      this.checked03b = "false";
      this.checked04a = "false";
      this.checked04b = "false";
      this.checked05a = "false";
      this.checked05b = "false";
      this.checked06a = "false";
      this.checked06b = "false";
      this.checked07a = "false";
      this.checked07b = "false";
      this.checked08a = "false";
      this.checked08b = "false";
      this.checked09a = "false";
      this.checked09b = "false";
      
      this.hideMe02 = true;
      this.hideMe03 = true;
      this.hideMe04 = true;
      this.hideMe05 = true;
      this.hideMe06 = true;
      this.hideMe07 = true;
      this.hideMe08 = true;
      this.hideMe09 = true;
      this.navCtrl.push(AbspeichernPage);
    }
    itemHome(event, item) {
      this.viewCtrl.dismiss();
    }
    itemPrev(event, item) {
      this.navCtrl.push(HomePage);
    }
    itemNext(event, item) {
      this.navCtrl.push(CheckList03B);
    }
    ja01()
    {
      this.checked="true";
      this.checked2="false";
      this.data.setData(200, "true", "false", false);
      this.hideMe02=false;
      
  
    }
    nein01()
    {
      this.checked="false";
      this.checked2="true";
      this.data.setData(200, "false", "true", false);
      this.hideMe02 = true;
  
    }
  
    ja02()
    {
  
      this.checked02a="true";
      this.checked02b="false";
      this.data.setData(201, "true", "false", false);
      this.hideMe03=false; 
    }
    nein02()
    {
  
      this.checked02a="false";
      this.checked02b="true";
      this.data.setData(201, "false", "true", false);
      this.hideMe03=false;
    }
    ja03()
    {
      this.checked03a="true";
      this.checked03b="false";
      this.data.setData(202, "true", "false", false);
      this.hideMe04=false;
    }
    nein03()
    {
      this.checked03a="false";
      this.checked03b="true";
      this.data.setData(202, "false", "true", false);
      this.hideMe04=false;
    }
    ja04()
    {
      this.checked04a="true";
      this.checked04b="false";
      this.data.setData(203, "true", "false", false);
      this.hideMe05=false;
    }
    nein04()
    {
      this.checked04a="false";
      this.checked04b="true";
      this.data.setData(203, "false", "true", false);
      this.hideMe05=false;
    }
    ja05()
    {
      this.checked05a="true";
      this.checked05b="false";
      this.data.setData(204, "true", "false", false);
      this.hideMe06=false;
    }
    nein05()
    {
      this.checked05a="false";
      this.checked05b="true";
      this.data.setData(204, "false", "true", false);
      this.hideMe06=true;
      this.Finish();
    }
    ja06()
    {
      this.checked06a="true";
      this.checked06b="false";
      this.data.setData(205, "true", "false", false);
      this.hideMe07=false;
    }
    nein06()
    {
      this.checked06a="false";
      this.checked06b="true";
      this.data.setData(205, "false", "true", false);
      this.hideMe07=false;
    }
    ja07()
    {
      this.checked07a="true";
      this.checked07b="false";
      this.data.setData(206, "true", "false", false);
      this.hideMe08=false;
    }
    nein07()
    {
      this.checked07a="false";
      this.checked07b="true";
      this.data.setData(206, "false", "true", false);
      this.hideMe08=false;
    }
    ja08()
    {
      this.checked08a="true";
      this.checked08b="false";
      this.data.setData(207, "true", "false", false);
      this.hideMe09=false;
    }
    nein08()
    {
      this.checked08a="false";
      this.checked08b="true";
      this.data.setData(207, "false", "true", false);
      this.hideMe09=true;
      this.Finish();
  
    }
    ja09()
    {
      this.checked09a="true";
      this.checked09b="false";
      this.data.setData(208, "true", "false", false);
      this.HinweisNext();
    }
    nein09()
    {
      this.checked09a="false";
      this.checked09b="true";
      this.data.setData(208, "false", "true", false);
      this.HinweisNext();
    }
    Hinweis(txt_Hinweis: string) {
      let alert = this.alertCtrl.create({
        title: 'Hinweis',
        subTitle: txt_Hinweis,
        //subTitle: 'Hinweistitel werden noch ergänzt.',
        buttons: ['Ok']
      });
  
      alert.present();
    }
    HinweisNext() {
      //neue Funktion!
          var data = { message : 'Hinweis' };
          var finishPage = this.modalCtrl.create('AdviceNextPage',data);
          finishPage.present();
        }
    Notizen(qnumber) 
    {
      console.log(qnumber);
      var notizenPage = this.modalCtrl.create('NotePage', { questionnumber:qnumber });
      notizenPage.present();
    }
  
    Advice(AdviceNr: string)
    {
      var advicePage = this.modalCtrl.create('Advice'+AdviceNr+'Page');
      advicePage.present();
    }
  
    Finish() {
      //neue Funktion!
          var data = { message : 'Ende des Verfahrens' };
          var finishPage = this.modalCtrl.create('FinishPage',data);
          finishPage.present();
        }
  }
  
  