import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0207Page } from './advice0207';

@NgModule({
  declarations: [
    Advice0207Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0207Page),
  ],
})
export class Advice0207PageModule {}
