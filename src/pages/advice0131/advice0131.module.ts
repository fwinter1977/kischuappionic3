import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0131Page } from './advice0131';

@NgModule({
  declarations: [
    Advice0131Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0131Page),
  ],
})
export class Advice0131PageModule {}
