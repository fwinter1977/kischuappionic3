import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0129Page } from './advice0129';

@NgModule({
  declarations: [
    Advice0129Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0129Page),
  ],
})
export class Advice0129PageModule {}
