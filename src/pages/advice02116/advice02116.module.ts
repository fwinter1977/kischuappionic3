import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice02116Page } from './advice02116';

@NgModule({
  declarations: [
    Advice02116Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice02116Page),
  ],
})
export class Advice02116PageModule {}
