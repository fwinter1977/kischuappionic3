import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0122Page } from './advice0122';

@NgModule({
  declarations: [
    Advice0122Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0122Page),
  ],
})
export class Advice0122PageModule {}
