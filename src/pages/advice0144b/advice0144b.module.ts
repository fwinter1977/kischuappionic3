import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0144bPage } from './advice0144b';

@NgModule({
  declarations: [
    Advice0144bPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0144bPage),
  ],
})
export class Advice0144bPageModule {}
