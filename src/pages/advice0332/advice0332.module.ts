import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0332Page } from './advice0332';

@NgModule({
  declarations: [
    Advice0332Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0332Page),
  ],
})
export class Advice0332PageModule {}
