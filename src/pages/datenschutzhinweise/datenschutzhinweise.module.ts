import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DatenschutzhinweisePage } from './datenschutzhinweise';

@NgModule({
  declarations: [
    DatenschutzhinweisePage,
  ],
  imports: [
    IonicPageModule.forChild(DatenschutzhinweisePage),
  ],
})
export class DatenschutzhinweisePageModule {}
