import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0326dPage } from './advice0326d';

@NgModule({
  declarations: [
    Advice0326dPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0326dPage),
  ],
})
export class Advice0326dPageModule {}
