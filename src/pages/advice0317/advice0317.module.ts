import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0317Page } from './advice0317';

@NgModule({
  declarations: [
    Advice0317Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0317Page),
  ],
})
export class Advice0317PageModule {}
