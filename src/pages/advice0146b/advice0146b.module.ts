import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0146bPage } from './advice0146b';

@NgModule({
  declarations: [
    Advice0146bPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0146bPage),
  ],
})
export class Advice0146bPageModule {}
