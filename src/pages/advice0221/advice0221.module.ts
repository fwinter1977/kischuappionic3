import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0221Page } from './advice0221';

@NgModule({
  declarations: [
    Advice0221Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0221Page),
  ],
})
export class Advice0221PageModule {}
