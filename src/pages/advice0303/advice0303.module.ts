import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0303Page } from './advice0303';

@NgModule({
  declarations: [
    Advice0303Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0303Page),
  ],
})
export class Advice0303PageModule {}
