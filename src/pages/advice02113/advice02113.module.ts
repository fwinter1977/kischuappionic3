import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice02113Page } from './advice02113';

@NgModule({
  declarations: [
    Advice02113Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice02113Page),
  ],
})
export class Advice02113PageModule {}
