import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContentsChecklist03Mldg02Page } from './contents-checklist03-mldg02';

@NgModule({
  declarations: [
    ContentsChecklist03Mldg02Page,
  ],
  imports: [
    IonicPageModule.forChild(ContentsChecklist03Mldg02Page),
  ],
})
export class ContentsChecklist03Mldg02PageModule {}
