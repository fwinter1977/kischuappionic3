import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFiContentFPage } from './mnu-fi-content-f';

@NgModule({
  declarations: [
    MnuFiContentFPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuFiContentFPage),
  ],
})
export class MnuFiContentFPageModule {}
