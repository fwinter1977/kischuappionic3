import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0330Page } from './advice0330';

@NgModule({
  declarations: [
    Advice0330Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0330Page),
  ],
})
export class Advice0330PageModule {}
