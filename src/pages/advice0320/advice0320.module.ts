import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0320Page } from './advice0320';

@NgModule({
  declarations: [
    Advice0320Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0320Page),
  ],
})
export class Advice0320PageModule {}
