import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { Storage} from '@ionic/storage';
/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  autoUpdate:boolean;
  updateStatus:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl : ViewController, public storage: Storage) {
    /*
    this.storage.get('autoUpdate').then((_update) => {
      this.autoUpdate = _update;
    });
    if (this.autoUpdate==false)
    {
      this.updateStatus = "Automatische Updates deaktiviert."
    }
    if (this.autoUpdate==true)
    {
      this.updateStatus = "Automatische Updates aktiviert."
    }
    */
  
  }

  //ionViewDidLoad() {

    ionViewDidEnter(){
    this.storage.get('autoUpdate').then((_update) => {
      this.autoUpdate = _update;
    });
    if (this.autoUpdate==false)
    {
      this.updateStatus = "Automatische Updates deaktiviert."
    }
    if (this.autoUpdate==true)
    {
      this.updateStatus = "Automatische Updates aktiviert."
    }
  }

/*
  notify() {
    console.log("toggled: "+ this.autoUpdate);
    this.storage.set('autoUpdate', this.autoUpdate);
  }
*/
  changeStatus()
  {
    if (this.autoUpdate==false)
    {
      this.autoUpdate = true;
      this.updateStatus = "Automatische Updates aktiviert."
    }
    else
    {
      this.autoUpdate = false;
      this.updateStatus = "Automatische Updates deaktiviert."
    }

    this.storage.set('autoUpdate', this.autoUpdate);
    //this.updateStatus = "Hugo";
  }

  showStatus()
  {
    if (this.autoUpdate==true)
    {
      this.updateStatus = "Automatische Updates aktiviert."
    }
    else
    {
      this.updateStatus = "Automatische Updates deaktiviert."
    }

  }

  public Back()
  {
    this.viewCtrl.dismiss();
  }
}
