import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0315Page } from './advice0315';

@NgModule({
  declarations: [
    Advice0315Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0315Page),
  ],
})
export class Advice0315PageModule {}
