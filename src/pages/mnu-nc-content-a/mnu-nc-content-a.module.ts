import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuNcContentAPage } from './mnu-nc-content-a';

@NgModule({
  declarations: [
    MnuNcContentAPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuNcContentAPage),
  ],
})
export class MnuNcContentAPageModule {}
