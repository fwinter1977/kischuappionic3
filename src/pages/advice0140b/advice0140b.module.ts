import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0140bPage } from './advice0140b';

@NgModule({
  declarations: [
    Advice0140bPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0140bPage),
  ],
})
export class Advice0140bPageModule {}
