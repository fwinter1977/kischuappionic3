import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0204Page } from './advice0204';

@NgModule({
  declarations: [
    Advice0204Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0204Page),
  ],
})
export class Advice0204PageModule {}
