import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0113Page } from './advice0113';

@NgModule({
  declarations: [
    Advice0113Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0113Page),
  ],
})
export class Advice0113PageModule {}
