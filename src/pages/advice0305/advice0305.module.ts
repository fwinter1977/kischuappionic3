import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0305Page } from './advice0305';

@NgModule({
  declarations: [
    Advice0305Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0305Page),
  ],
})
export class Advice0305PageModule {}
