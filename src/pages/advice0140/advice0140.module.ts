import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0140Page } from './advice0140';

@NgModule({
  declarations: [
    Advice0140Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0140Page),
  ],
})
export class Advice0140PageModule {}
