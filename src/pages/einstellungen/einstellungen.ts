import { Component } from '@angular/core';
import { NavController, AlertController, ViewController } from 'ionic-angular';
import { Nutzung01 } from '../nutzung01/nutzung01';
import { Nutzung02 } from '../nutzung02/nutzung02';
import { Storage} from '@ionic/storage';
import { DataProvider } from '../../providers/data/data';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-nutzung',
  templateUrl: 'einstellungen.html'
})
export class EinstellungenPage {

  contacts: Array<{name: string, function: string,  phone: number, web: string; mail: string, fax: string, traeger: string, profile: string, kompetenzen: string, availability: string}>;
  name : string;
  function : string; 
  phone : number; 
  mail : string;
  web : string;
  fax : string;
  traeger : string;
  profile : string;
  kompetenzen : string;
  availability : string;
  erfolgsmeldung: string;
  loggi: boolean;
  //test
  phonenr : string;
  constructor(public navCtrl: NavController, public storage: Storage, public data: DataProvider, public viewCtrl : ViewController, private alertCtrl: AlertController) {



  }
  ionViewDidLoad() {
    this.storage.get('contacts').then((_contacts) => {
      this.contacts = _contacts;
      });
  }
  LoginWarning() {
    let alert = this.alertCtrl.create({
      title: 'Hinweis',
      subTitle: 'Zum Speichern von Kontakten müssen Sie sich einloggen!',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
    this.navCtrl.setRoot(HomePage);
  }
speichern()
{


  this.loggi = this.data.getLoggedIn();

  if(this.loggi == false)
  {
    this.LoginWarning();
    //this.navCtrl.setRoot(HomePage);
  }
  else
  {
  if(this.contacts == undefined)
  {
    this.contacts = [
    { name: this.name, function: this.function,  phone: this.phone, web: this.web, mail: this.mail, fax: this.fax, traeger: this.traeger, profile: this.profile, kompetenzen: this.kompetenzen, availability: this.availability}
  ];
    console.log('<Kontakte= ',this.contacts);
    //this.contacts = this.data.getContacts();
  }
  else
  {
  this.contacts[this.contacts.length] =
    { name: this.name, function: this.function,  phone: this.phone, mail: this.mail, web: this.web, fax: this.fax, traeger: this.traeger, profile: this.profile, kompetenzen: this.kompetenzen, availability: this.availability};
  console.log('<Kontakte= ',this.contacts);
  }
  //this.phonenr = this.phone;
  this.storage.set('contacts', this.contacts);
  this.contacts = this.data.getContacts();
  this.erfolgsmeldung = "Kontakt wurde gespeichert";
}
}
  itemDieApp(event, item) {
    this.navCtrl.push(Nutzung01);
  }
  itemBedienung(event, item) {
    this.navCtrl.push(Nutzung02);
  }
}
