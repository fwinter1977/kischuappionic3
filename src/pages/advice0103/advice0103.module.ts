import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0103Page } from './advice0103';

@NgModule({
  declarations: [
    Advice0103Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0103Page),
  ],
})
export class Advice0103PageModule {}
