import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0159Page } from './advice0159';

@NgModule({
  declarations: [
    Advice0159Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0159Page),
  ],
})
export class Advice0159PageModule {}
