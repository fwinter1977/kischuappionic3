import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import { LoginPage } from '../login/login';

import { ErstInfoPage } from '../erstinfo/erstinfo';
import { DataProvider } from '../../providers/data/data';
import { LoginPage } from '../login/login';
import { Storage} from '@ionic/storage';

/**
 * Generated class for the IntroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})
export class IntroPage {
  not1stTime:boolean;
  contacts: Array<{name: string, function: string,  phone: string, mail: string, web: string, fax: string, traeger: string, profile: string, kompetenzen: string, availability: string}>;


  constructor(public navCtrl: NavController, public navParams: NavParams, public data: DataProvider, public storage: Storage) {
    
  }
/*
  ionViewDidLoad() {
    console.log('ionViewDidLoad IntroPage');
    this.storage.get('notFirstStart').then((_1st) => {
      this.not1stTime = _1st;
      console.log(this.not1stTime);
      if(this.not1stTime==true)
      {
        console.log("if two");
        //this.navCtrl.setRoot(LoginPage);
      }
      else
      {
        this.storage.set('notFirstStart', true);
        this.navCtrl.setRoot(ErstInfoPage);
      }
    });
  }
  */
  ngOnInit(){

    setTimeout(() => {
      // bestimmt den "TimOut" für die Dauer des Intros und springt nach xxxxx ms in die HomePage

      console.log('ionViewDidLoad IntroPage');
      this.storage.get('notFirstStart').then((_1st) => {
        this.not1stTime = _1st;
        console.log(this.not1stTime);
        if(this.not1stTime==true)
        {
          this.contacts = this.data.getContacts();
          console.log("if two");
          this.navCtrl.setRoot(LoginPage);
        }
        else
        {
          this.contacts = [
            { name: "Medizinische  Kinderschutzhotline", function: "Beratung & Hilfe",  phone: "08001921000", mail: "", web: "www.kinderschutzhotline.de", fax: "", traeger: "Medizinische Kinderschutzhotline, c/o Kinder- und Jugendpsychiatrie und Psychotherapie Universitätsklinik Ulm", profile: "Die Hotline richtet sich an medizinisches Fachpersonal. Die Hotline bietet  kollegiale Beratung und Fallbesprechung bei Verdacht auf Kindeswohlgefährdungen.", kompetenzen: "An der Medizinischen Kinderschutzhotline beraten Sie Ärztinnen und Ärzte, sowie eine Kinder- und Jugendlichenpsychotherapeutin im fortgeschrittenen Medizinstudium aus den Fachbereichen Rechtsmedizin, Kinder- und Jugendpsychiatrie und Kinder- und Jugendheilkunde, mit entsprechender Zusatzqualifikation.", availability: "24h"}
            ,{ name: "Kinderschutzhotline", function: "Beratung & Hilfe",  phone: "08001414007", mail: "", web: "", fax: "", traeger: "Arbeiter-Samariter-Bund", profile: "Landesweit Rat und Hilfe bei Auffälligkeiten wie Vernachlässigung von Kindern, häuslicher Gewalt, familiären Konflikten oder anderen Formen von Kindeswohlgefährdungen. Die Mitarbeiter*innen an der Hotline ersetzen nicht die Arbeit von Polizei und Jugendamt, sondern beraten Fachkräfte, Bürger*innen, Familien sowie Kinder und Jugendliche, auf Wunsch auch anonym.", kompetenzen: "Hohe Beratungs- sowie Fach- und Fallkompetenz durch Mitarbeiter*innen des Kinder- und Jugendnotdienstes", availability: "24h"}
          ];

          //Sortierversuch
          this.contacts = this.contacts.sort((a, b) => a < b ? -1 : 1);
          
          this.storage.set('contacts', this.contacts);
          this.storage.set('notFirstStart', true);
          this.storage.set('tries', 0);
          this.data.setLoggedIn(false);
          this.navCtrl.setRoot(ErstInfoPage);
        }
      });
 

        //this.navCtrl.setRoot(LoginPage); Zeit: 12sek

      
    }, 12000
  );
    
  }

  
}
