import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0139Page } from './advice0139';

@NgModule({
  declarations: [
    Advice0139Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0139Page),
  ],
})
export class Advice0139PageModule {}
