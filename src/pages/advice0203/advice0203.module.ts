import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0203Page } from './advice0203';

@NgModule({
  declarations: [
    Advice0203Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0203Page),
  ],
})
export class Advice0203PageModule {}
