import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0209bPage } from './advice0209b';

@NgModule({
  declarations: [
    Advice0209bPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0209bPage),
  ],
})
export class Advice0209bPageModule {}
