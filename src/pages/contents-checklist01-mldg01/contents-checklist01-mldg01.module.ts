import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContentsChecklist01Mldg01Page } from './contents-checklist01-mldg01';

@NgModule({
  declarations: [
    ContentsChecklist01Mldg01Page,
  ],
  imports: [
    IonicPageModule.forChild(ContentsChecklist01Mldg01Page),
  ],
})
export class ContentsChecklist01Mldg01PageModule {}
