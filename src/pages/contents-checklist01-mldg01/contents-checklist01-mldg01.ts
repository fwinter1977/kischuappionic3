import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { CheckList01E } from '../checklist01E/checklist01E';

/**
 * Generated class for the ContentsChecklist01Mldg01Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contents-checklist01-mldg01',
  templateUrl: 'contents-checklist01-mldg01.html',
})
export class ContentsChecklist01Mldg01Page {

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public viewCtrl : ViewController) {
  }

  goMeldung() {
    this.navCtrl.push(CheckList01E);
  }

  goCancel()
{
  var GoInPage = this.modalCtrl.create('ContentsChecklist01Mldg02Page');
  GoInPage.present();
}
}

