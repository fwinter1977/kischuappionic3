import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuNbContentDPage } from './mnu-nb-content-d';

@NgModule({
  declarations: [
    MnuNbContentDPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuNbContentDPage),
  ],
})
export class MnuNbContentDPageModule {}
