import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0146Page } from './advice0146';

@NgModule({
  declarations: [
    Advice0146Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0146Page),
  ],
})
export class Advice0146PageModule {}
