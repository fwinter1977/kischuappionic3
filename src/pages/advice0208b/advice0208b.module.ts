import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0208bPage } from './advice0208b';

@NgModule({
  declarations: [
    Advice0208bPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0208bPage),
  ],
})
export class Advice0208bPageModule {}
