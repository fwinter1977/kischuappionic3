import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Finish03Page } from './finish03';

@NgModule({
  declarations: [
    Finish03Page,
  ],
  imports: [
    IonicPageModule.forChild(Finish03Page),
  ],
})
export class Finish03PageModule {}
