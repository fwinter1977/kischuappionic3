import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';

/**
 * Generated class for the Finish03Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-finish03',
  templateUrl: 'finish03.html',
})
export class Finish03Page {
  public closeFinish()
{
  this.viewCtrl.dismiss();
}


  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl : ViewController, public modalCtrl : ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FinishPage');
  }
  goNext() {
    var GoInPage = this.modalCtrl.create('Finish04Page');
    GoInPage.present();
  }
}
