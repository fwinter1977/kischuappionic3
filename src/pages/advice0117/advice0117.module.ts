import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0117Page } from './advice0117';

@NgModule({
  declarations: [
    Advice0117Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0117Page),
  ],
})
export class Advice0117PageModule {}
