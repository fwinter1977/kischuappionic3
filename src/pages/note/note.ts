import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';

/**
 * Generated class for the NotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-note',
  templateUrl: 'note.html',
})
export class NotePage {

  text : string;
public closeNote()
{
  //this.data.setNote(this.navParams.data.questionnumber, this.text);
  this.viewCtrl.dismiss();
}



  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl : ViewController, public data : DataProvider) {
  }

  ionViewDidLoad() {
    console.log('status', this.data.getNote(this.navParams.data.questionnumber));
    if(this.data.getNote(this.navParams.data.questionnumber)!=undefined)
    {
      this.text = this.data.getNote(this.navParams.data.questionnumber)
    }
    else{
      this.data.setNote(this.navParams.data.questionnumber, this.text);
      console.log('set nicht vorhanden');
    }
    console.log('ionViewDidLoad NotePage');
    console.log('questionnumber', this.navParams.get('questionnumber'));
    console.log('Passed params', this.navParams.data.questionnumber);
   
    console.log[this.navParams.data.questionnumber];
    console.log('text', this.text);
   

  
  }
  save()
  {
    //this.text = "hjhhh";
    console.log(this.text);
//    this.data.setNote(this.navParams.data.questionnumber, this.text);
    this.data.setNote(this.navParams.data.questionnumber, this.text);
    this.viewCtrl.dismiss();
  }

}
