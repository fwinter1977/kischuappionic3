import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0101Page } from './advice0101';

@NgModule({
  declarations: [
    Advice0101Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0101Page),
  ],
})
export class Advice0101PageModule {}
