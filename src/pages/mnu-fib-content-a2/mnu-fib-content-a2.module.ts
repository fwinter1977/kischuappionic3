import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFibContentA2Page } from './mnu-fib-content-a2';

@NgModule({
  declarations: [
    MnuFibContentA2Page,
  ],
  imports: [
    IonicPageModule.forChild(MnuFibContentA2Page),
  ],
})
export class MnuFibContentA2PageModule {}
