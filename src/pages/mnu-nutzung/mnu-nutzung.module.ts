import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuNutzungPage } from './mnu-nutzung';

@NgModule({
  declarations: [
    MnuNutzungPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuNutzungPage),
  ],
})
export class MnuNutzungPageModule {}
