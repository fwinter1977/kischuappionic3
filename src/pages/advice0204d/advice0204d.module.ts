import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0204dPage } from './advice0204d';

@NgModule({
  declarations: [
    Advice0204dPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0204dPage),
  ],
})
export class Advice0204dPageModule {}
