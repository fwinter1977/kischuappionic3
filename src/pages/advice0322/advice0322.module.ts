import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0322Page } from './advice0322';

@NgModule({
  declarations: [
    Advice0322Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0322Page),
  ],
})
export class Advice0322PageModule {}
