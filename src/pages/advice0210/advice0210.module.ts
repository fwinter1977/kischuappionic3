import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0210Page } from './advice0210';

@NgModule({
  declarations: [
    Advice0210Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0210Page),
  ],
})
export class Advice0210PageModule {}
