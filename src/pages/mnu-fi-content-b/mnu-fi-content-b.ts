import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';

/**
 * Generated class for the MnuFiContentBPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mnu-fi-content-b',
  templateUrl: 'mnu-fi-content-b.html',
})
export class MnuFiContentBPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl : ModalController, public viewCtrl : ViewController) {
  }

  public Back()
  {
    this.viewCtrl.dismiss();
  }

  Go(target: string)
  {
    var GoInPage = this.modalCtrl.create(target+'Page');
    GoInPage.present();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MnuNutzungPage');
  }

}