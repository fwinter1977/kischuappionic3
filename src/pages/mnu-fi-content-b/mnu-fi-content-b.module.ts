import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFiContentBPage } from './mnu-fi-content-b';

@NgModule({
  declarations: [
    MnuFiContentBPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuFiContentBPage),
  ],
})
export class MnuFiContentBPageModule {}
