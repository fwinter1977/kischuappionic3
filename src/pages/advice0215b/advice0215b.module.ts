import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0215bPage } from './advice0215b';

@NgModule({
  declarations: [
    Advice0215bPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0215bPage),
  ],
})
export class Advice0215bPageModule {}
