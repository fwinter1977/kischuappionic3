import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import {NutzungsPage} from '../nutzung/nutzung';
//import { HomePage } from '../home/home';
import { CheckList01 } from '../checklist01/checklist01';
import { CheckList02A } from '../checklist02A/checklist02A';
import { CheckList03A } from '../checklist03A/checklist03A';
import { LoginPage } from '../login/login';
import { DataProvider } from '../../providers/data/data';
//import { TabsPage } from '../tabs/tabs';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  tab1Root = CheckList01;
  tab2Root = CheckList02A;
  tab3Root = NutzungsPage;
  tab4Root = NutzungsPage;
  tab5Root = NutzungsPage;
  public openNote()
  {
    var notizenPage = this.modalCtrl.create('NotePage');
    notizenPage.present();
  }

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public data: DataProvider) {


    
  }

  goNavy()
  {
    var GoInPage = this.modalCtrl.create('NavigationPage');
    GoInPage.present();    
  }

  goLogin()
  {
    this.data.setLoggedIn(false);
    this.navCtrl.setRoot(LoginPage);
  }
  Checklist(ChecklistNr: string)
  {
    var GoInPage = this.modalCtrl.create('GoInChecklist0'+ChecklistNr+'Page');
    GoInPage.present();
  }
  
}

