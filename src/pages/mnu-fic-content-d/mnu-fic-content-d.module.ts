import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFicContentDPage } from './mnu-fic-content-d';

@NgModule({
  declarations: [
    MnuFicContentDPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuFicContentDPage),
  ],
})
export class MnuFicContentDPageModule {}
