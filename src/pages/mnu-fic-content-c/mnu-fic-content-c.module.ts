import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFicContentCPage } from './mnu-fic-content-c';

@NgModule({
  declarations: [
    MnuFicContentCPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuFicContentCPage),
  ],
})
export class MnuFicContentCPageModule {}
