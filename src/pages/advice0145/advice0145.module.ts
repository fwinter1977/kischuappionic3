import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0145Page } from './advice0145';

@NgModule({
  declarations: [
    Advice0145Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0145Page),
  ],
})
export class Advice0145PageModule {}
