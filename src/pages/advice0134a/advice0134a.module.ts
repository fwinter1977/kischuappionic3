import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0134aPage } from './advice0134a';

@NgModule({
  declarations: [
    Advice0134aPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0134aPage),
  ],
})
export class Advice0134aPageModule {}
