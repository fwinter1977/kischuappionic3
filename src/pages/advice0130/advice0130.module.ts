import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0130Page } from './advice0130';

@NgModule({
  declarations: [
    Advice0130Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0130Page),
  ],
})
export class Advice0130PageModule {}
