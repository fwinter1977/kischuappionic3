import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFiaPage } from './mnu-fia';

@NgModule({
  declarations: [
    MnuFiaPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuFiaPage),
  ],
})
export class MnuFiaPageModule {}
