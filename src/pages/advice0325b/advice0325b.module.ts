import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0325bPage } from './advice0325b';

@NgModule({
  declarations: [
    Advice0325bPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0325bPage),
  ],
})
export class Advice0325bPageModule {}
