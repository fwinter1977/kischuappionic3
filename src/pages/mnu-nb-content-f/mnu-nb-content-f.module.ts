import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuNbContentFPage } from './mnu-nb-content-f';

@NgModule({
  declarations: [
    MnuNbContentFPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuNbContentFPage),
  ],
})
export class MnuNbContentFPageModule {}
