import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFicContentAPage } from './mnu-fic-content-a';

@NgModule({
  declarations: [
    MnuFicContentAPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuFicContentAPage),
  ],
})
export class MnuFicContentAPageModule {}
