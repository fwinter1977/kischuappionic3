import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0308Page } from './advice0308';

@NgModule({
  declarations: [
    Advice0308Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0308Page),
  ],
})
export class Advice0308PageModule {}
