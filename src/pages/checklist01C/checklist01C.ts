import { Component } from '@angular/core';
import { CheckList01B } from '../checklist01B/checklist01B';
import { CheckList01D } from '../checklist01D/checklist01D';
import { NavController, ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { Nav, Platform, ModalController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { DataProvider } from '../../providers/data/data';
import { AbspeichernPage } from '../abspeichern/abspeichern';

@Component({
  selector: 'page-checklist01C',
  templateUrl: 'checklist01C.html'
})
export class CheckList01C {
  loadedList: any;
  checked24a: string;
  checked24b: string;
  checked25a: string;
  checked25b: string;
  checked26a: string;
  checked26b: string;
  checked27a: string;
  checked27b: string;
  checked28a: string;
  checked28b: string;
  checked29a: string;
  checked29b: string;
  checked30a: string;
  checked30b: string;
  checked31a: string;
  checked31b: string;
  checked32a: string;
  checked32b: string;
  checked33a: string;
  checked33b: string;
  checked30ba: string;
  checked30bb: string;
  checked34ba: string;
  checked34bb: string;
  hideMe24: boolean;
  hideMe25: boolean;
  hideMe26: boolean;
  hideMe27: boolean;
  hideMe28: boolean;
  hideMe29: boolean;
  hideMe30: boolean;
  hideMe31: boolean;
  hideMe32: boolean;
  hideMe33: boolean;
  hideMe30b: boolean;
  hideMe34b: boolean;
  constructor(public modalCtrl: ModalController, public navCtrl: NavController, public alertCtrl: AlertController, public data: DataProvider,public viewCtrl: ViewController) {

    this.loadedList = this.data.getInitialData();
    if (this.loadedList.length == 0) {
      this.checked24a = "false";
      this.checked24b = "false";
      this.checked25a = "false";
      this.checked25b = "false";
      this.checked26a = "false";
      this.checked26b = "false";
      this.checked27a = "false";
      this.checked27b = "false";
      this.checked28a = "false";
      this.checked28b = "false";
      this.checked29a = "false";
      this.checked29b = "false";
      this.checked30a = "false";
      this.checked30b = "false";
      this.checked31a = "false";
      this.checked31b = "false";
      this.checked32a = "false";
      this.checked32b = "false";
      this.checked33a = "false";
      this.checked33b = "false";
      this.checked30ba = "false";
      this.checked30bb = "false";
      this.checked34ba = "false";
      this.checked34bb = "false";
      this.hideMe25 = true;
      this.hideMe26 = true;
      this.hideMe27 = true;
      this.hideMe28 = true;
      this.hideMe29 = true;
      this.hideMe30 = true;
      this.hideMe31 = true;
      this.hideMe32 = true;
      this.hideMe33 = true;
      this.hideMe30b = true;
      this.hideMe34b = true;
    }
    else {
      this.checked24a = "false";
      this.checked24b = "false";
      this.checked25a = "false";
      this.checked25b = "false";
      this.checked26a = "false";
      this.checked26b = "false";
      this.checked27a = "false";
      this.checked27b = "false";
      this.checked28a = "false";
      this.checked28b = "false";
      this.checked29a = "false";
      this.checked29b = "false";
      this.checked30a = "false";
      this.checked30b = "false";
      this.checked31a = "false";
      this.checked31b = "false";
      this.checked32a = "false";
      this.checked32b = "false";
      this.checked33a = "false";
      this.checked33b = "false";
      this.checked30ba = "false";
      this.checked30bb = "false";
      this.checked34ba = "false";
      this.checked34bb = "false";
      this.hideMe25 = true;
      this.hideMe26 = true;
      this.hideMe27 = true;
      this.hideMe28 = true;
      this.hideMe29 = true;
      this.hideMe30 = true;
      this.hideMe31 = true;
      this.hideMe32 = true;
      this.hideMe33 = true;
      this.hideMe30b = true;
      this.hideMe34b = true;

      if (this.loadedList[25] != undefined) {
        this.checked24a = this.loadedList[25].checkeda;
        this.checked24b = this.loadedList[25].checkedb;
        this.hideMe24 = this.loadedList[25].hidden;
      }
      if (this.loadedList[26] != undefined) {
        this.checked25a = this.loadedList[26].checkeda;
        this.checked25b = this.loadedList[26].checkedb;
        this.hideMe25 = this.loadedList[26].hidden;
      }
      if (this.loadedList[27] != undefined) {
        this.checked26a = this.loadedList[27].checkeda;
        this.checked26b = this.loadedList[27].checkedb;
        this.hideMe26 = this.loadedList[27].hidden;
      }
      if (this.loadedList[28] != undefined) {
        this.checked27a = this.loadedList[28].checkeda;
        this.checked27b = this.loadedList[28].checkedb;
        this.hideMe27 = this.loadedList[28].hidden;
      }
      if (this.loadedList[29] != undefined) {
        this.checked28a = this.loadedList[29].checkeda;
        this.checked28b = this.loadedList[29].checkedb;
        this.hideMe28 = this.loadedList[29].hidden;
      }
      if (this.loadedList[30] != undefined) {
        this.checked29a = this.loadedList[30].checkeda;
        this.checked29b = this.loadedList[30].checkedb;
        this.hideMe29 = this.loadedList[30].hidden;
      }
      if (this.loadedList[31] != undefined) {
        this.checked30a = this.loadedList[31].checkeda;
        this.checked30b = this.loadedList[31].checkedb;
        this.hideMe30 = this.loadedList[31].hidden;
      }
      if (this.loadedList[32] != undefined) {
        this.checked31a = this.loadedList[32].checkeda;
        this.checked31b = this.loadedList[32].checkedb;
        this.hideMe31 = this.loadedList[32].hidden;
      }
      if (this.loadedList[33] != undefined) {
        this.checked32a = this.loadedList[33].checkeda;
        this.checked32b = this.loadedList[33].checkedb;
        this.hideMe32 = this.loadedList[33].hidden;
      }
      if (this.loadedList[34] != undefined) {
        this.checked33a = this.loadedList[34].checkeda;
        this.checked33b = this.loadedList[34].checkedb;
        this.hideMe33 = this.loadedList[34].hidden;
      }
      if (this.loadedList[35] != undefined) {
        this.checked30ba = this.loadedList[35].checkeda;
        this.checked30bb = this.loadedList[35].checkedb;
        this.hideMe30b = this.loadedList[35].hidden;
      }
      if (this.loadedList[36] != undefined) {
        this.checked34ba = this.loadedList[36].checkeda;
        this.checked34bb = this.loadedList[36].checkedb;
        this.hideMe34b = this.loadedList[36].hidden;
      }
    }
  }
  /*
  this.checked24a = "false";
  this.checked24b = "false";
  this.checked25a = "false";
  this.checked25b = "false";
  this.checked26a = "false";
  this.checked26b = "false";
  this.checked27a = "false";
  this.checked27b = "false";
  this.checked28a = "false";
  this.checked28b = "false";
  this.checked29a = "false";
  this.checked29b = "false";
  this.checked30a = "false";
  this.checked30b = "false";
  this.checked31a = "false";
  this.checked31b = "false";
  this.checked32a = "false";
  this.checked32b = "false";
  this.checked33a = "false";
  this.checked33b = "false";
  this.checked34aa = "false";
  this.checked34ab = "false";
  this.checked34ba = "false";
  this.checked34bb = "false";
  this.hideMe25 = true;
  this.hideMe26 = true;
  this.hideMe27 = true;
  this.hideMe28 = true;
  this.hideMe29 = true;
  this.hideMe30 = true;
  this.hideMe31 = true;
  this.hideMe32 = true;
  this.hideMe33 = true;
  this.hideMe34a = true;
  this.hideMe34b = true;
}
*/
reset() {
  const confirm = this.alertCtrl.create({
    title: 'Zurücksetzen',
    message: 'Möchten Sie diesen Teil der Checkliste zurücksetzen?',
    buttons: [
      {
        text: 'Ja',
        handler: () => {
          this.checked24a = "false";
          this.checked24b = "false";
          this.checked25a = "false";
          this.checked25b = "false";
          this.checked26a = "false";
          this.checked26b = "false";
          this.checked27a = "false";
          this.checked27b = "false";
          this.checked28a = "false";
          this.checked28b = "false";
          this.checked29a = "false";
          this.checked29b = "false";
          this.checked30a = "false";
          this.checked30b = "false";
          this.checked31a = "false";
          this.checked31b = "false";
          this.checked32a = "false";
          this.checked32b = "false";
          this.checked33a = "false";
          this.checked33b = "false";
          this.checked30ba = "false";
          this.checked30bb = "false";
          this.checked34ba = "false";
          this.checked34bb = "false";
          this.hideMe25 = true;
          this.hideMe26 = true;
          this.hideMe27 = true;
          this.hideMe28 = true;
          this.hideMe29 = true;
          this.hideMe30 = true;
          this.hideMe31 = true;
          this.hideMe32 = true;
          this.hideMe33 = true;
          this.hideMe30b = true;
          this.hideMe34b = true;
        }
      },
      {
        text: 'nein',
        handler: () => {
          //console.log('Agree clicked');
        }
      }
    ]
  });
  confirm.present();
}
  saveList() {

    this.checked24a = "false";
    this.checked24b = "false";
    this.checked25a = "false";
    this.checked25b = "false";
    this.checked26a = "false";
    this.checked26b = "false";
    this.checked27a = "false";
    this.checked27b = "false";
    this.checked28a = "false";
    this.checked28b = "false";
    this.checked29a = "false";
    this.checked29b = "false";
    this.checked30a = "false";
    this.checked30b = "false";
    this.checked31a = "false";
    this.checked31b = "false";
    this.checked32a = "false";
    this.checked32b = "false";
    this.checked33a = "false";
    this.checked33b = "false";
    this.checked30ba = "false";
    this.checked30bb = "false";
    this.checked34ba = "false";
    this.checked34bb = "false";
    this.hideMe25 = true;
    this.hideMe26 = true;
    this.hideMe27 = true;
    this.hideMe28 = true;
    this.hideMe29 = true;
    this.hideMe30 = true;
    this.hideMe31 = true;
    this.hideMe32 = true;
    this.hideMe33 = true;
    this.hideMe30b = true;
    this.hideMe34b = true;
    this.navCtrl.push(AbspeichernPage);
  }
  itemPrev(event, item) {
    this.navCtrl.push(CheckList01B);
  }
  itemNext(event, item) {
    this.navCtrl.push(CheckList01D);
  }
  itemHome(event, item) {
    this.viewCtrl.dismiss();
  }
  ja24() {
    this.checked24a = "true";
    this.checked24b = "false";
    this.data.setData(25, "true", "false", false);
    this.hideMe25 = false;

  }
  nein24() {
    this.checked24a = "false";
    this.checked24b = "true";
    this.data.setData(25, "false", "true", false);
    this.hideMe25 = false;

  }

  ja25() {

    this.checked25a = "true";
    this.checked25b = "false";
    this.data.setData(26, "true", "false", false);
    this.hideMe26 = true;
    this.HinweisNext();
  }
  nein25() {

    this.checked25a = "false";
    this.checked25b = "true";
    this.data.setData(26, "false", "true", false);
    this.hideMe26 = false;
  }
  ja26() {

    this.checked26a = "true";
    this.checked26b = "false";
    this.data.setData(27, "true", "false", false);
    this.hideMe27 = false;
  }
  nein26() {

    this.checked26a = "false";
    this.checked26b = "true";
    this.data.setData(27, "false", "true", false);
    this.hideMe27 = true;
    this.HinweisFeld();
  }
  ja27() {

    this.checked27a = "true";
    this.checked27b = "false";
    this.data.setData(28, "true", "false", false);
    this.hideMe28 = false;
  }
  nein27() {

    this.checked27a = "false";
    this.checked27b = "true";
    this.data.setData(28, "false", "true", false);
    this.hideMe28 = false;

  }
  ja28() {

    this.checked28a = "true";
    this.checked28b = "false";
    this.data.setData(29, "true", "false", false);
    this.hideMe32 = false;
  }
  nein28() {

    this.checked28a = "false";
    this.checked28b = "true";
    this.data.setData(29, "true", "false", false);
    this.hideMe29 = false;

  }
  ja29() {

    this.checked29a = "true";
    this.checked29b = "false";
    this.data.setData(30, "true", "false", false);
    this.hideMe30 = false;
  }
  nein29() {

    this.checked29a = "false";
    this.checked29b = "true";
    this.data.setData(30, "false", "true", false);
    this.hideMe30 = false;

  }
  ja30() {

    this.checked30a = "true";
    this.checked30b = "false";
    this.data.setData(31, "true", "false", false);
    this.hideMe30b = false;
  }
  nein30() {

    this.checked30a = "false";
    this.checked30b = "true";
    this.data.setData(31, "false", "true", false);
    this.hideMe30b = false;
  }
  ja30b() {

    this.checked30ba = "true";
    this.checked30bb = "false";
    this.data.setData(31, "true", "false", false);
    this.hideMe31 = false;
  }
  nein30b() {

    this.checked30ba = "false";
    this.checked30bb = "true";
    this.data.setData(31, "false", "true", false);
    this.hideMe31 = false;
  }
  ja31() {

    this.checked31a = "true";
    this.checked31b = "false";
    this.data.setData(32, "true", "false", false);
    this.hideMe32 = false;
  }
  nein31() {

    this.checked31a = "false";
    this.checked31b = "true";
    this.data.setData(32, "false", "true", false);
    this.hideMe32 = false;

  }
  ja32() {

    this.checked32a = "true";
    this.checked32b = "false";
    this.data.setData(33, "true", "false", false);
    this.hideMe33 = false;
  }
  nein32() {

    this.checked32a = "false";
    this.checked32b = "true";
    this.data.setData(33, "false", "true", false);
    this.hideMe33 = true;
    this.HinweisFeld();

  }
  ja33() {

    this.checked33a = "true";
    this.checked33b = "false";
    this.data.setData(34, "true", "false", false);
    this.hideMe34b = false;
  }
  nein33() {

    this.checked33a = "false";
    this.checked33b = "true";
    this.data.setData(34, "false", "true", false);
    this.hideMe34b = true;
    this.HinweisFeld();
  }

  nein34a() {

    this.checked30ba = "false";
    this.checked30bb = "true";
    this.data.setData(35, "false", "true", false);
    this.hideMe31 = false;
  }
  ja34b() {

    this.checked34ba = "true";
    this.checked34bb = "false";
    this.data.setData(36, "true", "false", false);
    this.HinweisNext();

  }

  nein34b() {

    this.checked34ba = "false";
    this.checked34bb = "true";
    this.data.setData(36, "false", "true", false);
    this.Finish();
  }
  Finish() {
    //neue Funktion!
    var data = { message: 'Ende des Verfahrens' };
    var finishPage = this.modalCtrl.create('FinishPage', data);
    finishPage.present();
  }

  HinweisNext() {
    //neue Funktion!
    var data = { message: 'Hinweis' };
    var finishPage = this.modalCtrl.create('AdviceNextPage', data);
    finishPage.present();
  }

  Hinweis(txt_Hinweis: string) {
    let alert = this.alertCtrl.create({
      title: 'Hinweis',
      subTitle: txt_Hinweis,
      //subTitle: 'Hinweistitel werden noch ergänzt.',
      buttons: ['Ok']
    });
    alert.present();
  }
  HinweisFeld() {
    //neue Funktion!
    var data = { message: 'Hinweis' };
    var finishPage = this.modalCtrl.create('AdviceFieldPage', data);
    finishPage.present();
  }
  Notizen(qnumber) 
  {
    var notizenPage = this.modalCtrl.create('NotePage', { questionnumber:qnumber });
    notizenPage.present();
  }
  Advice(AdviceNr: string) {
    var advicePage = this.modalCtrl.create('Advice' + AdviceNr + 'Page');
    advicePage.present();
  }
}
