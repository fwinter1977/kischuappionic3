import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuNaPage } from './mnu-na';

@NgModule({
  declarations: [
    MnuNaPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuNaPage),
  ],
})
export class MnuNaPageModule {}
