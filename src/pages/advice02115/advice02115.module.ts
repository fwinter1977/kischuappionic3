import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice02115Page } from './advice02115';

@NgModule({
  declarations: [
    Advice02115Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice02115Page),
  ],
})
export class Advice02115PageModule {}
