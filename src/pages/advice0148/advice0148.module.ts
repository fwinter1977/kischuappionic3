import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0148Page } from './advice0148';

@NgModule({
  declarations: [
    Advice0148Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0148Page),
  ],
})
export class Advice0148PageModule {}
