import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0211Page } from './advice0211';

@NgModule({
  declarations: [
    Advice0211Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0211Page),
  ],
})
export class Advice0211PageModule {}
