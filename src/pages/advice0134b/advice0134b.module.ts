import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0134bPage } from './advice0134b';

@NgModule({
  declarations: [
    Advice0134bPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0134bPage),
  ],
})
export class Advice0134bPageModule {}
