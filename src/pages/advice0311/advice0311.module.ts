import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0311Page } from './advice0311';

@NgModule({
  declarations: [
    Advice0311Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0311Page),
  ],
})
export class Advice0311PageModule {}
