import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0204ePage } from './advice0204e';

@NgModule({
  declarations: [
    Advice0204ePage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0204ePage),
  ],
})
export class Advice0204ePageModule {}
