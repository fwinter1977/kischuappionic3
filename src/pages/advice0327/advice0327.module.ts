import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0327Page } from './advice0327';

@NgModule({
  declarations: [
    Advice0327Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0327Page),
  ],
})
export class Advice0327PageModule {}
