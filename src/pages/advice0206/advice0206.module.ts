import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0206Page } from './advice0206';

@NgModule({
  declarations: [
    Advice0206Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0206Page),
  ],
})
export class Advice0206PageModule {}
