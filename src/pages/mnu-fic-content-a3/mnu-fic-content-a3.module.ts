import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFicContentA3Page } from './mnu-fic-content-a3';

@NgModule({
  declarations: [
    MnuFicContentA3Page,
  ],
  imports: [
    IonicPageModule.forChild(MnuFicContentA3Page),
  ],
})
export class MnuFicContentA3PageModule {}
