import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';

import { ViewCompiler } from "@angular/compiler";
import { LoginPage } from '../login/login';

/**
 * Generated class for the AbspeichernPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-abspeichern',
  templateUrl: 'abspeichern.html',
})
export class AbspeichernPage {

  displayname1: string;
  displayname2: string;
  displayname3: string;
  
  notEmpty: boolean;
  cancelSave: boolean;
  myData: any;
  loggi: boolean;
  success: boolean;
  listennummer: string;
  filename1; filename2; filename3 : string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public data: DataProvider, public storage: Storage, public viewCtrl: ViewController, private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    this.success = false;
    this.cancelSave = true;
    this.storage.get('filename1').then((_pw) => {
      this.displayname1 = _pw;
    });
    
    this.storage.get('filename2').then((_pw) => {
      this.displayname2 = _pw;
    });
    this.storage.get('filename3').then((_pw) => {
      this.displayname3 = _pw;
    });
    /*
    if(this.displayname1 == undefined) this.filename1="freier Speicherplatz";
    if(this.displayname2 == undefined) this.filename2="freier Speicherplatz";
    if(this.displayname3 == undefined) this.filename3="freier Speicherplatz";
    */

  }
  Warning(nummer) {
    this.listennummer = nummer;
    let alert = this.alertCtrl.create({
      title: 'Hinweis',
      subTitle: 'Alle ggf. in dieser Akte bereits enthaltenden Daten werden überschrieben!',
      buttons: [
        {
          text: 'Abbrechen',
          //role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
            this.success = false;
            this.cancelSave = true;
            //this.navCtrl.setRoot(HomePage);
            this.viewCtrl.dismiss();
          }
        },
        {
          text: 'Ok',
          handler: () => {
            console.log('Buy clicked');
            this.success = true;
            this.cancelSave = false;

            if (this.listennummer == '1') {

              this.myData = this.data.getData('1');
              this.storage.set('filename1', this.filename1);
              this.data.saveToDisk(1);
              this.SaveSuccess();
            }
            else if (this.listennummer == '2') {
              this.myData = this.data.getData('2');
              this.storage.set('filename2', this.filename2);
              this.data.saveToDisk(2);
              this.SaveSuccess();
  
            }
            else if (this.listennummer == '3'){
              this.myData = this.data.getData('3');
              this.storage.set('filename3', this.filename3);
              this.data.saveToDisk(3);
              this.SaveSuccess();
  
            }

          }
        }
      ]
    });
    alert.present();
  }

  LoginWarning() {
    let alert = this.alertCtrl.create({
      title: 'Hinweis',
      subTitle: 'Zum Speichern von Daten müssen Sie sich einloggen!',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
    //this.viewCtrl.dismiss();
  }
  SaveSuccess() {
    let alert = this.alertCtrl.create({
      title: 'Hinweis',
      subTitle: 'Ihre Daten wurden erfolgreich gespeichert!',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
    //this.viewCtrl.dismiss();
  }
  save(nummer) {
    this.loggi = this.data.getLoggedIn();
    if (this.loggi == false) {
      this.LoginWarning();
      //this.navCtrl.setRoot(HomePage);
    }
    else {
      this.storage.get('notEmpty').then((_1st) => {
        this.notEmpty = _1st;
        if (this.notEmpty == true) {
          this.Warning(this.listennummer);
        }
        else {
          this.storage.set('notEmpty', true);
        }
        console.log('Jespeichertist!');

      });

      this.listennummer = nummer;
      if (this.listennummer == '1') {
        //this.myData = this.data.getData('1');
        //this.storage.set('filename1', this.filename1);


          
          //this.data.saveToDisk(1);

        
        
        //this.viewCtrl.dismiss();
        //this.SaveSuccess();
        //this.data.getData('1');   
        if (this.success == true) {
          //this.navCtrl.setRoot(HomePage);
        }
      }
      else if (this.listennummer == '2') {
        /* neo
        console.log('zwoi');
        this.myData = this.data.getData('2');
        this.storage.set('filename2', this.filename2);
        this.data.saveToDisk(2);
        */
        //this.SaveSuccess();
        if (this.success == true) {
          //this.navCtrl.setRoot(HomePage);
        }
      }
      else if (this.listennummer == '3') {
        /*neo
        console.log('droi');
        this.myData = this.data.getData('3');
        this.storage.set('filename3', this.filename3);
        this.data.saveToDisk(3);
        */
        //this.SaveSuccess();
        if (this.success == true) {
          //this.navCtrl.setRoot(HomePage);
        }
      }
    
  }
    //this.data.saveToDisk();
    //console.log(this.myData[0]);
  }

  GoBack() {
    this.navCtrl.setRoot(HomePage);
    //this.viewCtrl.dismiss(); NICHT DISMISS!!! geht zurück auf leere Liste!
  }
}
