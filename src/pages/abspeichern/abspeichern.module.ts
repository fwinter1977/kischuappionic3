import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AbspeichernPage } from './abspeichern';

@NgModule({
  declarations: [
    //AbspeichernPage,
  ],
  imports: [
    IonicPageModule.forChild(AbspeichernPage),
  ],
})
export class AbspeichernPageModule {}
