import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0144Page } from './advice0144';

@NgModule({
  declarations: [
    Advice0144Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0144Page),
  ],
})
export class Advice0144PageModule {}
