import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { HomePage } from '../home/home';
import { RegistrationPage } from '../registration/registration';
import { Storage } from '@ionic/storage';
import { DataProvider } from '../../providers/data/data';
import { HttpClient } from '@angular/common/http';
import { isNullOrUndefined } from 'util';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  films: any;
  user: string;
  pw: any;
  platform: Platform;
  errormessage: string;
  userdb: string;
  userpw: string;
  tries: any;
  contacts: Array<{ name: string, function: string, phone: number, web: string, mail: string, fax: string, traeger: string, profile: string, kompetenzen: string, availability: string }>;

  constructor(public navCtrl: NavController, public navParams: NavParams, platform: Platform, public storage: Storage, public data: DataProvider, public httpClient: HttpClient) {
    this.platform = platform;

    this.films = this.httpClient.get('https://swapi.co/api/films');
    this.films
      .subscribe(data => {
        console.log('my data: ', data);
        console.log('Versioning');
      })
  }

  login() {

    //hier muss die Variable aus der DB in tries geladen werden

    if (this.user == this.userdb && this.pw == this.userpw && this.user != undefined && this.pw != undefined)
    //if (this.user==this.userdb && this.pw==this.userpw)

    {
      this.data.setLoggedIn(true);
      this.tries = 0;
      this.storage.set('tries', 0);
      this.navCtrl.push(HomePage);
    }
    else {
      this.data.setLoggedIn(false);
      console.log("incorrect login");
      console.log(this.user);
      console.log("gegen");
      console.log(this.userdb);
      console.log(this.pw);
      console.log("gegen");
      console.log(this.userpw);

      //this.errormessage = "Die Logindaten sind nicht korrekt!";
      //Baustelle
      //this.data.setFailedLogins();
      //Baustelle
      this.tries++;
      this.storage.set('tries', this.tries);

      //Baustelle Tries
      //this.tries = this.data.getFailedLogins();
      //Baustelle Tries

      this.errormessage = "Fehlgeschlagene Versuche:" + this.tries + " von 10";
      if (this.tries == 10) {
        this.errormessage = "Sämtliche Nutzerdaten wurden gelöscht!";
        this.tries = 0;
        //this.data.resetFailedLogins();
        //Baustelle
        //this.data.resetFailedLogins();
        //Baustelle
        this.storage.clear();                       // alle Inhalte werden gelöscht weil 10mal Fehleingabe
        this.storage.set('tries', 0);
        //this.storage = null;
        //this.storage.set('username', "");

      }

    }
  }

  goHome() {
    this.data.setLoggedIn(false);
    this.navCtrl.push(HomePage);
  }
  goRegistration() {
    this.navCtrl.push(RegistrationPage);
  }

  exitApp() {
    this.data.setLoggedIn(false);
    this.platform.exitApp();
  }
  ionViewDidLoad() {
    this.contacts = this.data.getContacts();
    this.storage.get('password').then((_pw) => {
      this.userpw = _pw;
    });
    this.storage.get('username').then((_user) => {
      this.userdb = _user;
    });
    //Baustelle Tries
    this.storage.get('tries').then((_tries) => {
      this.tries = _tries;
    });
    //Baustelle Tries
  }

}
