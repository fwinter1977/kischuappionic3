import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuNbContentBPage } from './mnu-nb-content-b';

@NgModule({
  declarations: [
    MnuNbContentBPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuNbContentBPage),
  ],
})
export class MnuNbContentBPageModule {}
