import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0301Page } from './advice0301';

@NgModule({
  declarations: [
    Advice0301Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0301Page),
  ],
})
export class Advice0301PageModule {}
