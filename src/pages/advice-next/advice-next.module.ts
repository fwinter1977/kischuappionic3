import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdviceNextPage } from './advice-next';

@NgModule({
  declarations: [
    AdviceNextPage,
  ],
  imports: [
    IonicPageModule.forChild(AdviceNextPage),
  ],
})
export class AdviceNextPageModule {}
