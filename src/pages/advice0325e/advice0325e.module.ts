import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0325ePage } from './advice0325e';

@NgModule({
  declarations: [
    Advice0325ePage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0325ePage),
  ],
})
export class Advice0325ePageModule {}
