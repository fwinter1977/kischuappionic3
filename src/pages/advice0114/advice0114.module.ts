import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0114Page } from './advice0114';

@NgModule({
  declarations: [
    Advice0114Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0114Page),
  ],
})
export class Advice0114PageModule {}
