import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController }  from 'ionic-angular';

/**
 * Generated class for the Advice0114Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-advice0114',
  templateUrl: 'advice0114.html',
})
export class Advice0114Page {

  public closeAdvice()
  {
    this.viewCtrl.dismiss();
  }
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl : ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Advice0102Page');
  }

}
