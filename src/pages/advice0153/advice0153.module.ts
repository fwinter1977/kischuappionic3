import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0153Page } from './advice0153';

@NgModule({
  declarations: [
    Advice0153Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0153Page),
  ],
})
export class Advice0153PageModule {}
