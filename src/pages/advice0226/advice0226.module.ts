import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0226Page } from './advice0226';

@NgModule({
  declarations: [
    Advice0226Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0226Page),
  ],
})
export class Advice0226PageModule {}
