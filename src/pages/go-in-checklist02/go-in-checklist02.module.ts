import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GoInChecklist02Page } from './go-in-checklist02';

@NgModule({
  declarations: [
    GoInChecklist02Page,
  ],
  imports: [
    IonicPageModule.forChild(GoInChecklist02Page),
  ],
})
export class GoInChecklist02PageModule {}
