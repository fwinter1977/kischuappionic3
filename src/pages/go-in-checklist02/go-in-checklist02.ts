import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController, AlertController } from 'ionic-angular';
import { SpeicherPage } from '../speicher/speicher';
import { DataProvider } from '../../providers/data/data';
import { Speicher2Page } from '../speicher2/speicher2';

/**
 * Generated class for the GoInChecklist02Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-go-in-checklist02',
  templateUrl: 'go-in-checklist02.html',
})
export class GoInChecklist02Page {
  loggedin : boolean; 
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public viewCtrl : ViewController, public data : DataProvider, public alertCtrl : AlertController) {
  }
  public closeAdvice()
  {
    this.viewCtrl.dismiss();
  }
  GoSpeicher() {
    this.loggedin = this.data.getLoggedIn();
    console.log(this.loggedin);
    if (this.loggedin == true)
    {
      this.navCtrl.push(Speicher2Page);
    }
    else
    {
      let alert = this.alertCtrl.create({
        title: 'Achtung',
        subTitle: 'Aus datenschutztechnischen Gründen müssen Sie sich zunächst einloggen, um diese Funktion zu nutzen!',
        buttons: ['Ok']
      });
      alert.present();
    }


    
    }
    GoContent()
    {
      this.data.eraseData();
      var GoInPage = this.modalCtrl.create('ContentsChecklist02Page');
      GoInPage.present();
    }
    GoExisting()
    {
      //this.viewCtrl.dismiss();
      //this.data.eraseData();
      var GoInPage = this.modalCtrl.create('ContentsChecklist02Page');
      GoInPage.present();
    }
  ionViewDidLoad() {
    console.log('ionViewDidLoad GoInChecklist02Page');
  }
}
