import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0325Page } from './advice0325';

@NgModule({
  declarations: [
    Advice0325Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0325Page),
  ],
})
export class Advice0325PageModule {}
