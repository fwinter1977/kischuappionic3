import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0326bPage } from './advice0326b';

@NgModule({
  declarations: [
    Advice0326bPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0326bPage),
  ],
})
export class Advice0326bPageModule {}
