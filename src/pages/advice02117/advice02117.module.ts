import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice02117Page } from './advice02117';

@NgModule({
  declarations: [
    Advice02117Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice02117Page),
  ],
})
export class Advice02117PageModule {}
