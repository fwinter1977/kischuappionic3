import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0123aPage } from './advice0123a';

@NgModule({
  declarations: [
    Advice0123aPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0123aPage),
  ],
})
export class Advice0123aPageModule {}
