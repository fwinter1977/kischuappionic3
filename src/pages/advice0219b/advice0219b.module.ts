import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0219bPage } from './advice0219b';

@NgModule({
  declarations: [
    Advice0219bPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0219bPage),
  ],
})
export class Advice0219bPageModule {}
