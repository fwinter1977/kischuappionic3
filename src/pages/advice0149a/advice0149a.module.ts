import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0149aPage } from './advice0149a';

@NgModule({
  declarations: [
    Advice0149aPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0149aPage),
  ],
})
export class Advice0149aPageModule {}
