import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0204fPage } from './advice0204f';

@NgModule({
  declarations: [
    Advice0204fPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0204fPage),
  ],
})
export class Advice0204fPageModule {}
