import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0120Page } from './advice0120';

@NgModule({
  declarations: [
    Advice0120Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0120Page),
  ],
})
export class Advice0120PageModule {}
