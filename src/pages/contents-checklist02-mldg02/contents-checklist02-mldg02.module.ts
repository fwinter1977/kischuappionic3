import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContentsChecklist02Mldg02Page } from './contents-checklist02-mldg02';

@NgModule({
  declarations: [
    ContentsChecklist02Mldg02Page,
  ],
  imports: [
    IonicPageModule.forChild(ContentsChecklist02Mldg02Page),
  ],
})
export class ContentsChecklist02Mldg02PageModule {}
