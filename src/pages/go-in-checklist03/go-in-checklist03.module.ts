import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GoInChecklist03Page } from './go-in-checklist03';

@NgModule({
  declarations: [
    GoInChecklist03Page,
  ],
  imports: [
    IonicPageModule.forChild(GoInChecklist03Page),
  ],
})
export class GoInChecklist03PageModule {}
