import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { GoInChecklist02Page } from '../go-in-checklist02/go-in-checklist02';
import { Storage} from '@ionic/storage';
/**
 * Generated class for the Speicher2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-speicher2',
  templateUrl: 'speicher2.html',
})
export class Speicher2Page {
  filename1 : string = "leer";
  filename2 : string = "leer";
  filename3 : string = "leer";
  emptyMessage : string;
  hidden01; hidden02; hidden03  : boolean;



  constructor(public navCtrl: NavController, public data: DataProvider, public storage: Storage, public modalCtrl: ModalController, public viewCtrl : ViewController) {
    //this.emptyMessage = "Es sind gegenwärtig keine Fallakten gespeichert!";
    
    this.hidden01 = true;
    this.hidden02 = true;
    this.hidden03 = true;
    
  }
  GoBack()
  {
    this.navCtrl.push(GoInChecklist02Page);
  }
  ionViewDidLoad() {
    this.storage.get('filename1').then((_pw) => {
      this.filename1 = _pw;
    });
    this.storage.get('filename2').then((_pw) => {
      this.filename2 = _pw;
    });
    this.storage.get('filename3').then((_pw) => {
      this.filename3 = _pw;
    });
  }


  loadList1()
  {

    this.data.getData('1');    
    var GoInPage = this.modalCtrl.create('ContentsChecklist02Page');
    GoInPage.present();
  }
  loadList2()
  {

    this.data.getData('2');    
    var GoInPage = this.modalCtrl.create('ContentsChecklist02Page');
    GoInPage.present();
  }
  loadList3()
  {

    this.data.getData('3');    
    var GoInPage = this.modalCtrl.create('ContentsChecklist02Page');
    GoInPage.present();
  }
}