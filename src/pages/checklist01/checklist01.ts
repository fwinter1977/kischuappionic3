import { Component } from '@angular/core';
import { Nav, Platform, ModalController, ViewController, AlertController } from 'ionic-angular';
import { CheckList01B } from '../checklist01B/checklist01B';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { DataProvider } from '../../providers/data/data';
import { AbspeichernPage } from '../abspeichern/abspeichern';
import { empty } from 'rxjs/Observer';
import { ContentsChecklist01Page } from '../contents-checklist01/contents-checklist01';

//import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'page-checklist01',
  templateUrl: 'checklist01.html'

})

export class CheckList01 {

  namen: any;
  checked: string;
  checked2: string;
  checked02a: string;
  checked02b: string;
  checked03a: string;
  checked03b: string;
  checked04a: string;
  checked04b: string;
  checked05a: string;
  checked05b: string;
  checked06a: string;
  checked06b: string;
  checked07aa: string;
  checked07ab: string;
  checked07ba: string;
  checked07bb: string;
  checked08a: string;
  checked08b: string;
  checked09aa: string;
  checked09ab: string;
  hideMe: boolean;
  hideMe02: boolean;
  hideMe03: boolean;
  hideMe04: boolean;
  hideMe05: boolean;
  hideMe06: boolean;
  hideMe07a: boolean;
  hideMe07b: boolean;
  hideMe08: boolean;
  hideMe09a: boolean;

  loadedList: any;
  constructor(public alertCtrl: AlertController, public modalCtrl: ModalController, public navCtrl: NavController, public data: DataProvider, public viewCtrl: ViewController) {

    this.loadedList = this.data.getInitialData();

    if (this.loadedList.length == 0) {

      this.checked = "false";
      this.checked2 = "false";
      this.checked02a = "false";
      this.checked02b = "false";
      this.checked03a = "false";
      this.checked03b = "false";
      this.checked04a = "false";
      this.checked04b = "false";
      this.checked05a = "false";
      this.checked05b = "false";
      this.checked06a = "false";
      this.checked06b = "false";
      this.checked07aa = "false";
      this.checked07ab = "false";
      this.checked07ba = "false";
      this.checked07bb = "false";
      this.checked08a = "false";
      this.checked08b = "false";
      this.checked09aa = "false";
      this.checked09ab = "false";
      this.hideMe02 = true;
      this.hideMe03 = true;
      this.hideMe04 = true;
      this.hideMe05 = true;
      this.hideMe06 = true;
      this.hideMe07a = true;
      this.hideMe07b = true;
      this.hideMe08 = true;
      this.hideMe09a = true;
    }
    else {

      this.checked = "false";
      this.checked2 = "false";
      this.checked02a = "false";
      this.checked02b = "false";
      this.checked03a = "false";
      this.checked03b = "false";
      this.checked04a = "false";
      this.checked04b = "false";
      this.checked05a = "false";
      this.checked05b = "false";
      this.checked06a = "false";
      this.checked06b = "false";
      this.checked07aa = "false";
      this.checked07ab = "false";
      this.checked07ba = "false";
      this.checked07bb = "false";
      this.checked08a = "false";
      this.checked08b = "false";
      this.checked09aa = "false";
      this.checked09ab = "false";
      this.hideMe02 = true;
      this.hideMe03 = true;
      this.hideMe04 = true;
      this.hideMe05 = true;
      this.hideMe06 = true;
      this.hideMe07a = true;
      this.hideMe07b = true;
      this.hideMe08 = true;
      this.hideMe09a = true;
      if (this.loadedList[0] != undefined) {
        this.checked = this.loadedList[0].checkeda;
        this.checked2 = this.loadedList[0].checkedb;
        this.hideMe = this.loadedList[0].hidden;
      }
      if (this.loadedList[1] != undefined) {
        this.checked02a = this.loadedList[1].checkeda;
        this.checked02b = this.loadedList[1].checkedb;
        this.hideMe02 = this.loadedList[1].hidden;
      }
      if (this.loadedList[2] != undefined) {
        this.checked03a = this.loadedList[2].checkeda;
        this.checked03b = this.loadedList[2].checkedb;
        this.hideMe03 = this.loadedList[2].hidden;
      }
      if (this.loadedList[3] != undefined) {
        this.checked04a = this.loadedList[3].checkeda;
        this.checked04b = this.loadedList[3].checkedb;
        this.hideMe04 = this.loadedList[3].hidden;
      }
      if (this.loadedList[4] != undefined) {
        this.checked05a = this.loadedList[4].checkeda;
        this.checked05b = this.loadedList[4].checkedb;
        this.hideMe05 = this.loadedList[4].hidden;
      }
      if (this.loadedList[5] != undefined) {
        this.checked06a = this.loadedList[5].checkeda;
        this.checked06b = this.loadedList[5].checkedb;
        this.hideMe06 = this.loadedList[5].hidden;
      }
      if (this.loadedList[6] != undefined) {
        this.checked07aa = this.loadedList[6].checkeda;
        this.checked07ab = this.loadedList[6].checkedb;
        this.hideMe07a = this.loadedList[6].hidden;
      }
      if (this.loadedList[7] != undefined) {
        this.checked07ba = this.loadedList[7].checkeda;
        this.checked07bb = this.loadedList[7].checkedb;
        this.hideMe07b = this.loadedList[7].hidden;
      }
      if (this.loadedList[8] != undefined) {
        this.checked08a = this.loadedList[8].checkeda;
        this.checked08b = this.loadedList[8].checkedb;
        this.hideMe08 = this.loadedList[8].hidden;
      }
      if (this.loadedList[9] != undefined) {
        this.checked09aa = this.loadedList[9].checkeda;
        this.checked09ab = this.loadedList[9].checkedb;
        this.hideMe09a = this.loadedList[9].hidden;
      }
    }
  }
  itemHome(event, item) {
    //this.viewCtrl.dismiss();
    this.navCtrl.push(ContentsChecklist01Page);
    //this.navCtrl.push(ContentsChecklist01Page).then(() => {

    
  }
  itemPrev(event, item) {
    /*
    this.navCtrl.setRoot(HomePage).then(() => {

    });;
    */
   this.navCtrl.push(ContentsChecklist01Page);
  }
  itemNext(event, item) {
    this.navCtrl.push(CheckList01B);
    /*
    this.navCtrl.setRoot(CheckList01B).then(() => {

    });;
    */
  }


  saveList() {

    this.checked = "false";
    this.checked2 = "false";
    this.checked02a = "false";
    this.checked02b = "false";
    this.checked03a = "false";
    this.checked03b = "false";
    this.checked04a = "false";
    this.checked04b = "false";
    this.checked05a = "false";
    this.checked05b = "false";
    this.checked06a = "false";
    this.checked06b = "false";
    this.checked07aa = "false";
    this.checked07ab = "false";
    this.checked07ba = "false";
    this.checked07bb = "false";
    this.checked08a = "false";
    this.checked08b = "false";
    this.checked09aa = "false";
    this.checked09ab = "false";
    this.hideMe02 = true;
    this.hideMe03 = true;
    this.hideMe04 = true;
    this.hideMe05 = true;
    this.hideMe06 = true;
    this.hideMe07a = true;
    this.hideMe07b = true;
    this.hideMe08 = true;
    this.hideMe09a = true;
    this.navCtrl.push(AbspeichernPage);
  }
  reset() {
    const confirm = this.alertCtrl.create({
      title: 'Zurücksetzen',
      message: 'Möchten Sie diesen Teil der Checkliste zurücksetzen?',
      buttons: [
        {
          text: 'Ja',
          handler: () => {
            this.checked = "false";
            this.checked2 = "false";
            this.checked02a = "false";
            this.checked02b = "false";
            this.checked03a = "false";
            this.checked03b = "false";
            this.checked04a = "false";
            this.checked04b = "false";
            this.checked05a = "false";
            this.checked05b = "false";
            this.checked06a = "false";
            this.checked06b = "false";
            this.checked07aa = "false";
            this.checked07ab = "false";
            this.checked07ba = "false";
            this.checked07bb = "false";
            this.checked08a = "false";
            this.checked08b = "false";
            this.checked09aa = "false";
            this.checked09ab = "false";
            this.hideMe02 = true;
            this.hideMe03 = true;
            this.hideMe04 = true;
            this.hideMe05 = true;
            this.hideMe06 = true;
            this.hideMe07a = true;
            this.hideMe07b = true;
            this.hideMe08 = true;
            this.hideMe09a = true;
          }
        },
        {
          text: 'nein',
          handler: () => {
            //console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }


  ja01() {
    this.checked = "true";
    this.checked2 = "false";
    this.data.setData(0, "true", "false", false);
    this.hideMe02 = false;


  }
  nein01() {
    this.checked = "false";
    this.checked2 = "true";
    this.hideMe02 = true;
    this.data.setData(0, "false", "true", false);
  }

  ja02() {

    this.checked02a = "true";
    this.checked02b = "false";
    this.data.setData(1, "true", "false", false);
    this.hideMe03 = false;
  }
  nein02() {

    this.checked02a = "false";
    this.checked02b = "true";
    this.data.setData(1, "false", "true", false);
    this.hideMe03 = false;
  }
  ja03() {
    this.checked03a = "true";
    this.checked03b = "false";
    this.data.setData(2, "true", "false", false);
    this.hideMe04 = false;
  }
  nein03() {
    this.checked03a = "false";
    this.checked03b = "true";
    this.data.setData(2, "false", "true", false);
    this.hideMe04 = false;
  }
  ja04() {
    this.checked04a = "true";
    this.checked04b = "false";
    this.data.setData(3, "true", "false", false);
    this.hideMe05 = false;
  }
  nein04() {
    this.checked04a = "false";
    this.checked04b = "true";
    this.data.setData(3, "false", "true", false);
    this.hideMe05 = true;
    this.Finish();
  }
  ja05() {
    this.checked05a = "true";
    this.checked05b = "false";
    this.data.setData(4, "true", "false", false);
    this.hideMe06 = false;
  }
  nein05() {
    this.checked05a = "false";
    this.checked05b = "true";
    this.data.setData(4, "false", "true", false);
    this.hideMe06 = true;
    this.HinweisFeld();
  }
  ja06() {
    this.checked06a = "true";
    this.checked06b = "false";
    this.data.setData(5, "true", "false", false);
    this.hideMe07a = false;
  }
  nein06() {
    this.checked06a = "false";
    this.checked06b = "true";
    this.data.setData(5, "false", "true", false);
    this.hideMe07a = false;
  }
  ja07a() {
    this.checked07aa = "true";
    this.checked07ab = "false";
    this.data.setData(6, "true", "false", false);
    this.hideMe07b = false;
  }
  nein07a() {
    this.checked07aa = "false";
    this.checked07ab = "true";
    this.data.setData(6, "false", "true", false);
    this.hideMe07b = false;
  }
  ja07b() {
    this.checked07ba = "true";
    this.checked07bb = "false";
    this.data.setData(7, "true", "false", false);
    this.hideMe08 = false;

  }
  nein07b() {
    this.checked07ba = "false";
    this.checked07bb = "true";
    this.data.setData(7, "false", "true", false);
    this.hideMe08 = true;
    this.Finish();
  }
  ja08() {
    this.checked08a = "true";
    this.checked08b = "false";
    this.data.setData(8, "true", "false", false);
    this.hideMe09a = false;
  }
  nein08() {
    this.checked08a = "false";
    this.checked08b = "true";
    this.data.setData(8, "false", "true", false);
    this.hideMe09a = false;

  }
  ja09a() {
    this.checked09aa = "true";
    this.checked09ab = "false";
    this.data.setData(9, "true", "false", false);
    this.HinweisNext();
  }
  nein09a() {
    this.checked09aa = "false";
    this.checked09ab = "true";
    this.data.setData(9, "true", "false", false);
    this.HinweisNext();
  }

  HinweisNext() {
    //neue Funktion!
    var data = { message: 'Hinweis' };
    var finishPage = this.modalCtrl.create('AdviceNextPage', data);
    finishPage.present();
  }
  HinweisFeld() {
    //neue Funktion!
    var data = { message: 'Hinweis' };
    var finishPage = this.modalCtrl.create('AdviceFieldPage', data);
    finishPage.present();
  }

  Notizen(qnumber) 
  {
    console.log(qnumber);
    var notizenPage = this.modalCtrl.create('NotePage', { questionnumber:qnumber });
    notizenPage.present();
  }

  Advice(AdviceNr: string) {
    var advicePage = this.modalCtrl.create('Advice' + AdviceNr + 'Page');
    advicePage.present();
  }

  Finish() {
    //neue Funktion!
    var data = { message: 'Ende des Verfahrens' };
    var finishPage = this.modalCtrl.create('FinishPage', data);
    finishPage.present();
  }
}

