import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0204bPage } from './advice0204b';

@NgModule({
  declarations: [
    Advice0204bPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0204bPage),
  ],
})
export class Advice0204bPageModule {}
