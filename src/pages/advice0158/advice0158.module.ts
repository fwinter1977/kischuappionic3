import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0158Page } from './advice0158';

@NgModule({
  declarations: [
    Advice0158Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0158Page),
  ],
})
export class Advice0158PageModule {}
