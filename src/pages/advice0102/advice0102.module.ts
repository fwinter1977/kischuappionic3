import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0102Page } from './advice0102';

@NgModule({
  declarations: [
    Advice0102Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0102Page),
  ],
})
export class Advice0102PageModule {}
