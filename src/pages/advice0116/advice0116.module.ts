import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0116Page } from './advice0116';

@NgModule({
  declarations: [
    Advice0116Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0116Page),
  ],
})
export class Advice0116PageModule {}
