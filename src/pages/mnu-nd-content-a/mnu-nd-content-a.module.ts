import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuNdContentAPage } from './mnu-nd-content-a';

@NgModule({
  declarations: [
    MnuNdContentAPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuNdContentAPage),
  ],
})
export class MnuNdContentAPageModule {}
