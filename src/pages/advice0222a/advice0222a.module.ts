import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0222aPage } from './advice0222a';

@NgModule({
  declarations: [
    Advice0222aPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0222aPage),
  ],
})
export class Advice0222aPageModule {}
