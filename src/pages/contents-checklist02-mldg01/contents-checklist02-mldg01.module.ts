import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContentsChecklist02Mldg01Page } from './contents-checklist02-mldg01';

@NgModule({
  declarations: [
    ContentsChecklist02Mldg01Page,
  ],
  imports: [
    IonicPageModule.forChild(ContentsChecklist02Mldg01Page),
  ],
})
export class ContentsChecklist02Mldg01PageModule {}
