import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { CheckList02D } from '../checklist02D/checklist02D';

/**
 * Generated class for the ContentsChecklist02Mldg01Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contents-checklist02-mldg01',
  templateUrl: 'contents-checklist02-mldg01.html',
})
export class ContentsChecklist02Mldg01Page {

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public viewCtrl : ViewController) {
  }

  goMeldung() {
    this.navCtrl.push(CheckList02D);
  }

  goCancel()
{
  var GoInPage = this.modalCtrl.create('ContentsChecklist02Mldg02Page');
  GoInPage.present();
}
}

