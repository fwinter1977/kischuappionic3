import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0112aPage } from './advice0112a';

@NgModule({
  declarations: [
    Advice0112aPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0112aPage),
  ],
})
export class Advice0112aPageModule {}
