import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFiContentAPage } from './mnu-fi-content-a';

@NgModule({
  declarations: [
    MnuFiContentAPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuFiContentAPage),
  ],
})
export class MnuFiContentAPageModule {}
