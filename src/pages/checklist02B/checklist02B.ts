import { Component } from '@angular/core';
import { Nav, Platform, ModalController, NavParams, ViewController } from 'ionic-angular';
import { CheckList02A } from '../checklist02A/checklist02A';
import { CheckList02C } from '../checklist02C/checklist02C';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { AbspeichernPage } from '../abspeichern/abspeichern';
import { DataProvider } from '../../providers/data/data';
@Component({
  selector: 'page-checklist02B',
  templateUrl: 'checklist02B.html'
})
export class CheckList02B {
  loadedList: any;
  checked05a : string;
  checked05b : string;
  checked06a : string;
  checked06b : string;
  checked07a : string;
  checked07b : string;
  checked08a : string;
  checked08b : string;
  checked08ba : string;
  checked08bb : string;

  hideMe05: boolean;
  hideMe06: boolean;
  hideMe07: boolean;
  hideMe08: boolean;
  hideMe08b: boolean;
  
  
  constructor(public modalCtrl: ModalController, public navCtrl: NavController, public alertCtrl: AlertController, public data : DataProvider, public viewCtrl: ViewController) {

    this.loadedList = this.data.getInitialData();
    if (this.loadedList.length == 0) {
      this.checked05a = "false";
      this.checked05b = "false";
      this.checked06a = "false";
      this.checked06b = "false";
      this.checked07a = "false";
      this.checked07b = "false";
      this.checked08a = "false";
      this.checked08b = "false";
      this.checked08ba = "false";
      this.checked08bb = "false";
  
      this.hideMe06 = true;
      this.hideMe07 = true;
      this.hideMe08 = true;
      this.hideMe08b = true;
    }
    else {
      this.checked05a = "false";
      this.checked05b = "false";
      this.checked06a = "false";
      this.checked06b = "false";
      this.checked07a = "false";
      this.checked07b = "false";
      this.checked08a = "false";
      this.checked08b = "false";
      this.checked08ba = "false";
      this.checked08bb = "false";
  
      this.hideMe06 = true;
      this.hideMe07 = true;
      this.hideMe08 = true;
      this.hideMe08b = true;

      if (this.loadedList[108] != undefined) {
        this.checked05a = this.loadedList[108].checkeda;
        this.checked05b = this.loadedList[108].checkedb;
        this.hideMe05 = this.loadedList[108].hidden;
      }
      if (this.loadedList[109] != undefined) {
        this.checked06a = this.loadedList[109].checkeda;
        this.checked06b = this.loadedList[109].checkedb;
        this.hideMe06 = this.loadedList[109].hidden;
      }
      if (this.loadedList[110] != undefined) {
        this.checked07a = this.loadedList[110].checkeda;
        this.checked07b = this.loadedList[110].checkedb;
        this.hideMe07 = this.loadedList[110].hidden;
      }
      if (this.loadedList[111] != undefined) {
        this.checked08a = this.loadedList[111].checkeda;
        this.checked08b = this.loadedList[111].checkedb;
        this.hideMe08 = this.loadedList[111].hidden;
      }
      if (this.loadedList[112] != undefined) {
        this.checked08ba = this.loadedList[112].checkeda;
        this.checked08bb = this.loadedList[112].checkedb;
        this.hideMe08b = this.loadedList[112].hidden;
      }
    }
    
  
  }
  saveList() {

    this.checked05a = "false";
    this.checked05b = "false";
    this.checked06a = "false";
    this.checked06b = "false";
    this.checked07a = "false";
    this.checked07b = "false";
    this.checked08a = "false";
    this.checked08b = "false";
    this.checked08ba = "false";
    this.checked08bb = "false";

    this.hideMe06 = true;
    this.hideMe07 = true;
    this.hideMe08 = true;
    this.hideMe08b = true;
    this.navCtrl.push(AbspeichernPage);
  }
  itemPrev(event, item) {
    this.navCtrl.push(CheckList02A);
  }
  itemNext(event, item) {
    this.navCtrl.push(CheckList02C);
  }
  itemHome(event, item) {
    this.viewCtrl.dismiss();
  }
  ja05()
  {
    this.checked05a="true";
    this.checked05b="false";
    this.data.setData(108, "true", "false", false);
    this.hideMe06 = true;
    this.HinweisJugendamt();

  }
  nein05()
  {
    this.checked05a="false";
    this.checked05b="true";
    this.data.setData(108, "false", "true", false);
    this.hideMe06 = false;
  }

  ja06()
  {

    this.checked06a="true";
    this.checked06b="false";
    this.data.setData(109, "true", "false", false);
    this.hideMe07=false;
  }
  nein06()
  {

    this.checked06a="false";
    this.checked06b="true";
    this.data.setData(109, "false", "true", false);
    this.hideMe07=true;
    this.HinweisFeld();
  }
  ja07()
  {

    this.checked07a="true";
    this.checked07b="false";
    this.data.setData(110, "true", "false", false);
    this.hideMe08b=false;
    this.hideMe08 = true;

  }
  nein07()
  {

    this.checked07a="false";
    this.checked07b="true";
    this.data.setData(110, "false", "true", false);
    this.hideMe08=false;
    this.hideMe08b = true;
  }
  ja08()
  {

    this.checked08a="true";
    this.checked08b="false";
    this.data.setData(111, "true", "false", false);
    this.hideMe08b=false;
  }
  nein08()
  {

    this.checked08a="false";
    this.checked08b="true";
    this.data.setData(111, "false", "true", false);
    this.hideMe08b=true;
    this.HinweisFeld();
  }
  ja08b()
  {

    this.checked08ba="true";
    this.checked08bb="false";
    this.data.setData(112, "true", "false", false);
    this.HinweisNext();
  }  
  nein08b()
  {

    this.checked08ba="false";
    this.checked08bb="true";
    this.data.setData(112, "true", "false", false);
    this.Finish();
  }
 


  HinweisNext() {
    //neue Funktion!
    var data = { message: 'Hinweis' };
    var finishPage = this.modalCtrl.create('AdviceNextPage', data);
    finishPage.present();
  }
  HinweisJugendamt() {
    //neue Funktion!
    var data = { message: 'Hinweis' };
    var finishPage = this.modalCtrl.create('AdviceJugendamtPage', data);
    finishPage.present();
  }

  HinweisFeld() {
    //neue Funktion!
    var data = { message: 'Hinweis' };
    var finishPage = this.modalCtrl.create('AdviceFieldPage', data);
    finishPage.present();
  }
  Hinweis(txt_Hinweis: string) {
    let alert = this.alertCtrl.create({
      title: 'Hinweis',
      subTitle: txt_Hinweis,
      //subTitle: 'Hinweistitel werden noch ergänzt.',
      buttons: ['Ok']
    });
    alert.present();
  }
  
  Finish() {
//neue Funktion!
    var data = { message : 'Ende des Verfahrens' };
    var finishPage = this.modalCtrl.create('FinishPage',data);
    finishPage.present();
  }
  Notizen(qnumber) 
  {
    var notizenPage = this.modalCtrl.create('NotePage', { questionnumber:qnumber });
    notizenPage.present();
  }

  Advice(AdviceNr: string)
  {
    var advicePage = this.modalCtrl.create('Advice'+AdviceNr+'Page');
    advicePage.present();
  }
}
