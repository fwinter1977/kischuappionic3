import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0137Page } from './advice0137';

@NgModule({
  declarations: [
    Advice0137Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0137Page),
  ],
})
export class Advice0137PageModule {}
