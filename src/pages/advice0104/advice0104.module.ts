import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0104Page } from './advice0104';

@NgModule({
  declarations: [
    Advice0104Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0104Page),
  ],
})
export class Advice0104PageModule {}
