import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFibContentA5Page } from './mnu-fib-content-a5';

@NgModule({
  declarations: [
    MnuFibContentA5Page,
  ],
  imports: [
    IonicPageModule.forChild(MnuFibContentA5Page),
  ],
})
export class MnuFibContentA5PageModule {}
