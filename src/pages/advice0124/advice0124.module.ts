import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0124Page } from './advice0124';

@NgModule({
  declarations: [
    Advice0124Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0124Page),
  ],
})
export class Advice0124PageModule {}
