import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0316Page } from './advice0316';

@NgModule({
  declarations: [
    Advice0316Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0316Page),
  ],
})
export class Advice0316PageModule {}
