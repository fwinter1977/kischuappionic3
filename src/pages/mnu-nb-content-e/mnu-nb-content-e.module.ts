import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuNbContentEPage } from './mnu-nb-content-e';

@NgModule({
  declarations: [
    MnuNbContentEPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuNbContentEPage),
  ],
})
export class MnuNbContentEPageModule {}
