import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuNbContentGPage } from './mnu-nb-content-g';

@NgModule({
  declarations: [
    MnuNbContentGPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuNbContentGPage),
  ],
})
export class MnuNbContentGPageModule {}
