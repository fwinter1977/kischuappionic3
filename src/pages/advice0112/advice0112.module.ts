import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0112Page } from './advice0112';

@NgModule({
  declarations: [
    Advice0112Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0112Page),
  ],
})
export class Advice0112PageModule {}
