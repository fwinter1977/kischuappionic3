import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';
import { CheckList03C } from '../checklist03C/checklist03C';
import { CheckList03E } from '../checklist03E/checklist03E';
import { HomePage } from '../home/home';
import { AlertController } from 'ionic-angular';
import { Nav, Platform, ModalController, NavParams } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { AbspeichernPage } from '../abspeichern/abspeichern';
@Component({
  selector: 'page-checklist03D',
  templateUrl: 'checklist03D.html'
})
export class CheckList03D {
  loadedList: any;
  checked26a: string;
  checked26b: string;
  checked26ba: string;
  checked26bb: string;
  checked26ca: string;
  checked26cb: string;
  checked26da: string;
  checked26db: string;
  checked27a: string;
  checked27b: string;
  checked28a: string;
  checked28b: string;
  checked29a: string;
  checked29b: string;
  checked30a: string;
  checked30b: string;
  checked31a: string;
  checked31b: string;
  checked32a: string;
  checked32b: string;
  checked33a: string;
  checked33b: string;

  hideMe26: boolean;
  hideMe26b: boolean;
  hideMe26c: boolean;
  hideMe26d: boolean;
  hideMe27: boolean;
  hideMe28: boolean;
  hideMe29: boolean;
  hideMe30: boolean;
  hideMe31: boolean;
  hideMe32: boolean;
  hideMe33: boolean;

  constructor(public modalCtrl: ModalController, public navCtrl: NavController, public alertCtrl: AlertController, public data : DataProvider ,public viewCtrl: ViewController) {

    this.loadedList = this.data.getInitialData();
    if (this.loadedList.length == 0) {
      this.checked26a = "false";
      this.checked26b = "false";
      this.checked26ba = "false";
      this.checked26bb = "false";
      this.checked26ca = "false";
      this.checked26cb = "false";
      this.checked26da = "false";
      this.checked26db = "false";
      this.checked27a = "false";
      this.checked27b = "false";
      this.checked28a = "false";
      this.checked28b = "false";
      this.checked29a = "false";
      this.checked29b = "false";
      this.checked30a = "false";
      this.checked30b = "false";
      this.checked31a = "false";
      this.checked31b = "false";
      this.checked32a = "false";
      this.checked32b = "false";
      this.checked33a = "false";
      this.checked33b = "false";
  
      this.hideMe26b = true;
      this.hideMe26c = true;
      this.hideMe26d = true;
      this.hideMe27 = true;
      this.hideMe28 = true;
      this.hideMe29 = true;
      this.hideMe30 = true;
      this.hideMe31 = true;
      this.hideMe32 = true;
      this.hideMe33 = true;
    }
    else {
      this.checked26a = "false";
      this.checked26b = "false";
      this.checked26ba = "false";
      this.checked26bb = "false";
      this.checked26ca = "false";
      this.checked26cb = "false";
      this.checked26da = "false";
      this.checked26db = "false";
      this.checked27a = "false";
      this.checked27b = "false";
      this.checked28a = "false";
      this.checked28b = "false";
      this.checked29a = "false";
      this.checked29b = "false";
      this.checked30a = "false";
      this.checked30b = "false";
      this.checked31a = "false";
      this.checked31b = "false";
      this.checked32a = "false";
      this.checked32b = "false";
      this.checked33a = "false";
      this.checked33b = "false";
  
      this.hideMe26b = true;
      this.hideMe26c = true;
      this.hideMe26d = true;
      this.hideMe27 = true;
      this.hideMe28 = true;
      this.hideMe29 = true;
      this.hideMe30 = true;
      this.hideMe31 = true;
      this.hideMe32 = true;
      this.hideMe33 = true;
      
      if (this.loadedList[228] != undefined) {
        this.checked26a = this.loadedList[228].checkeda;
        this.checked26b = this.loadedList[228].checkedb;
        this.hideMe26 = this.loadedList[228].hidden;
      }
      if (this.loadedList[2281] != undefined) {
        this.checked26ba = this.loadedList[2281].checkeda;
        this.checked26bb = this.loadedList[2281].checkedb;
        this.hideMe26b = this.loadedList[2281].hidden;
      }
      if (this.loadedList[2282] != undefined) {
        this.checked26ca = this.loadedList[2282].checkeda;
        this.checked26cb = this.loadedList[2282].checkedb;
        this.hideMe26c = this.loadedList[2282].hidden;
      }
      if (this.loadedList[2283] != undefined) {
        this.checked26da = this.loadedList[2283].checkeda;
        this.checked26db = this.loadedList[2283].checkedb;
        this.hideMe26d = this.loadedList[2283].hidden;
      }
      if (this.loadedList[229] != undefined) {
        this.checked27a = this.loadedList[229].checkeda;
        this.checked27b = this.loadedList[229].checkedb;
        this.hideMe27 = this.loadedList[229].hidden;
      }
      if (this.loadedList[230] != undefined) {
        this.checked28a = this.loadedList[230].checkeda;
        this.checked28b = this.loadedList[230].checkedb;
        this.hideMe28 = this.loadedList[230].hidden;
      }
      if (this.loadedList[231] != undefined) {
        this.checked29a = this.loadedList[231].checkeda;
        this.checked29b = this.loadedList[231].checkedb;
        this.hideMe29 = this.loadedList[231].hidden;
      }
      if (this.loadedList[232] != undefined) {
        this.checked30a = this.loadedList[232].checkeda;
        this.checked30b = this.loadedList[232].checkedb;
        this.hideMe30 = this.loadedList[232].hidden;
      }
      if (this.loadedList[233] != undefined) {
        this.checked31a = this.loadedList[233].checkeda;
        this.checked31b = this.loadedList[233].checkedb;
        this.hideMe31 = this.loadedList[233].hidden;
      }
      if (this.loadedList[234] != undefined) {
        this.checked32a = this.loadedList[234].checkeda;
        this.checked32b = this.loadedList[234].checkedb;
        this.hideMe32 = this.loadedList[234].hidden;
      }
      if (this.loadedList[235] != undefined) {
        this.checked33a = this.loadedList[235].checkeda;
        this.checked33b = this.loadedList[235].checkedb;
        this.hideMe33 = this.loadedList[235].hidden;
      }
    }
  
  }
  reset() {
    const confirm = this.alertCtrl.create({
      title: 'Zurücksetzen',
      message: 'Möchten Sie diesen Teil der Checkliste zurücksetzen?',
      buttons: [
        {
          text: 'Ja',
          handler: () => {
            this.checked26a = "false";
            this.checked26b = "false";
            this.checked26ba = "false";
            this.checked26bb = "false";
            this.checked26ca = "false";
            this.checked26cb = "false";
            this.checked26da = "false";
            this.checked26db = "false";
            this.checked27a = "false";
            this.checked27b = "false";
            this.checked28a = "false";
            this.checked28b = "false";
            this.checked29a = "false";
            this.checked29b = "false";
            this.checked30a = "false";
            this.checked30b = "false";
            this.checked31a = "false";
            this.checked31b = "false";
            this.checked32a = "false";
            this.checked32b = "false";
            this.checked33a = "false";
            this.checked33b = "false";
        
            this.hideMe26b = true;
            this.hideMe26c = true;
            this.hideMe26d = true;
            this.hideMe27 = true;
            this.hideMe28 = true;
            this.hideMe29 = true;
            this.hideMe30 = true;
            this.hideMe31 = true;
            this.hideMe32 = true;
            this.hideMe33 = true;
          }
        },
        {
          text: 'nein',
          handler: () => {
            //console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }
  
  saveList() {

    this.checked26a = "false";
    this.checked26b = "false";
    this.checked26ba = "false";
    this.checked26bb = "false";
    this.checked26ca = "false";
    this.checked26cb = "false";
    this.checked26da = "false";
    this.checked26db = "false";
    this.checked27a = "false";
    this.checked27b = "false";
    this.checked28a = "false";
    this.checked28b = "false";
    this.checked29a = "false";
    this.checked29b = "false";
    this.checked30a = "false";
    this.checked30b = "false";
    this.checked31a = "false";
    this.checked31b = "false";
    this.checked32a = "false";
    this.checked32b = "false";
    this.checked33a = "false";
    this.checked33b = "false";

    this.hideMe26b = true;
    this.hideMe26c = true;
    this.hideMe26d = true;
    this.hideMe27 = true;
    this.hideMe28 = true;
    this.hideMe29 = true;
    this.hideMe30 = true;
    this.hideMe31 = true;
    this.hideMe32 = true;
    this.hideMe33 = true;
    this.navCtrl.setRoot(AbspeichernPage);
  }





  itemPrev(event, item) {
    this.navCtrl.setRoot(CheckList03C);
  }
  itemNext(event, item) {
    this.navCtrl.setRoot(CheckList03E);
  }
  itemHome(event, item) {
    this.viewCtrl.dismiss();
  }

  ja26() {

    this.checked26a = "true";
    this.checked26b = "false";
    this.data.setData(228, "true", "false", false);
    this.hideMe27 = false;
    this.hideMe26c = true;
  }
  nein26() {

    this.checked26a = "false";
    this.checked26b = "true";
    this.data.setData(228, "false", "true", false);
    this.hideMe27 = true;
    this.hideMe26c = false;
  }
  ja26b() {

    this.checked26ba = "true";
    this.checked26bb = "false";
    this.data.setData(2281, "true", "false", false);
    this.hideMe26d = false;
    this.hideMe26c = true;
  }
  nein26b() {

    this.checked26ba = "false";
    this.checked26bb = "true";
    this.data.setData(2281, "false", "true", false);
    this.hideMe26d = true;
    this.hideMe26c = false;
  }
  ja26c() {

    this.checked26ca = "true";
    this.checked26cb = "false";
    this.data.setData(2282, "true", "false", false);
    this.hideMe26d = true;
    this.hideMe27 = false;
  }
  nein26c() {

    this.checked26ca = "false";
    this.checked26cb = "true";
    this.data.setData(2282, "false", "true", false);
    this.hideMe26d = false;
    this.hideMe27 = true;
  }
  ja26d() {

    this.checked26da = "true";
    this.checked26db = "false";
    this.data.setData(2283, "true", "false", false);
    this.HinweisNext();
  }
  nein26d() {

    this.checked26da = "false";
    this.checked26db = "true";
    this.data.setData(2283, "false", "true", false);
    this.Finish();
  }
  ja27() {

    this.checked27a = "true";
    this.checked27b = "false";
    this.data.setData(229, "true", "false", false);
    this.hideMe28 = false;
    this.hideMe29 = false;
  }
  nein27() {

    this.checked27a = "false";
    this.checked27b = "true";
    this.data.setData(229, "false", "true", false);
    this.hideMe28 = false;
    this.hideMe29 = false;

  }
  ja28() {

    this.checked28a = "true";
    this.checked28b = "false";
    this.data.setData(230, "true", "false", false);

  }
  nein28() {

    this.checked28a = "false";
    this.checked28b = "true";
    this.data.setData(230, "true", "false", false);
  }
  ja29() {

    this.checked29a = "true";
    this.checked29b = "false";
    this.data.setData(231, "true", "false", false);
    this.hideMe30 = false;
  }
  nein29() {

    this.checked29a = "false";
    this.checked29b = "true";
    this.data.setData(231, "false", "true", false);
    this.hideMe30 = false;

  }
  ja30() {

    this.checked30a = "true";
    this.checked30b = "false";
    this.data.setData(232, "true", "false", false);
    this.hideMe31 = false;
    this.hideMe32 = true;
  }
  nein30() {

    this.checked30a = "false";
    this.checked30b = "true";
    this.data.setData(232, "false", "true", false);
    this.hideMe31 = true;
    this.hideMe32 = false;
  }
  ja31() {

    this.checked31a = "true";
    this.checked31b = "false";
    this.data.setData(233, "true", "false", false);
    this.hideMe32 = false;
  }
  nein31() {

    this.checked31a = "false";
    this.checked31b = "true";
    this.data.setData(233, "false", "true", false);
    this.hideMe32 = false;

  }
  ja32() {

    this.checked32a = "true";
    this.checked32b = "false";
    this.data.setData(234, "true", "false", false);
    this.hideMe33 = false;
  }
  nein32() {

    this.checked32a = "false";
    this.checked32b = "true";
    this.data.setData(234, "false", "true", false);
    this.hideMe33 = false;

  }
  ja33() {

    this.checked33a = "true";
    this.checked33b = "false";
    this.data.setData(235, "true", "false", false);
    this.Finish();
  }
  nein33() {

    this.checked33a = "false";
    this.checked33b = "true";
    this.data.setData(235, "false", "true", false);
    this.HinweisNext();
  }
 
  Finish() {
    //neue Funktion!
        var data = { message : 'Ende des Verfahrens' };
        var finishPage = this.modalCtrl.create('FinishPage',data);
        finishPage.present();
      }
      
      HinweisNext() {
        //neue Funktion!
            var data = { message : 'Hinweis' };
            var finishPage = this.modalCtrl.create('AdviceNextPage',data);
            finishPage.present();
          }

  Hinweis(txt_Hinweis: string) {
    let alert = this.alertCtrl.create({
      title: 'Hinweis',
      subTitle: txt_Hinweis,
      //subTitle: 'Hinweistitel werden noch ergänzt.',
      buttons: ['Ok']
    });
    alert.present();
  }
  Notizen(qnumber) 
  {
    console.log(qnumber);
    var notizenPage = this.modalCtrl.create('NotePage', { questionnumber:qnumber });
    notizenPage.present();
  }
  Advice(AdviceNr: string)
  {
    var advicePage = this.modalCtrl.create('Advice'+AdviceNr+'Page');
    advicePage.present();
  }
}
