import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0155Page } from './advice0155';

@NgModule({
  declarations: [
    Advice0155Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0155Page),
  ],
})
export class Advice0155PageModule {}
