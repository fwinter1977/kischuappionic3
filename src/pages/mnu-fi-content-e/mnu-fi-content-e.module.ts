import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFiContentEPage } from './mnu-fi-content-e';

@NgModule({
  declarations: [
    MnuFiContentEPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuFiContentEPage),
  ],
})
export class MnuFiContentEPageModule {}
