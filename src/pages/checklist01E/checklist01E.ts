import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';
import { CheckList01D } from '../checklist01D/checklist01D';
//import { HomePage } from '../home/home';
import { AlertController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { AbspeichernPage } from '../abspeichern/abspeichern';
@Component({
  selector: 'page-checklist01E',
  templateUrl: 'checklist01E.html'
})
export class CheckList01E {
  loadedList: any;
  checked55a : string;
  checked55b : string;
  checked56a : string;
  checked56b : string;
  checked57a : string;
  checked57b : string;
  checked58a : string;
  checked58b : string;
  checked59a : string;
  checked59b : string;
  checked60a : string;
  checked60b : string;  
  hideMe55: boolean;
  hideMe56: boolean;
  hideMe57: boolean;
  hideMe58: boolean;
  hideMe59: boolean;
  hideMe60: boolean;

  constructor(public modalCtrl: ModalController, public navCtrl: NavController, public alertCtrl: AlertController, public data : DataProvider, public viewCtrl: ViewController) {
    this.loadedList = this.data.getInitialData();
    if (this.loadedList.length == 0) {
      this.checked55a = "false";
      this.checked55b = "false";
      this.checked56a = "false";
      this.checked56b = "false";
      this.checked57a = "false";
      this.checked57b = "false";
      this.checked58a = "false";
      this.checked58b = "false";
      this.checked59a = "false";
      this.checked59b = "false";
      this.checked60a = "false";
      this.checked60b = "false";
      this.hideMe56 = true;
      this.hideMe57 = true;
      this.hideMe58 = true;
      this.hideMe59 = true;
      this.hideMe60 = true;
    }
    else {
      this.checked55a = "false";
      this.checked55b = "false";
      this.checked56a = "false";
      this.checked56b = "false";
      this.checked57a = "false";
      this.checked57b = "false";
      this.checked58a = "false";
      this.checked58b = "false";
      this.checked59a = "false";
      this.checked59b = "false";
      this.checked60a = "false";
      this.checked60b = "false";
      this.hideMe56 = true;
      this.hideMe57 = true;
      this.hideMe58 = true;
      this.hideMe59 = true;
      this.hideMe60 = true;

      if (this.loadedList[58] != undefined) {
        this.checked55a = this.loadedList[58].checkeda;
        this.checked55b = this.loadedList[58].checkedb;
        this.hideMe55 = this.loadedList[58].hidden;
      }
      if (this.loadedList[59] != undefined) {
        this.checked56a = this.loadedList[59].checkeda;
        this.checked56b = this.loadedList[59].checkedb;
        this.hideMe56 = this.loadedList[59].hidden;
      }
      if (this.loadedList[60] != undefined) {
        this.checked57a = this.loadedList[60].checkeda;
        this.checked57b = this.loadedList[60].checkedb;
        this.hideMe57 = this.loadedList[60].hidden;
      }
      if (this.loadedList[61] != undefined) {
        this.checked58a = this.loadedList[61].checkeda;
        this.checked58b = this.loadedList[61].checkedb;
        this.hideMe58 = this.loadedList[61].hidden;
      }
      if (this.loadedList[62] != undefined) {
        this.checked59a = this.loadedList[62].checkeda;
        this.checked59b = this.loadedList[62].checkedb;
        this.hideMe59 = this.loadedList[62].hidden;
      }
      if (this.loadedList[63] != undefined) {
        this.checked60a = this.loadedList[63].checkeda;
        this.checked60b = this.loadedList[63].checkedb;
        this.hideMe60 = this.loadedList[63].hidden;
      }

    }
  }
    /*
    this.checked55a = "false";
    this.checked55b = "false";
    this.checked56a = "false";
    this.checked56b = "false";
    this.checked57a = "false";
    this.checked57b = "false";
    this.checked58a = "false";
    this.checked58b = "false";
    this.checked59a = "false";
    this.checked59b = "false";
    this.checked60a = "false";
    this.checked60b = "false";
    this.hideMe56 = true;
    this.hideMe57 = true;
    this.hideMe58 = true;
    this.hideMe59 = true;
    this.hideMe60 = true;
  }
  */
 reset() {
  const confirm = this.alertCtrl.create({
    title: 'Zurücksetzen',
    message: 'Möchten Sie diesen Teil der Checkliste zurücksetzen?',
    buttons: [
      {
        text: 'Ja',
        handler: () => {
          this.checked55a = "false";
          this.checked55b = "false";
          this.checked56a = "false";
          this.checked56b = "false";
          this.checked57a = "false";
          this.checked57b = "false";
          this.checked58a = "false";
          this.checked58b = "false";
          this.checked59a = "false";
          this.checked59b = "false";
          this.checked60a = "false";
          this.checked60b = "false";
          this.hideMe56 = true;
          this.hideMe57 = true;
          this.hideMe58 = true;
          this.hideMe59 = true;
          this.hideMe60 = true;
        }
      },
      {
        text: 'nein',
        handler: () => {
          //console.log('Agree clicked');
        }
      }
    ]
  });
  confirm.present();
}

 saveList() {

  this.checked55a = "false";
  this.checked55b = "false";
  this.checked56a = "false";
  this.checked56b = "false";
  this.checked57a = "false";
  this.checked57b = "false";
  this.checked58a = "false";
  this.checked58b = "false";
  this.checked59a = "false";
  this.checked59b = "false";
  this.checked60a = "false";
  this.checked60b = "false";
  this.hideMe56 = true;
  this.hideMe57 = true;
  this.hideMe58 = true;
  this.hideMe59 = true;
  this.hideMe60 = true;
  this.navCtrl.push(AbspeichernPage);
}

  itemPrev(event, item) {
    this.navCtrl.push(CheckList01D);
  }
  itemNext(event, item) {
    this.navCtrl.push(CheckList01E);
  }
  itemHome(event, item) {
    this.viewCtrl.dismiss();
  }

  ja55()
  {
    this.checked55a="true";
    this.checked55b="false";
    this.data.setData(58, "true", "false", false);
    this.hideMe56 = false;
  }
  nein55()
  {
    this.checked55a="false";
    this.checked55b="true";
    this.data.setData(58, "false", "true", false);
    this.hideMe56 = true;
    this.HinweisFeld();
  }
  ja56()
  {
    this.checked56a="true";
    this.checked56b="false";
    this.data.setData(59, "true", "false", false);
    this.hideMe57 = false;
  }
  nein56()
  {
    this.checked56a="false";
    this.checked56b="true";
    this.data.setData(59, "false", "true", false);
    this.hideMe57 = false;
  }
  ja57()
  {
    this.checked57a="true";
    this.checked57b="false";
    this.data.setData(60, "true", "false", false);
    this.hideMe58 = false;
  }
  nein57()
  {
    this.checked57a="false";
    this.checked57b="true";
    this.data.setData(60, "false", "true", false);
    this.hideMe58 = false;
  }
  ja58()
  {
    this.checked58a="true";
    this.checked58b="false";
    this.data.setData(61, "true", "false", false);
    this.hideMe59 = false;
  }
  nein58()
  {
    this.checked58a="false";
    this.checked58b="true";
    this.data.setData(61, "false", "true", false);
    this.hideMe59 = false;
  }
  ja59()
  {
    this.checked59a="true";
    this.checked59b="false";
    this.data.setData(62, "true", "false", false);
    this.hideMe60 = false;
  }
  nein59()
  {
    this.checked59a="false";
    this.checked59b="true";
    this.data.setData(62, "false", "true", false);
    this.hideMe60 = false;
  }
  ja60()
  {
    this.checked60a="true";
    this.checked60b="false";
    this.data.setData(63, "true", "false", false);
    this.FinishJA();
  }
  nein60()
  {
    this.checked60a="false";
    this.checked60b="true";
    this.data.setData(63, "false", "true", false);
    this.FinishJA();
  }

  Finish() {
    //neue Funktion!
        var data = { message : 'Ende des Verfahrens' };
        var finishPage = this.modalCtrl.create('FinishPage',data);
        finishPage.present();
      }

      FinishJA() {
        //neue Funktion!
            var data = { message : 'Ende des Verfahrens' };
            var finishPage = this.modalCtrl.create('FinishjamtPage',data);
            finishPage.present();
          }
      HinweisNext() {
        //neue Funktion!
            var data = { message : 'Hinweis' };
            var finishPage = this.modalCtrl.create('AdviceNextPage',data);
            finishPage.present();
          }
      HinweisFeld() {
        //neue Funktion!
        var data = { message: 'Hinweis' };
        var finishPage = this.modalCtrl.create('AdviceFieldPage', data);
        finishPage.present();
      }
  Hinweis(txt_Hinweis: string) {
    let alert = this.alertCtrl.create({
      title: 'Hinweis',
      subTitle: txt_Hinweis,
      //subTitle: 'Hinweistitel werden noch ergänzt.',
      buttons: ['Ok']
    });
    alert.present();
  }
  Notizen(qnumber) 
  {
    var notizenPage = this.modalCtrl.create('NotePage', { questionnumber:qnumber });
    notizenPage.present();
  }
  Advice(AdviceNr: string)
  {
    var advicePage = this.modalCtrl.create('Advice'+AdviceNr+'Page');
    advicePage.present();
  }
}
