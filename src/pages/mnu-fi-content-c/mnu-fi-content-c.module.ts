import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFiContentCPage } from './mnu-fi-content-c';

@NgModule({
  declarations: [
    MnuFiContentCPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuFiContentCPage),
  ],
})
export class MnuFiContentCPageModule {}
