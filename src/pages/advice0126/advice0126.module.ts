import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0126Page } from './advice0126';

@NgModule({
  declarations: [
    Advice0126Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0126Page),
  ],
})
export class Advice0126PageModule {}
