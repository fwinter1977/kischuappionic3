import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0223Page } from './advice0223';

@NgModule({
  declarations: [
    Advice0223Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0223Page),
  ],
})
export class Advice0223PageModule {}
