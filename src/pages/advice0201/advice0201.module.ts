import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0201Page } from './advice0201';

@NgModule({
  declarations: [
    Advice0201Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0201Page),
  ],
})
export class Advice0201PageModule {}
