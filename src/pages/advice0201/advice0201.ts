import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the Advice0201Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-advice0201',
  templateUrl: 'advice0201.html',
})
export class Advice0201Page {

    public closeAdvice()
    {
      this.viewCtrl.dismiss();
    }
  
    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl : ViewController) {
    }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad Advice0101Page');
    }
  
  }
  