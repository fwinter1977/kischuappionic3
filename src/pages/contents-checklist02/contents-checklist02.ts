import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { CheckList02D } from '../checklist02D/checklist02D';
import { CheckList02C } from '../checklist02C/checklist02C';
import { CheckList02B } from '../checklist02B/checklist02B';
import { CheckList02A } from '../checklist02A/checklist02A';



/**
 * Generated class for the ContentsChecklist02Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contents-checklist02',
  templateUrl: 'contents-checklist02.html',
})
export class ContentsChecklist02Page {

    constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public viewCtrl : ViewController) {
    }
  
    public closeAdvice()
    {
      this.viewCtrl.dismiss();
    }
    Start() {
      this.navCtrl.push(CheckList02A);
    }
  
    Start2() {
      this.navCtrl.push(CheckList02B);
    }
    Start3() {
      this.navCtrl.push(CheckList02C);
    }
    /*
    Start4() {
      this.navCtrl.push(CheckList02D);
    }
    */
    Start4() {
      var GoInPage = this.modalCtrl.create('ContentsChecklist02Mldg01Page');
      GoInPage.present();
    }
    GoContent()
    {
      var GoInPage = this.modalCtrl.create('ContentsChecklist01Page');
      GoInPage.present();
    }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad ContentsChecklist01Page');
    }
  
  }
  