import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContentsChecklist02Page } from './contents-checklist02';

@NgModule({
  declarations: [
    ContentsChecklist02Page,
  ],
  imports: [
    IonicPageModule.forChild(ContentsChecklist02Page),
  ],
})
export class ContentsChecklist02PageModule {}
