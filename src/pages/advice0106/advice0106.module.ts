import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0106Page } from './advice0106';

@NgModule({
  declarations: [
    Advice0106Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0106Page),
  ],
})
export class Advice0106PageModule {}
