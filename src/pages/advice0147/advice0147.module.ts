import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0147Page } from './advice0147';

@NgModule({
  declarations: [
    Advice0147Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0147Page),
  ],
})
export class Advice0147PageModule {}
