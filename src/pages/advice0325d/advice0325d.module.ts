import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0325dPage } from './advice0325d';

@NgModule({
  declarations: [
    Advice0325dPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0325dPage),
  ],
})
export class Advice0325dPageModule {}
