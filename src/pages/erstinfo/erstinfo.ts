import { Component } from '@angular/core';
import { NavController, NavParams, Platform, AlertController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { Storage} from '@ionic/storage';

@Component({
  templateUrl: 'erstinfo.html'
})
export class ErstInfoPage {
  platform: Platform;
  not1stTime:boolean;
  autoUpdate:boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, platform: Platform, private alertCtrl: AlertController, public storage: Storage) 
  {
    this.platform = platform;
  }

  /*
  ionViewDidLoad() {
    console.log('ionViewDidLoad IntroPage');
    this.storage.get('notFirstStart').then((_1st) => {
      this.not1stTime = _1st;
      console.log(this.not1stTime);
      if(this.not1stTime==true)
      {
        console.log("if two");
        this.navCtrl.setRoot(LoginPage);
      }
      else
      {
        this.storage.set('notFirstStart', true);
      }
    });
  }
*/
ngOnInit()
{
  this.storage.get('autoUpdate').then((_update) => {
    this.autoUpdate = _update;
  });
}
Updates() {


  let alert = this.alertCtrl.create({
    title: 'Updates',
    subTitle: 'Soll die App im laufenden Betrieb selbsttätig nach Updates suchen?',
    buttons: [
      {
        text: 'Ja',
        role: 'cancel',
        handler: () => {
          
          console.log('Cancel clicked');
          this.storage.set('autoUpdate', true);

        }
      },
      {
        text: 'Nein',
        handler: () => {
          console.log('Buy clicked');
          this.storage.set('autoUpdate', false);

        }
      }
    ]
  });
  alert.present();
}

Links() {
  let alert = this.alertCtrl.create({
    title: 'Hinweis',
    subTitle: 'Diese App enthält vereinzelt Links zu Internetseiten, die für Sie von Interesse sein können. <br/>Beachten Sie Folgendes, wenn Sie einen Link öffnen: <br/><b>Datensicherheit</b><br/> Sie werden nicht automatisch ausgeloggt. Die Kinderschutz-App bleibt geöffnet.<br/><b>Datenschutz</b><br/>Es entsteht ein Datenverkehr zwischen dem Smartphone und der Internetseite. Lesen Sie hierzu unsere Datenschutzhinweise.',
    buttons: [
      {
        text: 'Verstanden',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');

        }
      },
    ]
  });
  alert.present();
}

  startApp()
  {
    if (this.autoUpdate == undefined)
    {
      this.Updates();
      this.Links();
    }
    this.navCtrl.setRoot(LoginPage);
  }



  slides = [
    {
      title: "Herzlich Willkommen!",
      description: "Herzlich Willkommen in der Kinderschutz-App des Landes Mecklenburg-Vorpommern.<br/><br/><br/><br/><br/>",
      image: "assets/imgs/Bild Seite (1).png",
    },
    {
      title: "Orientierung und Hilfe",
      description: "Die Kinderschutz-App bietet Ihnen Orientierung und Hilfestellung im Kinderschutz-Verfahren.<br/><br/><br/><br/>",
      image: "assets/imgs/Bild Seite (2).png",
    },
    {
      title: "Checklisten wählen",
      description: "Wählen Sie dazu Ihre Checkliste aus und bestimmen Sie den Stand Ihres Verfahrens.<br/><br/><br/><br/><br/>",
      image: "assets/imgs/Bild Seite (3).png",
    },
    {
      title: "Hinweise und eigene Notizen",
      description: "Sie finden in der Kinderschutz-App zu allen Fragen weiterführende Hinweise und können Notizen speichern. Wischen Sie dazu unterhalb der Fragen über das angezeigte Symbol.<br/><br/>",
      image: "assets/imgs/Bild Seite (4).png",
    },
    {
      title: "Datenschutz",
      description: "Aber Achtung! Speichern Sie niemals personenbezogene Informationen, die das Kind oder den Einzelfall betreffen.<br/>Notizen und Hinweise sind <u><b>ausschließlich</b></u> als Hilfe gedacht, um Ihr Verfahren stetig zu verbessern.",
      image: "assets/imgs/Bild Seite (5).png",
    },
    {
      title: "Noch mehr Informationen",
      description: "Weitere Informationen zu den Funktionen und zum Gebrauch der Kinderschutz-App finden Sie im Menü (oben rechts).<br/><br/><br/><br/>",
      image: "assets/imgs/Bild Seite (6).png",
    },
    {
      title: "Los gehts!",
      description: "Viel Erfolg und gutes Gelingen.<br/><br/><br/><br/><br/><br/>",
      image: "assets/imgs/Bild Seite (7).png",
    }
  ];
}
