import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFibContentDPage } from './mnu-fib-content-d';

@NgModule({
  declarations: [
    MnuFibContentDPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuFibContentDPage),
  ],
})
export class MnuFibContentDPageModule {}
