import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { GoInChecklist03Page } from '../go-in-checklist03/go-in-checklist03';
import { Storage} from '@ionic/storage';

/**
 * Generated class for the Speicher3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-speicher3',
  templateUrl: 'speicher3.html',
})
export class Speicher3Page {
  filename1 : string = "leer";
  filename2 : string = "leer";
  filename3 : string = "leer";
  emptyMessage : string;
  hidden01; hidden02; hidden03  : boolean;



  constructor(public navCtrl: NavController, public data: DataProvider, public storage: Storage, public modalCtrl: ModalController, public viewCtrl : ViewController) {
    //this.emptyMessage = "Es sind gegenwärtig keine Fallakten gespeichert!";
    
    this.hidden01 = true;
    this.hidden02 = true;
    this.hidden03 = true;
    
  }
  GoBack()
  {
    this.viewCtrl.dismiss();
    this.navCtrl.push(GoInChecklist03Page);
  }
  ionViewDidLoad() {
    this.storage.get('filename1').then((_pw) => {
      this.filename1 = _pw;
    });
    this.storage.get('filename2').then((_pw) => {
      this.filename2 = _pw;
    });
    this.storage.get('filename3').then((_pw) => {
      this.filename3 = _pw;
    });
  }


  loadList1()
  {
    //this.viewCtrl.dismiss();
    this.data.getData('1');    
    var GoInPage = this.modalCtrl.create('ContentsChecklist03Page');
    GoInPage.present();
  }
  loadList2()
  {

    this.data.getData('2');    
    var GoInPage = this.modalCtrl.create('ContentsChecklist03Page');
    GoInPage.present();
  }
  loadList3()
  {

    this.data.getData('3');    
    var GoInPage = this.modalCtrl.create('ContentsChecklist03Page');
    GoInPage.present();
  }
}