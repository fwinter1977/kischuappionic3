import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0309Page } from './advice0309';

@NgModule({
  declarations: [
    Advice0309Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0309Page),
  ],
})
export class Advice0309PageModule {}
