import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0333Page } from './advice0333';

@NgModule({
  declarations: [
    Advice0333Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0333Page),
  ],
})
export class Advice0333PageModule {}
