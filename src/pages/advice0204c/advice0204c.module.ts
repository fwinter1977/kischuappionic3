import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0204cPage } from './advice0204c';

@NgModule({
  declarations: [
    Advice0204cPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0204cPage),
  ],
})
export class Advice0204cPageModule {}
