import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0121Page } from './advice0121';

@NgModule({
  declarations: [
    Advice0121Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0121Page),
  ],
})
export class Advice0121PageModule {}
