import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0119Page } from './advice0119';

@NgModule({
  declarations: [
    Advice0119Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0119Page),
  ],
})
export class Advice0119PageModule {}
