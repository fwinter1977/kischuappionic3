import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Finish02Page } from './finish02';

@NgModule({
  declarations: [
    Finish02Page,
  ],
  imports: [
    IonicPageModule.forChild(Finish02Page),
  ],
})
export class Finish02PageModule {}
