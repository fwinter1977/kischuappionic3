import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFibContentAPage } from './mnu-fib-content-a';

@NgModule({
  declarations: [
    MnuFibContentAPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuFibContentAPage),
  ],
})
export class MnuFibContentAPageModule {}
