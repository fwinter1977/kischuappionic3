import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { KiWoGe01A } from '../kiwoge01A/kiwoge01A';
import { KiWoGe01B } from '../kiwoge01B/kiwoge01B';
import { KiWoGe01C } from '../kiwoge01C/kiwoge01C';
import { KiWoGe01D } from '../kiwoge01D/kiwoge01D';
import { KiWoGe01E } from '../kiwoge01E/kiwoge01E';
import { KiWoGe01F } from '../kiwoge01F/kiwoge01F';
import { KiWoGe01G } from '../kiwoge01G/kiwoge01G';
@Component({
  selector: 'page-kiwoge01',
  templateUrl: 'kiwoge01.html'
})
export class KiWoGe01 {


  constructor(public navCtrl: NavController) {

  }

  itemNutzg01(event, item) {
    this.navCtrl.push(KiWoGe01A);
  }
  itemNutzg02(event, item) {
    this.navCtrl.push(KiWoGe01B);
  }
  itemNutzg03(event, item) {
    this.navCtrl.push(KiWoGe01C);
  }
  itemNutzg04(event, item) {
    this.navCtrl.push(KiWoGe01D);
  }
  itemNutzg05(event, item) {
    this.navCtrl.push(KiWoGe01E);
  }
  itemNutzg06(event, item) {
    this.navCtrl.push(KiWoGe01F);
  }
  itemNutzg07(event, item) {
    this.navCtrl.push(KiWoGe01G);
  }



}
