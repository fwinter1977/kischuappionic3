import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0143Page } from './advice0143';

@NgModule({
  declarations: [
    Advice0143Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0143Page),
  ],
})
export class Advice0143PageModule {}
