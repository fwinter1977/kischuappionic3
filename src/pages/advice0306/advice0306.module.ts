import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0306Page } from './advice0306';

@NgModule({
  declarations: [
    Advice0306Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0306Page),
  ],
})
export class Advice0306PageModule {}
