import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0128Page } from './advice0128';

@NgModule({
  declarations: [
    Advice0128Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0128Page),
  ],
})
export class Advice0128PageModule {}
