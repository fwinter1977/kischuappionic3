import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0319Page } from './advice0319';

@NgModule({
  declarations: [
    Advice0319Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0319Page),
  ],
})
export class Advice0319PageModule {}
