import { Component } from '@angular/core';
import {NutzungsPage} from '../nutzung/nutzung';
import { HomePage } from '../home/home';
import { CheckList01 } from '../checklist01/checklist01';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root = CheckList01;
  tab2Root = NutzungsPage;
  tab3Root = NutzungsPage;

  constructor() {

  }
}
