import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Nutzung01 } from '../nutzung01/nutzung01';
import { Nutzung02 } from '../nutzung02/nutzung02';

@Component({
  selector: 'page-nutzung',
  templateUrl: 'nutzung.html'
})
export class NutzungsPage {

  constructor(public navCtrl: NavController) {



  }

  itemDieApp(event, item) {
    this.navCtrl.push(Nutzung01);
  }
  itemBedienung(event, item) {
    this.navCtrl.push(Nutzung02);
  }
}
