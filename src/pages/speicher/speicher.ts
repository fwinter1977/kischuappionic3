import { Component } from '@angular/core';
import { NavController, ModalController, ViewController, AlertController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
//import { CheckList01 } from '../checklist01/checklist01';
import { GoInChecklist01Page } from '../go-in-checklist01/go-in-checklist01';
import { Storage} from '@ionic/storage';
import { ContentsChecklist01Page } from '../contents-checklist01/contents-checklist01';
import { LoginPage } from '../login/login';
import { HomePage } from '../home/home';


@Component({
  selector: 'page-speicher',
  templateUrl: 'speicher.html'
})
export class SpeicherPage {

  emptyMessage : string;
  hidden01; hidden02; hidden03 : boolean;
  loggedin : boolean;
  filename1 : string = "leer";
  filename2 : string = "leer";
  filename3 : string = "leer";



  constructor(public navCtrl: NavController, public data: DataProvider, public storage: Storage, public modalCtrl: ModalController, public viewCtrl : ViewController , public alertCtrl : AlertController) {
    //this.emptyMessage = "Es sind gegenwärtig keine Fallakten gespeichert!";
    
    this.hidden01 = true;
    this.hidden02 = true;
    this.hidden03 = true;
    
    
  }
  GoBack()
  {
    this.navCtrl.setRoot(HomePage);
    //this.navCtrl.push(this.navCtrl.getPrevious());
  }
  ionViewDidLoad() {
    this.storage.get('filename1').then((_pw) => {
      this.filename1 = _pw;
    });
    this.storage.get('filename2').then((_pw) => {
      this.filename2 = _pw;
    });
    this.storage.get('filename3').then((_pw) => {
      this.filename3 = _pw;
    });
  }


  loadList1()
  {
    this.loggedin = this.data.getLoggedIn();
    console.log(this.loggedin);
    if (this.loggedin == true)
    {
      this.data.getData('1');    
      var GoInPage = this.modalCtrl.create('ContentsChecklist01Page');
      GoInPage.present();
    }
    else
    {
      let alert = this.alertCtrl.create({
        title: 'Achtung',
        subTitle: 'Aus datenschutztechnischen Gründen müssen Sie sich zunächst einloggen, um diese Funktion zu nutzen!',
        buttons: ['Ok']
      });
      alert.present();
    }

  }
  loadList2()
  {

    this.loggedin = this.data.getLoggedIn();
    console.log(this.loggedin);
    if (this.loggedin == true)
    {
      this.data.getData('2');    
      var GoInPage = this.modalCtrl.create('ContentsChecklist01Page');
      GoInPage.present();
    }
    else
    {
      let alert = this.alertCtrl.create({
        title: 'Achtung',
        subTitle: 'Aus datenschutztechnischen Gründen müssen Sie sich zunächst einloggen, um diese Funktion zu nutzen!',
        buttons: ['Ok']
      });
      alert.present();
    }


  }
  loadList3()
  {
    this.loggedin = this.data.getLoggedIn();
    console.log(this.loggedin);
    if (this.loggedin == true)
    {
      this.data.getData('3');    
      var GoInPage = this.modalCtrl.create('ContentsChecklist01Page');
      GoInPage.present();
    }
    else
    {
      let alert = this.alertCtrl.create({
        title: 'Achtung',
        subTitle: 'Aus datenschutztechnischen Gründen müssen Sie sich zunächst einloggen, um diese Funktion zu nutzen!',
        buttons: ['Ok']
      });
      alert.present();
    }

  }
}
