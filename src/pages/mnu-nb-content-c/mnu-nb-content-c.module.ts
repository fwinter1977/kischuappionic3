import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuNbContentCPage } from './mnu-nb-content-c';

@NgModule({
  declarations: [
    MnuNbContentCPage,
  ],
  imports: [
    IonicPageModule.forChild(MnuNbContentCPage),
  ],
})
export class MnuNbContentCPageModule {}
