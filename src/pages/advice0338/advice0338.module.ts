import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0338Page } from './advice0338';

@NgModule({
  declarations: [
    Advice0338Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0338Page),
  ],
})
export class Advice0338PageModule {}
