import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NutzungsbedingungenPage } from './nutzungsbedingungen';

@NgModule({
  declarations: [
    NutzungsbedingungenPage,
  ],
  imports: [
    IonicPageModule.forChild(NutzungsbedingungenPage),
  ],
})
export class NutzungsbedingungenPageModule {}
