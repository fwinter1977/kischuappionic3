import { Component } from '@angular/core';
import { ModalController, ViewController } from 'ionic-angular';
import { CheckList01 } from '../checklist01/checklist01';
import { CheckList01C } from '../checklist01C/checklist01C';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
//import { HomePage } from '../home/home';
import { DataProvider } from '../../providers/data/data';
import { AbspeichernPage } from '../abspeichern/abspeichern';
@Component({
  selector: 'page-checklist01B',
  templateUrl: 'checklist01B.html'
})
export class CheckList01B {
  checked10a: string;
  checked10b: string;
  checked11a: string;
  checked11b: string;
  checked12a: string;
  checked12b: string;
  checked12aa: string;
  checked12ab: string;
  checked13a: string;
  checked13b: string;
  checked14a: string;
  checked14b: string;
  checked15a: string;
  checked15b: string;
  checked16a: string;
  checked16b: string;
  checked17a: string;
  checked17b: string;
  checked18a: string;
  checked18b: string;
  checked19a: string;
  checked19b: string;
  checked20a: string;
  checked20b: string;
  checked21a: string;
  checked21b: string;
  checked22a: string;
  checked22b: string;
  checked23aa: string;
  checked23ab: string;
  checked23ba: string;
  checked23bb: string;
  hideMe10: boolean;
  hideMe11: boolean;
  hideMe12: boolean;
  hideMe12a: boolean;
  hideMe13: boolean;
  hideMe14: boolean;
  hideMe15: boolean;
  hideMe16: boolean;
  hideMe17: boolean;
  hideMe18: boolean;
  hideMe19: boolean;
  hideMe20: boolean;
  hideMe21: boolean;
  hideMe22: boolean;
  hideMe23a: boolean;
  hideMe23b: boolean;
  loadedList: any;

  constructor(public modalCtrl: ModalController, public navCtrl: NavController, public alertCtrl: AlertController, public data: DataProvider, public viewCtrl: ViewController) {
    this.loadedList = this.data.getInitialData();

    if (this.loadedList.length == 0) {
      this.checked10a = "false";
      this.checked10b = "false";
      this.checked11a = "false";
      this.checked11b = "false";
      this.checked12a = "false";
      this.checked12b = "false";
      this.checked12aa = "false";
      this.checked12ab = "false";
      this.checked13a = "false";
      this.checked13b = "false";
      this.checked14a = "false";
      this.checked14b = "false";
      this.checked15a = "false";
      this.checked15b = "false";
      this.checked16a = "false";
      this.checked16b = "false";
      this.checked17a = "false";
      this.checked17b = "false";
      this.checked18a = "false";
      this.checked18b = "false";
      this.checked19a = "false";
      this.checked19b = "false";
      this.checked20a = "false";
      this.checked20b = "false";
      this.checked21a = "false";
      this.checked21b = "false";
      this.checked22a = "false";
      this.checked22b = "false";
      this.checked23aa = "false";
      this.checked23ab = "false";
      this.checked23ba = "false";
      this.checked23bb = "false";
      this.hideMe11 = true;
      this.hideMe12 = true;
      this.hideMe12a = true;
      this.hideMe13 = true;
      this.hideMe14 = true;
      this.hideMe15 = true;
      this.hideMe16 = true;
      this.hideMe17 = true;
      this.hideMe18 = true;
      this.hideMe19 = true;
      this.hideMe20 = true;
      this.hideMe21 = true;
      this.hideMe22 = true;
      this.hideMe23a = true;
      this.hideMe23b = true;
    }
    else {

      this.checked10a = "false";
      this.checked10b = "false";
      this.checked11a = "false";
      this.checked11b = "false";
      this.checked12a = "false";
      this.checked12b = "false";
      this.checked12aa = "false";
      this.checked12ab = "false";
      this.checked13a = "false";
      this.checked13b = "false";
      this.checked14a = "false";
      this.checked14b = "false";
      this.checked15a = "false";
      this.checked15b = "false";
      this.checked16a = "false";
      this.checked16b = "false";
      this.checked17a = "false";
      this.checked17b = "false";
      this.checked18a = "false";
      this.checked18b = "false";
      this.checked19a = "false";
      this.checked19b = "false";
      this.checked20a = "false";
      this.checked20b = "false";
      this.checked21a = "false";
      this.checked21b = "false";
      this.checked22a = "false";
      this.checked22b = "false";
      this.checked23aa = "false";
      this.checked23ab = "false";
      this.checked23ba = "false";
      this.checked23bb = "false";
      this.hideMe11 = true;
      this.hideMe12 = true;
      this.hideMe12a = true;
      this.hideMe13 = true;
      this.hideMe14 = true;
      this.hideMe15 = true;
      this.hideMe16 = true;
      this.hideMe17 = true;
      this.hideMe18 = true;
      this.hideMe19 = true;
      this.hideMe20 = true;
      this.hideMe21 = true;
      this.hideMe22 = true;
      this.hideMe23a = true;
      this.hideMe23b = true;

      if (this.loadedList[10] != undefined) {
        this.checked10a = this.loadedList[10].checkeda;
        this.checked10b = this.loadedList[10].checkedb;
        this.hideMe10 = this.loadedList[10].hidden;
      }
      if (this.loadedList[11] != undefined) {
        this.checked11a = this.loadedList[11].checkeda;
        this.checked11b = this.loadedList[11].checkedb;
        this.hideMe11 = this.loadedList[11].hidden;
      }
      if (this.loadedList[12] != undefined) {
        this.checked12a = this.loadedList[12].checkeda;
        this.checked12b = this.loadedList[12].checkedb;
        this.hideMe12 = this.loadedList[12].hidden;
      }

      if (this.loadedList[64] != undefined) {
        this.checked12a = this.loadedList[64].checkeda;
        this.checked12b = this.loadedList[64].checkedb;
        this.hideMe12 = this.loadedList[64].hidden;
      }

      if (this.loadedList[13] != undefined) {
        this.checked13a = this.loadedList[13].checkeda;
        this.checked13b = this.loadedList[13].checkedb;
        this.hideMe13 = this.loadedList[13].hidden;
      }
      if (this.loadedList[14] != undefined) {
        this.checked14a = this.loadedList[14].checkeda;
        this.checked14b = this.loadedList[14].checkedb;
        this.hideMe14 = this.loadedList[14].hidden;
      }
      if (this.loadedList[15] != undefined) {
        this.checked15a = this.loadedList[15].checkeda;
        this.checked15b = this.loadedList[15].checkedb;
        this.hideMe15 = this.loadedList[15].hidden;
      }
      if (this.loadedList[16] != undefined) {
        this.checked16a = this.loadedList[16].checkeda;
        this.checked16b = this.loadedList[16].checkedb;
        this.hideMe16 = this.loadedList[16].hidden;
      }
      if (this.loadedList[17] != undefined) {
        this.checked17a = this.loadedList[17].checkeda;
        this.checked17b = this.loadedList[17].checkedb;
        this.hideMe17 = this.loadedList[17].hidden;
      }
      if (this.loadedList[18] != undefined) {
        this.checked18a = this.loadedList[18].checkeda;
        this.checked18b = this.loadedList[18].checkedb;
        this.hideMe18 = this.loadedList[18].hidden;
      }
      if (this.loadedList[19] != undefined) {
        this.checked19a = this.loadedList[19].checkeda;
        this.checked19b = this.loadedList[19].checkedb;
        this.hideMe19 = this.loadedList[19].hidden;
      }
      if (this.loadedList[20] != undefined) {
        this.checked20a = this.loadedList[20].checkeda;
        this.checked20b = this.loadedList[20].checkedb;
        this.hideMe20 = this.loadedList[20].hidden;
      }
      if (this.loadedList[21] != undefined) {
        this.checked21a = this.loadedList[21].checkeda;
        this.checked21b = this.loadedList[21].checkedb;
        this.hideMe21 = this.loadedList[21].hidden;
      }
      if (this.loadedList[22] != undefined) {
        this.checked22a = this.loadedList[22].checkeda;
        this.checked22b = this.loadedList[22].checkedb;
        this.hideMe22 = this.loadedList[22].hidden;
      }
      if (this.loadedList[23] != undefined) {
        this.checked23aa = this.loadedList[23].checkeda;
        this.checked23ab = this.loadedList[23].checkedb;
        this.hideMe19 = this.loadedList[23].hidden;
      }
      if (this.loadedList[24] != undefined) {
        this.checked23ba = this.loadedList[24].checkeda;
        this.checked23bb = this.loadedList[24].checkedb;
        this.hideMe19 = this.loadedList[24].hidden;
      }
    }
    /*
    this.checked10a = "false";
    this.checked10b = "false";
    this.checked11a = "false";
    this.checked11b = "false";
    this.checked12a = "false";
    this.checked12b = "false";
    this.checked13a = "false";
    this.checked13b = "false";
    this.checked14a = "false";
    this.checked14b = "false";
    this.checked15a = "false";
    this.checked15b = "false";
    this.checked16a = "false";
    this.checked16b = "false";
    this.checked17a = "false";
    this.checked17b = "false";
    this.checked18a = "false";
    this.checked18b = "false";
    this.checked19a = "false";
    this.checked19b = "false";
    this.checked20a = "false";
    this.checked20b = "false";
    this.checked21a = "false";
    this.checked21b = "false";
    this.checked22a = "false";
    this.checked22b = "false";
    this.checked23aa = "false";
    this.checked23ab = "false";
    this.checked23ba = "false";
    this.checked23bb = "false";
    this.hideMe11 = true;
    this.hideMe12 = true;
    this.hideMe13 = true;
    this.hideMe14 = true;
    this.hideMe15 = true;
    this.hideMe16 = true;
    this.hideMe17 = true;
    this.hideMe18 = true;
    this.hideMe19 = true;
    this.hideMe20 = true;
    this.hideMe21 = true;
    this.hideMe22 = true;
    this.hideMe23a = true;
    this.hideMe23b = true;
    */
  }

  reset() {
    const confirm = this.alertCtrl.create({
      title: 'Zurücksetzen',
      message: 'Möchten Sie diesen Teil der Checkliste zurücksetzen?',
      buttons: [
        {
          text: 'Ja',
          handler: () => {
            this.checked10a = "false";
            this.checked10b = "false";
            this.checked11a = "false";
            this.checked11b = "false";
            this.checked12a = "false";
            this.checked12b = "false";
            this.checked12aa = "false";
            this.checked12ab = "false";
            this.checked13a = "false";
            this.checked13b = "false";
            this.checked14a = "false";
            this.checked14b = "false";
            this.checked15a = "false";
            this.checked15b = "false";
            this.checked16a = "false";
            this.checked16b = "false";
            this.checked17a = "false";
            this.checked17b = "false";
            this.checked18a = "false";
            this.checked18b = "false";
            this.checked19a = "false";
            this.checked19b = "false";
            this.checked20a = "false";
            this.checked20b = "false";
            this.checked21a = "false";
            this.checked21b = "false";
            this.checked22a = "false";
            this.checked22b = "false";
            this.checked23aa = "false";
            this.checked23ab = "false";
            this.checked23ba = "false";
            this.checked23bb = "false";
            this.hideMe11 = true;
            this.hideMe12 = true;
            this.hideMe12a = true;
            this.hideMe13 = true;
            this.hideMe14 = true;
            this.hideMe15 = true;
            this.hideMe16 = true;
            this.hideMe17 = true;
            this.hideMe18 = true;
            this.hideMe19 = true;
            this.hideMe20 = true;
            this.hideMe21 = true;
            this.hideMe22 = true;
            this.hideMe23a = true;
            this.hideMe23b = true;
          }
        },
        {
          text: 'nein',
          handler: () => {
            //console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }


  saveList() {

    this.checked10a = "false";
    this.checked10b = "false";
    this.checked11a = "false";
    this.checked11b = "false";
    this.checked12a = "false";
    this.checked12b = "false";
    this.checked12aa = "false";
    this.checked12ab = "false";
    this.checked13a = "false";
    this.checked13b = "false";
    this.checked14a = "false";
    this.checked14b = "false";
    this.checked15a = "false";
    this.checked15b = "false";
    this.checked16a = "false";
    this.checked16b = "false";
    this.checked17a = "false";
    this.checked17b = "false";
    this.checked18a = "false";
    this.checked18b = "false";
    this.checked19a = "false";
    this.checked19b = "false";
    this.checked20a = "false";
    this.checked20b = "false";
    this.checked21a = "false";
    this.checked21b = "false";
    this.checked22a = "false";
    this.checked22b = "false";
    this.checked23aa = "false";
    this.checked23ab = "false";
    this.checked23ba = "false";
    this.checked23bb = "false";
    this.hideMe11 = true;
    this.hideMe12 = true;
    this.hideMe12a = true;
    this.hideMe13 = true;
    this.hideMe14 = true;
    this.hideMe15 = true;
    this.hideMe16 = true;
    this.hideMe17 = true;
    this.hideMe18 = true;
    this.hideMe19 = true;
    this.hideMe20 = true;
    this.hideMe21 = true;
    this.hideMe22 = true;
    this.hideMe23a = true;
    this.hideMe23b = true;
    this.navCtrl.push(AbspeichernPage);
  }
  itemPrev(event, item) {
    this.navCtrl.push(CheckList01);
  }
  itemNext(event, item) {
    this.navCtrl.push(CheckList01C);
  }
  itemHome(event, item) {

    this.viewCtrl.dismiss();
  }
  ja10() {
    this.checked10a = "true";
    this.checked10b = "false";
    this.data.setData(10, "true", "false", false);
    this.hideMe11 = false;

  }
  nein10() {
    this.checked10a = "false";
    this.checked10b = "true";
    this.data.setData(10, "false", "true", false);
    this.hideMe11 = false;

  }

  ja11() {

    this.checked11a = "true";
    this.checked11b = "false";
    this.data.setData(11, "true", "false", false);
    this.hideMe12 = true;
    this.HinweisJugendamt();
  }
  nein11() {

    this.checked11a = "false";
    this.checked11b = "true";
    this.data.setData(11, "false", "true", false);
    this.hideMe12 = false;

  }
  ja12() {

    this.checked12a = "true";
    this.checked12b = "false";
    this.data.setData(12, "true", "false", false);
    this.hideMe12a = false;

  }
  nein12() {

    this.checked12a = "false";
    this.checked12b = "true";
    this.data.setData(12, "false", "true", false);
    this.hideMe12a = true;
    this.HinweisFeld();

  }

  ja12a() {

    this.checked12aa = "true";
    this.checked12ab = "false";
    this.data.setData(64, "true", "false", false);
    this.hideMe13 = false;
    //this.HinweisNext();

  }
  nein12a() {

    this.checked12aa = "false";
    this.checked12ab = "true";
    this.data.setData(64, "false", "true", false);
    this.hideMe13 = true;
    this.HinweisNext();

  }

  ja13() {

    this.checked13a = "true";
    this.checked13b = "false";
    this.data.setData(13, "true", "false", false);
    this.hideMe14 = false;
  }
  nein13() {

    this.checked13a = "false";
    this.checked13b = "true";
    this.data.setData(13, "false", "true", false);
    this.hideMe14 = false;
  }
  ja14() {

    this.checked14a = "true";
    this.checked14b = "false";
    this.data.setData(14, "true", "false", false);
    this.hideMe15 = false;
  }
  nein14() {

    this.checked14a = "false";
    this.checked14b = "true";
    this.data.setData(14, "false", "true", false);
    this.hideMe15 = false;
  }
  ja15() {

    this.checked15a = "true";
    this.checked15b = "false";
    this.data.setData(15, "true", "false", false);
    this.hideMe16 = false;
    this.hideMe17 = true;
  }
  nein15() {

    this.checked15a = "false";
    this.checked15b = "true";
    this.data.setData(15, "false", "true", false);
    this.hideMe16 = true;
    this.hideMe17 = false;
  }
  ja16() {

    this.checked16a = "true";
    this.checked16b = "false";
    this.data.setData(15, "true", "false", false);
    this.hideMe21 = false;
    this.hideMe17 = true;
  }
  nein16() {

    this.checked16a = "false";
    this.checked16b = "true";
    this.data.setData(16, "false", "true", false);
    this.hideMe21 = true;
    this.hideMe17 = false;
  }
  ja17() {

    this.checked17a = "true";
    this.checked17b = "false";
    this.data.setData(17, "true", "false", false);
    this.hideMe18 = false;
  }
  nein17() {

    this.checked17a = "false";
    this.checked17b = "true";
    this.data.setData(17, "false", "true", false);
    this.hideMe18 = false;
  }
  ja18() {

    this.checked18a = "true";
    this.checked18b = "false";
    this.data.setData(18, "true", "false", false);
    this.hideMe19 = false;
  }
  nein18() {

    this.checked18a = "false";
    this.checked18b = "true";
    this.data.setData(18, "false", "true", false);
    this.hideMe19 = false;
  }
  ja19() {

    this.checked19a = "true";
    this.checked19b = "false";
    this.data.setData(19, "true", "false", false);
    this.hideMe20 = false;
  }
  nein19() {

    this.checked19a = "false";
    this.checked19b = "true";
    this.data.setData(19, "false", "true", false);
    this.hideMe20 = false;
  }
  ja20() {

    this.checked20a = "true";
    this.checked20b = "false";
    this.data.setData(20, "true", "false", false);
    this.hideMe21 = false;
  }
  nein20() {

    this.checked20a = "false";
    this.checked20b = "true";
    this.data.setData(20, "false", "true", false);
    this.hideMe21 = false;
  }
  ja21() {

    this.checked21a = "true";
    this.checked21b = "false";
    this.data.setData(21, "true", "false", false);
    this.hideMe22 = false;
  }

  nein21() {
    this.checked21a = "false";
    this.checked21b = "true";
    this.data.setData(21, "false", "true", false);
    this.hideMe22 = true;
    this.HinweisFeld();
  }

  ja22() {
    this.checked22a = "true";
    this.checked22b = "false";
    this.data.setData(22, "true", "false", false);
    this.hideMe23a = false;
  }

  nein22() {

    this.checked22a = "false";
    this.checked22b = "true";
    this.data.setData(22, "false", "true", false);
    this.hideMe23a = true;
    this.HinweisFeld();
  }
  ja23a() {

    this.checked23aa = "true";
    this.checked23ab = "false";
    this.data.setData(23, "true", "false", false);
    this.hideMe23b = false;

  }
  nein23a() {

    this.checked23aa = "false";
    this.checked23ab = "true";
    this.data.setData(23, "false", "true", false);
    this.hideMe23b = true;
    this.HinweisFeld();
  }
  ja23b() {

    this.checked23ba = "true";
    this.checked23bb = "false";
    this.data.setData(24, "true", "false", false);
    this.HinweisNext();
  }
  nein23b() {

    this.checked23ba = "false";
    this.checked23bb = "true";
    this.data.setData(24, "false", "true", false);
    this.Finish();
  }

  HinweisNext() {
    //neue Funktion!
    var data = { message: 'Hinweis' };
    var finishPage = this.modalCtrl.create('AdviceNextPage', data);
    finishPage.present();
  }
  HinweisJugendamt() {
    //neue Funktion!
    var data = { message: 'Hinweis' };
    var finishPage = this.modalCtrl.create('AdviceJugendamtPage', data);
    finishPage.present();
  }

  HinweisFeld() {
    //neue Funktion!
    var data = { message: 'Hinweis' };
    var finishPage = this.modalCtrl.create('AdviceFieldPage', data);
    finishPage.present();
  }


  Hinweis(txt_Hinweis: string) {
    let alert = this.alertCtrl.create({
      title: 'Hinweis',
      subTitle: txt_Hinweis,
      //subTitle: 'Hinweistitel werden noch ergänzt.',
      buttons: ['Ok']
    });
    alert.present();
  }


  Finish() {
    //neue Funktion!
    var data = { message: 'Ende des Verfahrens' };
    var finishPage = this.modalCtrl.create('FinishPage', data);
    finishPage.present();
  }
  Notizen(qnumber) {
    var notizenPage = this.modalCtrl.create('NotePage', { questionnumber: qnumber });
    notizenPage.present();
  }

  Advice(AdviceNr: string) {
    var advicePage = this.modalCtrl.create('Advice' + AdviceNr + 'Page');
    advicePage.present();
  }
}
