import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFicContentA1Page } from './mnu-fic-content-a1';

@NgModule({
  declarations: [
    MnuFicContentA1Page,
  ],
  imports: [
    IonicPageModule.forChild(MnuFicContentA1Page),
  ],
})
export class MnuFicContentA1PageModule {}
