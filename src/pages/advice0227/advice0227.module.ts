import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0227Page } from './advice0227';

@NgModule({
  declarations: [
    Advice0227Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0227Page),
  ],
})
export class Advice0227PageModule {}
