import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MnuFicContentA2Page } from './mnu-fic-content-a2';

@NgModule({
  declarations: [
    MnuFicContentA2Page,
  ],
  imports: [
    IonicPageModule.forChild(MnuFicContentA2Page),
  ],
})
export class MnuFicContentA2PageModule {}
