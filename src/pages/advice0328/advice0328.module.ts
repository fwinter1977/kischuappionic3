import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0328Page } from './advice0328';

@NgModule({
  declarations: [
    Advice0328Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0328Page),
  ],
})
export class Advice0328PageModule {}
