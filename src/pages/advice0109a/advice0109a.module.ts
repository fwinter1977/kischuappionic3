import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0109aPage } from './advice0109a';

@NgModule({
  declarations: [
    Advice0109aPage,
  ],
  imports: [
    IonicPageModule.forChild(Advice0109aPage),
  ],
})
export class Advice0109aPageModule {}
