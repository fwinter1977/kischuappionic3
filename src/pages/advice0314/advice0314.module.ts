import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0314Page } from './advice0314';

@NgModule({
  declarations: [
    Advice0314Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0314Page),
  ],
})
export class Advice0314PageModule {}
