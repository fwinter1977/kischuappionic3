import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0138Page } from './advice0138';

@NgModule({
  declarations: [
    Advice0138Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0138Page),
  ],
})
export class Advice0138PageModule {}
