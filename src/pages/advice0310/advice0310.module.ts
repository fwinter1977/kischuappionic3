import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0310Page } from './advice0310';

@NgModule({
  declarations: [
    Advice0310Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0310Page),
  ],
})
export class Advice0310PageModule {}
