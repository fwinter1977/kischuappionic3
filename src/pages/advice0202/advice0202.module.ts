import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0202Page } from './advice0202';

@NgModule({
  declarations: [
    Advice0202Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0202Page),
  ],
})
export class Advice0202PageModule {}
