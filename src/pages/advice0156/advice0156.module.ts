import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0156Page } from './advice0156';

@NgModule({
  declarations: [
    Advice0156Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0156Page),
  ],
})
export class Advice0156PageModule {}
