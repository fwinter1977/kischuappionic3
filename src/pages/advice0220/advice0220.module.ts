import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0220Page } from './advice0220';

@NgModule({
  declarations: [
    Advice0220Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0220Page),
  ],
})
export class Advice0220PageModule {}
