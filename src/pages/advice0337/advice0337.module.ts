import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Advice0337Page } from './advice0337';

@NgModule({
  declarations: [
    Advice0337Page,
  ],
  imports: [
    IonicPageModule.forChild(Advice0337Page),
  ],
})
export class Advice0337PageModule {}
