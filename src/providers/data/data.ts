import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
//import { checkAndUpdateDirectiveDynamic } from '@angular/core/src/view/provider';
import { Storage} from '@ionic/storage';

/*
  Generated class for the DataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

// Akte A, B C: je kann sein Typ 1, 2, 3
@Injectable()
export class DataProvider {
  sysdata : any;
  notasette : any;
  contasette : any;
  datasette2 : any;
  listennummer : string;
  public listentyp : string;
  public loggedin : boolean = false;
  public failedlogins: any = 0;
  constructor(public http: HttpClient, public storage: Storage) {
    /*
    this.datasette = 
    [
      {name: 'Aktenname', number: 'Fallaktennummer', type: '1', date: 'Änderungsdatum'},
      {name: '0101', checkeda: 'true', checkedb: 'true', hideMe: false, note: 'Notizentext'},
      {name: '0102', checkeda: 'false', checkedb: 'false', hideMe: false, note: 'Notizentext'},
    ]
    */
    this.datasette2=[
      {checkeda: "false", checkedb: "false", hidden: false, note: ""},
 
      {checkeda: "false", checkedb: "false", hidden: true, note: ""},
 
      {checkeda: "false", checkedb: "false", hidden: true, note: ""},

      {checkeda: "false", checkedb: "false", hidden: true, note: ""},

      {checkeda: "false", checkedb: "false", hidden: true, note: ""},

      {checkeda: "false", checkedb: "false", hidden: true, note: ""},

      {checkeda: "false", checkedb: "false", hidden: true, note: ""},

      {checkeda: "false", checkedb: "false", hidden: true, note: ""},

      {checkeda: "false", checkedb: "false", hidden: true, note: ""},

      {checkeda: "false", checkedb: "false", hidden: true, note: ""}
    ];
  }

  setData(number, checkeda, checkedb, hidden)
  {
    this.datasette2[number] = {checkeda: checkeda, checkedb: checkedb, hidden: hidden};
  }

  setNote(number, note)
  {
    this.datasette2[number].note = note;
    console.log('setted note');
  }

  getNote(number)
  {
    return this.datasette2[number].note;
  }

  getData(nummer)
  {
    this.listennummer = nummer;
    if(this.listennummer == '1')
    {
      this.storage.get('Akte1').then((_akte1) => {
        this.datasette2 = _akte1;
        });
      return this.datasette2;
    }
    if(this.listennummer == '2')
    {
      this.storage.get('Akte2').then((_akte2) => {
        this.datasette2 = _akte2;
        });
      return this.datasette2;
    }
    if(this.listennummer == '3')
    {
      this.storage.get('Akte3').then((_akte3) => {
        this.datasette2 = _akte3;
        });
      return this.datasette2;
    }
  }
  eraseData()
  {
    this.datasette2 = [];
  }

  getInitialData()
  {
    return this.datasette2;
  }

  getContacts()
  {
    this.storage.get('contacts').then((_contacts) => {
      this.contasette = _contacts;
      //this.contasette.sort();
      });
      //console.log('Kontakte', this.contasette)
    return this.contasette;
  }
  getTheContacts()
  {
    return this.contasette;
  }

  setLoggedIn(status : boolean)
  {
    this.loggedin = status;
  }

  getLoggedIn()
  {
    return this.loggedin;
  }

  getFailedLogins()
  {
    return this.failedlogins;
  }

  resetFailedLogins()
  {
    //this.sysdata.firstStart = true;
    this.failedlogins = 0;
    //this.storage = null;
  }
  
  setFailedLogins()
  {
    this.failedlogins = this.failedlogins + 1;
  }


  
  set1StStart()
  {
    this.sysdata.firstStart = false;
    //save
  }

  get1stStart()
  {
    //load
    return this.sysdata.firstStart;
  }



  loadAll()
  {
    /*
    this.storage.set('checked0101a', this.datasette[1].checkeda);
    this.storage.set('checked0101b', this.datasette[1].checkedb);
    this.storage.set('hideMe0101', this.datasette[1].hideMe);
    this.storage.set('note0101', this.datasette[1].note);
    */
    //return Promise.resolve(this.datasette);
  }


  saveToDisk(nummer)
  {
    //this.storage.set('0101', this.datasette[0]);
    //this.storage.set('0102', this.datasette2[0])
    this.listennummer = nummer;
    if(this.listennummer == '1')
    {
      this.storage.set('Akte1', this.datasette2);
      this.datasette2 = null;
    }
    if(this.listennummer == '2')
    {
      this.storage.set('Akte2', this.datasette2);
      this.datasette2 = null;
    }
    if(this.listennummer == '3')
    {
      this.storage.set('Akte3', this.datasette2);
      this.datasette2 = null;
    }
  }
}
