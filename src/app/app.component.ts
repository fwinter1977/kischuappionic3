import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ModalController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { IntroPage } from '../pages/intro/intro';
import { ImpressumPage } from '../pages/impressum/impressum';
import { EinstellungenPage } from '../pages/einstellungen/einstellungen';
import { KontaktePage } from '../pages/kontakte/kontakte';
import { SpeicherPage } from '../pages/speicher/speicher';
import { ErstInfoPage } from '../pages/erstinfo/erstinfo';
import { HomePage } from '../pages/home/home';
import  { SettingsPage } from '../pages/settings/settings';
/*
import { KiWoGe } from '../pages/kiwoge/kiwoge';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import {NutzungsPage} from '../pages/nutzung/nutzung';

import { SpeicherPage } from '../pages/speicher/speicher';

import { ErstInfoPage } from '../pages/erstinfo/erstinfo';



*/
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
loggedIn : boolean;
  @ViewChild(Nav) nav: Nav;

  //rootPage: any = HomePage;  
  rootPage: any = IntroPage;

  pages: Array<{title: string, component: any}>;
  

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,public modalCtrl : ModalController, public menuCtrl: MenuController) {

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage},
      { title: 'Speicher', component: SpeicherPage },
      { title: 'Kontakte', component: KontaktePage },
      { title: 'Kontakt hinzufügen', component: EinstellungenPage},
      { title: 'Impressum', component: ImpressumPage},
      { title: 'Tutorial', component: ErstInfoPage }
      /*
      ,
      { title: 'Tutorial', component: ErstInfoPage },
      { title: 'Nutzung der App', component: NutzungsPage},
      { title: 'Kindeswohlgefährdung', component: KiWoGe },
      { title: 'Gesetze', component: ErstInfoPage },
      { title: 'Hilfe anbieten', component: ErstInfoPage }
      */
    ];
    
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  ionViewDidEnter() {
    this.menuCtrl.enable(true, "menu");
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  Go(target: string)
  {
    var GoInPage = this.modalCtrl.create(target+'Page');
    GoInPage.present();
  }

  GoTo(target)
  {
    //var GoInPage = this.modalCtrl.create(target+'Page');
    //GoInPage.present();
    this.nav.setRoot(target);
  }

  GoHome()
  {
    //var GoInPage = this.modalCtrl.create(target+'Page');
    //GoInPage.present();
    this.nav.setRoot(HomePage);
  }
  GoSpeicher()
  {
    //var GoInPage = this.modalCtrl.create(target+'Page');
    //GoInPage.present();
    this.nav.setRoot(SpeicherPage);
  }
  GoKontakte()
  {
    //var GoInPage = this.modalCtrl.create(target+'Page');
    //GoInPage.present();
    this.nav.setRoot(KontaktePage);
  }
  GoEinstellungen()
  {
    //var GoInPage = this.modalCtrl.create(target+'Page');
    //GoInPage.present();
    this.nav.setRoot(EinstellungenPage);
  }
  GoErstInfo()
  {
    //var GoInPage = this.modalCtrl.create(target+'Page');
    //GoInPage.present();
    this.nav.setRoot(ErstInfoPage);
  }
  GoImpressum()
  {
    //var GoInPage = this.modalCtrl.create(target+'Page');
    //GoInPage.present();
    this.nav.setRoot(ImpressumPage);
  }
  GoEinstellungen2()
  {
    //var GoInPage = this.modalCtrl.create(target+'Page');
    //GoInPage.present();
    this.nav.setRoot(SettingsPage);
  }
}
