import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { IonicStorageModule } from '@ionic/storage';  //FW: SQLite
//import { Storage } from '@ionic/storage'; // FW SQlite: https://www.youtube.com/watch?v=E0aTpYTFhDk
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { NutzungsPage } from '../pages/nutzung/nutzung';
import { ListPage } from '../pages/list/list';
import { CheckList01 } from '../pages/checklist01/checklist01';
import { CheckList01B } from '../pages/checklist01B/checklist01B';
import { CheckList01C } from '../pages/checklist01C/checklist01C';
import { CheckList01D } from '../pages/checklist01D/checklist01D';
import { CheckList01E } from '../pages/checklist01E/checklist01E';
import { CheckList02A } from '../pages/checklist02A/checklist02A';
import { CheckList02B } from '../pages/checklist02B/checklist02B';
import { CheckList02C } from '../pages/checklist02C/checklist02C';
import { CheckList02D } from '../pages/checklist02D/checklist02D';
import { CheckList03A } from '../pages/checklist03A/checklist03A';
import { CheckList03B } from '../pages/checklist03B/checklist03B';
import { CheckList03C } from '../pages/checklist03C/checklist03C';
import { CheckList03D } from '../pages/checklist03D/checklist03D';
import { CheckList03E } from '../pages/checklist03E/checklist03E';
import { Nutzung01 } from '../pages/nutzung01/nutzung01';
import { Nutzung02 } from '../pages/nutzung02/nutzung02';
import { Nutzung02A } from '../pages/nutzung02A/nutzung02A';
import { Nutzung02B } from '../pages/nutzung02B/nutzung02B';
import { Nutzung02C } from '../pages/nutzung02C/nutzung02C';
import { Nutzung02D } from '../pages/nutzung02D/nutzung02D';
import { Nutzung02E } from '../pages/nutzung02E/nutzung02E';
import { Nutzung02F } from '../pages/nutzung02F/nutzung02F';
import { Nutzung02G } from '../pages/nutzung02G/nutzung02G';
import { Nutzung02H } from '../pages/nutzung02H/nutzung02H';
import { Nutzung03 } from '../pages/nutzung03/nutzung03';
import { Nutzung04 } from '../pages/nutzung04/nutzung04';
import { KiWoGe } from '../pages/kiwoge/kiwoge';
import { KiWoGe01 } from '../pages/kiwoge01/kiwoge01';
import { KiWoGe01A } from '../pages/kiwoge01A/kiwoge01A';
import { KiWoGe01B } from '../pages/kiwoge01B/kiwoge01B';
import { KiWoGe01C } from '../pages/kiwoge01C/kiwoge01C';
import { KiWoGe01D } from '../pages/kiwoge01D/kiwoge01D';
import { KiWoGe01E } from '../pages/kiwoge01E/kiwoge01E';
import { KiWoGe01F } from '../pages/kiwoge01F/kiwoge01F';
import { KiWoGe01G } from '../pages/kiwoge01G/kiwoge01G';
import { ImpressumPage } from '../pages/impressum/impressum';
import { SpeicherPage } from '../pages/speicher/speicher';
import { EinstellungenPage } from '../pages/einstellungen/einstellungen';
import { ErstInfoPage } from '../pages/erstinfo/erstinfo';
import { LoginPage } from '../pages/login/login';
import { IntroPage } from '../pages/intro/intro';
//import { GoInChecklist01Page } from '../pages/go-in-checklist01/go-in-checklist01';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { RegistrationPage } from '../pages/registration/registration';
import { Registration02Page } from '../pages/registration02/registration02';
import { KontaktePage } from '../pages/kontakte/kontakte';
import { DataProvider } from '../providers/data/data';
import { HttpClientModule } from '@angular/common/http'; 
import { AbspeichernPage } from '../pages/abspeichern/abspeichern';
import { Speicher2Page } from '../pages/speicher2/speicher2';
import { Speicher3Page } from '../pages/speicher3/speicher3';
import { TabsPage } from '../pages/tabs/tabs';
import { CheckLists } from '../pages/checklists/checklists';
//import { SettingsPage } from '../pages/settings/settings';
@NgModule({
  declarations: [
    MyApp,
    IntroPage,
    LoginPage,
    HomePage,
    ListPage,
    ImpressumPage,
    SpeicherPage, Speicher2Page, Speicher3Page,
    EinstellungenPage,
    ErstInfoPage,
    RegistrationPage, Registration02Page, KontaktePage,
    AbspeichernPage, 
    NutzungsPage, Nutzung01, Nutzung02, Nutzung02A, Nutzung02B, Nutzung02C, Nutzung02D, Nutzung02E, Nutzung02F, Nutzung02G, Nutzung02H, Nutzung03, Nutzung04,
      KiWoGe, KiWoGe01, KiWoGe01A, KiWoGe01B, KiWoGe01C, KiWoGe01D, KiWoGe01E, KiWoGe01F, KiWoGe01G,
    TabsPage, CheckLists, 
    CheckList01, CheckList01B, CheckList01C, CheckList01D, CheckList01E,
    CheckList02A, CheckList02B, CheckList02C, CheckList02D,
    CheckList03A, CheckList03B, CheckList03C, CheckList03D, CheckList03E

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__strgDB',
         driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    IntroPage,
    LoginPage,
    HomePage, 
    NutzungsPage, Nutzung01, Nutzung02, Nutzung02A, Nutzung02B, Nutzung02C, Nutzung02D, Nutzung02E, Nutzung02F, Nutzung02G, Nutzung02H, Nutzung03, Nutzung04,
      KiWoGe, KiWoGe01, KiWoGe01A, KiWoGe01B, KiWoGe01C, KiWoGe01D, KiWoGe01E, KiWoGe01F, KiWoGe01G,
    ListPage,
    ImpressumPage,
    SpeicherPage, Speicher2Page, Speicher3Page,
    EinstellungenPage,
    ErstInfoPage,
    RegistrationPage, Registration02Page, KontaktePage,
    AbspeichernPage,
    TabsPage, CheckLists,
    CheckList01, CheckList01B, CheckList01C, CheckList01D, CheckList01E,
    CheckList02A, CheckList02B, CheckList02C, CheckList02D,
    CheckList03A, CheckList03B, CheckList03C, CheckList03D, CheckList03E
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DataProvider
  ]
})
export class AppModule
{
}
